#==================================================
# AVR-GXX Makefile
#==================================================
# File         : programmer.inc
# Author       : Andrew Carney
# Date         : 16th February 2021
# Version      : First Version for Atmel devices V1.00
# Copyright(c) 2021  Andrew Carney
# License	: GPL v3.0 or later
# **********************************************************************
# Modification :
# **********************************************************************
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>
# **********************************************************************
#
##	Programmer defines
CPU			=	-p atmega328p
UNO_PORT0	=	-P /dev/ttyACM0
UNO_PORT1	=	-P /dev/ttyACM1
NANO_PORT0	=	-P /dev/ttyUSB0
NANO_PORT1	=	-P /dev/ttyUSB1
UNO_SPEED	=	-b 115200
NANO_SPEED	=	-b 57600
ARDUINO		=	-c arduino
USBTINY		=	-c usbtiny
FLASH		=	-U flash:w:$(TARGET).hex:i
EEPROM		=	-U eeprom:w:$(TARGET).eep:i
#=====================================================
tinyflash: $(TARGET).hex
ifeq ($(TARGET),)
	@echo "===================================="
	@echo "To write the file to the device"
	@echo "the target MUST be specified"
	@echo "As in -- "
	@echo "      make TARGET=Program tinyflash"
	@echo "==================================="
else
		$(PROGRAM) $(CPU) $(USBTINY) $(FLASH)
endif
tinyeeprom: $(TARGET).hex
	$(PROGRAM) $(CPU) $(USBTINY) $(EEPROM)

Uno_0: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(UNO_PORT0) $(UNO_SPEED) $(FLASH)

Uno_1: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(UNO_PORT1) $(UNO_SPEED) $(FLASH)

Uno_0_eeprom: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(UNO_PORT0) $(UNO_SPEED) $(EEPROM)

Uno_1_eeprom: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(UNO_PORT2) $(UNO_SPEED) $(EEPROM)

nano_0: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(NANO_PORT0) $(NANO_SPEED) $(FLASH)

nano_0_eeprom: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(NANO_PORT0) $(NANO_SPEED) $(EEPROM)

nano_1: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(NANO_PORT1) $(NANO_SPEED) $(FLASH)

nano_1_eeprom: $(TARGET).hex
	$(PROGRAM) -D $(CPU) $(ARDUINO) $(NANO_PORT1) $(NANO_SPEED) $(EEPROM)
