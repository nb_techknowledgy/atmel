/* ********************************************************************/
/* File Name    : SPI.hpp                                             */
/* Description  : C++ class to drive the Atmel SPI port               */
/*                Unlike the correpsonding Arduino library this       */
/*                class does NOT provide a 'bit-bang' style interface */
/*                It will ONLY operate on the dedicated SPI lines     */
/*                the devce select line can be chosen which allows    */
/*                more than one device to be attached and driven      */
/*                with each device being driven by a separate class   */
/*                instance.                                           */
/*                This DOES NOT operate in interrupt mode, but may be */
/*                a future enhancement if the speed gain is deemed    */
/*                worth the effort                                    */
/* Author       : Andrew Carney                                       */
/* Date         : 16th February 2021                                  */
/* Version      : First Version for Atmel devices V1.00               */
/* Copyright(c) 2021  Andrew Carney                                   */
/* License      : GPL v3.0 or later                                   */
/* ********************************************************************/
/* Modification :                                                     */
/*      V1.01     Doxygen documentation hooks added                   */
/* *********************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __SPI_H
#define __SPI_H

#include <avr/io.h>
#include <stdint.h>

enum SPIorder	{ LSB_FIRST, MSB_FIRST };

class SPI
{
	public:
		enum SPImode
		{	SPI_MODE0 = 0x00,
			SPI_MODE1 = 0x04,
			SPI_MODE2 = 0x08,
			SPI_MODE3 = 0x0C
		};
		SPI(uint32_t Clock, uint8_t BitOrder, SPImode Mode);
//		SPI();
		void		SetCSpin(uint8_t *pPort, uint8_t Mask);
		void		Enable()	{	SPCR |=  (1 << SPE);	}
		void		Disable()	{	SPCR &= ~(1 << SPE);	}
		uint8_t		Transfer(uint8_t Data);
		uint16_t	Transfer(uint16_t SendData);
		uint32_t	Transfer(uint32_t SendData);
		uint8_t		Transfer(uint8_t SendData[], uint8_t RecvData[], uint8_t DataLen);
	private:
		volatile uint8_t	*m_pCS_Port;
		uint8_t	m_CS_PinMask;

};
#endif
