/* ****************************************************************/
/* File Name    : Timer.hpp                                       */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __TIMER_HPP
#define __TIMER_HPP

#include "HwTimer.hpp"

#define TIMER_RESOLUTION	(49)	/* 1mS resolution -- PIT fires every milli-second */
#define TIMER_PRESCALER		(4)
#define MAX_TIMER_CHANS		(16)	/* sixteen timers ... from one hardware timer */

#define TIMER_ELAPSED (-1)


class Timer
{
	public:
		Timer(eHW_TIMER eTimer = Timer0);
		~Timer();
		void		StartTimer(	uint32_t timeout, void *pData = NULL);
		void		RestartTimer();
		void		ResetTimer();
		void		StopTimer();
		void		KillTimer();
		uint32_t	GetElapsedTime();
		static void	Handler();
		virtual	void Callback() = 0;
	protected:
		void				*m_pData;
	private:
		//	--------------------------------------------------------------------
		//	variable shared across all instances
		static eHW_TIMER	s_eHWtimer;	// set a default
		static uint8_t		s_initialised;
		static uint32_t 	s_ElapsedTime;
		static int8_t		s_MaxToTry;
		static uint8_t		s_Running;
		static Timer		*s_Timer[MAX_TIMER_CHANS];
		static HwTimer		s_hwtimer;
		//	--------------------------------------------------------------------
		//	variables unique to each object
		int8_t				m_Channel;
		volatile uint32_t	m_TimeOut;
		volatile uint32_t	m_Interval;
		volatile uint8_t	m_Stopped;
		volatile uint8_t	m_InUse;
		//	--------------------------------------------------------------------
};
#endif	/* end of #ifdef __TIMER_HPP		*/
