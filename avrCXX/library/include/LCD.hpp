/* ****************************************************************/
/* File Name    : LCD.hpp                                         */
/* Description  : C++ class to drives a basic HD4470 LCD display  */
/*              : Unlike the original Arduino library, this class */
/*                1. does NOT support 8 bit mode                  */
/*                2. defaults to using a single write to write to */
/*                   the data bits (D7..D4)                       */
/*                3. defaults to a 20 char by 4 line display      */
/*                4. is optimised for speed, not size             */
/*                5. does not need the Arduino.h header file      */
/*                Derived form Arduino Libary LiquidCrystal.h     */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Migrate from Timer inheritance to DelayTimer to */
/*                implment Delay() function                       */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __LCD_H
#define __LCD_H
#include <avr/io.h>
#include <stdint.h>

#include "LCDdef.hpp"
#include "DelayTimer.hpp"

class LCD	: public DelayTimer
{
	public:
				LCD(	uint8_t rs,
						uint8_t enable,
						uint8_t d0,
						uint8_t d1,
						uint8_t d2,
						uint8_t d3,
						bool	bitwise = false);
//				~LCD()	{;};
		void	InitDisplay(uint8_t cols = 20, uint8_t rows = 4);
		void	Size(uint8_t rows, uint8_t cols)		{	m_Rows = rows; m_Columns = cols;	}
		void	Rows(uint8_t rows)						{	m_Rows = rows;						}
		void	Columns(uint8_t cols)					{	m_Columns = cols;					}
		uint8_t	Rows()									{	return m_Rows;						}
		uint8_t	Columns()								{	return m_Columns;					}
		void	Clear()									{						Command(LCD_CLEARDISPLAY);	Delay(5);								};
		void	Home()									{						Command(LCD_RETURNHOME);	Delay(5);								};
		void	Display(bool OnOff)						{	if (OnOff)			Command(m_DisplayState		|=	 LCD_DISPLAYON);
															else				Command(m_DisplayState		&=	~LCD_DISPLAYON);					};
		void	Blink(bool OnOff)						{	if (OnOff)			Command(m_DisplayState		|=	LCD_CURSORON);
															else				Command(m_DisplayState		&=	~LCD_CURSORON);						};
		void	Cursor(bool OnOff)						{	if (OnOff)			Command(m_DisplayState		|=	LCD_BLINKON);
															else				Command(m_DisplayState 		&=	~LCD_BLINKON);						};
		void	AutoScroll(bool OnOff)					{	if (OnOff)			Command(m_DisplayMode		|=	LCD_ENTRYSHIFTINCREMENT);
															else				Command(m_DisplayMode		&=	~LCD_ENTRYSHIFTINCREMENT);			};
		void	LeftToRight()							{						Command(m_DisplayMode		|=	LCD_ENTRYLEFT);						};
		void	RightToLeft()							{						Command(m_DisplayMode		&=	~LCD_ENTRYLEFT);					};
		void	ScrollLeft(uint8_t columns)				{	while(columns--) {	Command(LCD_CURSORSHIFT		|	LCD_DISPLAYMOVE | LCD_MOVELEFT);}	};
		void	ScrollRight(uint8_t columns)			{	while(columns--) {	Command(LCD_CURSORSHIFT		|	LCD_DISPLAYMOVE | LCD_MOVERIGHT);}	};
		void	SetCursor(uint8_t column, uint8_t row);
		void	CustomChar(uint8_t loc, uint8_t map[]);

		void	Command(uint8_t command);
		uint8_t	Write(const char *pData);
		void	Write(uint8_t data);
		void	WritePins(uint8_t data);
	private:
		void	DoClock();
		void	WriteNibble(uint8_t nibble);
		void	WriteNibblePins(uint8_t nibble);
		//	---------------------------------------------
		uint8_t		m_Rows;
		uint8_t		m_Columns;
		struct		{
						volatile uint8_t *Port;
						uint8_t				Pin;
					} m_DataBit[8];
		//	---------------------------------------------
		volatile	uint8_t	*m_pRS_Port;
		volatile	uint8_t	*m_RS_PortPin;
		volatile	uint8_t	*m_pRS_PortDirection;

		volatile	uint8_t	*m_pEnablePort;
		volatile	uint8_t	*m_pEnablePortDirection;

		volatile	uint8_t	*m_pDataPort;
		volatile	uint8_t	*m_pDataPortDirection;

		uint8_t		m_DisplayState;
		uint8_t		m_DisplayMode;
		uint8_t		m_RS_PortBit;

		uint8_t		m_EnablePortBit;
		uint8_t		m_DataPortBits[3];
		uint8_t		m_DataPortMask;
		uint8_t		m_DataShift;
		uint8_t		m_PortNum;
		bool		m_BitWrite;				//	if true -- data bits are spread across 2 or more ports !

};

#endif
