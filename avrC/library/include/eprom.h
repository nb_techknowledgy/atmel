/* ****************************************************************/
/* File Name    : eprom.h                                         */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __EPROM_H
#define __EPROM_H

#define SERVO_MIN_POSITION		(0)
#define SERVO_MAX_POSITION		(1)



#define EPROM_Set_UINT16(x,y)	do { ; } while(0)
#define EPROM_Get_UINT16(x)		(0)



#endif	// of #ifndef __EPROM_H
