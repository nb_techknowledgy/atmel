/* ****************************************************************/
/* File Name    : Delay.hpp                                       */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __DELAYTIMER_HPP
#define __DELAYTIMER_HPP

#include "Timer.hpp"

class DelayTimer	:	public Timer
{
	public:
//		DelayTimer();
		inline void	DelayMs(uint16_t delay)	{	Delay(delay);		}
		/** ************************************************************
		 *	\brief		Timer based millisecond delay
		 *	\param		time in mS
		 *	\return		Nothing;
		 **************************************************************/
		void	Delay(uint16_t mSec)	{	m_Done = false;		\
											StartTimer(mSec);	\
											while (!m_Done);	}
		/** ************************************************************
		 *	\brief		Loop based short delay.
		 *	\param		uSec	--	Approximate microsecond delay.
		 *	\return		Nothing.
		 **************************************************************/
		void DelayUs(uint16_t uSec)								\
		{														\
			volatile uint16_t loop;								\
			loop = (uSec >> 1) + (uSec >> 2) + (uSec >> 3);		\
			while (--loop);										\
		}

	private:
		void Callback()	{	m_Done = true;	}
		volatile bool m_Done;
};

#endif //	endif "#ifndef __DELAYTIMER_HPP"
