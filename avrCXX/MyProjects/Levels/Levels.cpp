/* ****************************************************************/
/* File Name    : LCDtest.cpp                                     */
/* Description  : A program to monitor the fluid levels from      */
/*                3 level sensors and display on an LED display   */  
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License	: GPL v3.0 or later                                   */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/

#include <stdint.h>
#include <string.h>
//#include <unistd.h>

#include "Timer.hpp"
#include "A2D.hpp"
#include "LedMux.hpp"

//#define	DEBUG_LEVEL	(1)
#ifdef DEBUG_LEVEL
// #define DEBUG_ADC
#include "Serial.hpp"

#define DEBUG_PRINT(x,y,z)	if (DEBUG_LEVEL >= z) { SERIAL_SendMessageInt(x,y); }
#else
#define DEBUG_PRINT(x,y,z)	while (0) { ; }
#endif

class Display : public Timer
{
	public:
		Display()	{	m_Sensor.Start();	}
		void Callback();
		LedMux	m_Display;
	private:
		A2D		m_Sensor;
};

void Display::Callback()
{
	static uint8_t index = 0;
	int ADCvalue = -1;
	switch (index)
	{
		case 0:	{
			m_Display.DisplayWord("FUEL");
			}	break;
		case 1:
			ADCvalue = m_Sensor.GetValue(5);
			break;
		case 3:	{
			m_Display.DisplayWord("H20");
			}	break;
		case 4:
			ADCvalue = m_Sensor.GetValue(6);
			break;
		case 6:	{
			m_Display.DisplayWord("Loo");
			}	break;
		case 7:
			ADCvalue = m_Sensor.GetValue(7);
			break;
		default:
			m_Display.Blank();
			break;
	}
	if (-1 != ADCvalue)
	{
		if (ADCvalue < 1001)
//			m_Display.DisplayUnits(ADCvalue/5,3,1,'L');
			m_Display.DisplayNumber(ADCvalue,3,1);
		else
			m_Display.DisplayWord("FULL");
	}
#ifdef DEBUG_ADC
	char toprint[40];
	memset(toprint,0,40);
	int length = sprintf(toprint,"ADC(%i) = %i\n\r",index,ADCvalue);
	SERIAL_SendMessagePoll(toprint,length);
#endif	
	index++;
	if (index > 9) index = 0;
}

int main()
{
	Display	myDisplay;
	myDisplay.m_Display.Test();
	myDisplay.StartTimer(1500);
	do
	{
	} while (1);
	return 0;
}
