/* ****************************************************************/
/* File Name    : LCDtest.cpp                                     */
/* Description  : A program to test an LCD display                */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License	: GPL v3.0 or later                                   */
/* ****************************************************************/
/* Modification :	                                              */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <stdio.h>
#include "LCD.hpp"

LCD lcd(2,3,4,5,6,7,true);

void Scroll()
{
	lcd.Clear();
	lcd.SetCursor(10,0);
	lcd.Write("Scroll");
	lcd.SetCursor(13,1);
	lcd.Write("1");
	lcd.SetCursor(13,2);
	lcd.Write("2");
	lcd.SetCursor(13,3);
	lcd.Write("3");
	for (int k=0; k<5; k++)
	{
		lcd.ScrollLeft(2);
		lcd.DelayMs(500);
	}
	for (int k=0; k<5; k++)
	{
		lcd.ScrollRight(2);
		lcd.DelayMs(500);
	}
}
void MultiLine()
{
	lcd.Clear();
	lcd.Write("1234567890123456789098765432109876543210abcdefghijklmnopqrstABCDEFGHIJKLMNOPQRST");
}

void Cursor()
{
	lcd.Cursor(true);
	lcd.Blink(true);
	lcd.DelayMs(2000);
	lcd.Cursor(false);
	lcd.Blink(false);
}
void SetCursor()
{
	lcd.Clear();
	for (int y=0;y<4;y++)
	{
		for (int x=0; x<20; x++)
		{
			lcd.Clear();
			lcd.SetCursor(x,y);
			lcd.Write("x");
			lcd.DelayMs(200);
		}
	}
}
void	Direction()
{
	lcd.Clear();
	lcd.Write("Direction");
	lcd.SetCursor(15,1);
	lcd.RightToLeft();
	lcd.Write("tfeLoTthgiR");
	lcd.SetCursor(5,2);
	lcd.LeftToRight();
	lcd.Write("LeftToRight");
	lcd.DelayMs(5000);
}

const char *buffer = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

void AutoScroll()
{
	uint8_t k = 0;
	lcd.Clear();
	do
	{
		lcd.Write(buffer[k++]);
		lcd.DelayMs(200);
	}	while (buffer[k]);
	lcd.Clear();
	lcd.Write("Set Autoscroll");
	lcd.AutoScroll(true);
	k = 0;
	do
	{
		lcd.Write(buffer[k++]);
		lcd.DelayMs(200);
	}	while (buffer[k]);
	lcd.Clear();
	lcd.Write("Clear Autoscroll");
	lcd.AutoScroll(false);
	k = 0;
	do
	{
		lcd.Write(buffer[k++]);
		lcd.DelayMs(200);
	}	while (buffer[k]);
}

void EveryChar()
{
	uint8_t charmap[8] = { 7, 5, 7, 0, 0, 0, 0, 0 };
	lcd.Clear();
	lcd.CustomChar(2,charmap);
	lcd.Write("38.4\2C");
	lcd.Delay(1500);
	do
	{
		for (uint8_t c=128;c<138;c++)
		{
			char buff[20];
			sprintf(buff,"Char %i = ",c);
			lcd.SetCursor(0,0);
			lcd.Write(buff);
			lcd.Write(c);
			lcd.Delay(500);
		}
		lcd.Write("38.4\2C");
		lcd.Delay(2000);
	} while (true);
}

int main()
{
	DDRB = 3;
	PORTB = 1;
	lcd.InitDisplay();
	lcd.Display(true);
	lcd.Write("Testing");
	while (1)
	{
		EveryChar();
		lcd.DelayMs(1000);
		Cursor();
		lcd.DelayMs(1000);
		MultiLine();
		lcd.DelayMs(1000);
		Scroll();
		lcd.DelayMs(1000);
		AutoScroll();
		lcd.DelayMs(1000);
		Direction();
		lcd.DelayMs(5000);
		SetCursor();}
	while(1);
	return 0;
}
