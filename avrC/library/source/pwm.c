/* ****************************************************************/
/* File Name    : pwm.c                                           */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include "hwtimer.h"	// for timer definitions

uint8_t PWM_Init(uint32_t u32Period, eHW_TIMER eTimer)
{
//	if ( 0 == u8HWTIMER_AcquireTimer(eTimer))
//	{
//		return 0;
//	}
	switch(eTimer)
	{
		case Timer0:
			DDRD |= (1 << DDD5) |(1 << DDD6);
			TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM00) | (1 << WGM01);
			TCCR0B = (1 << CS01) | (1 << CS00);
			OCR0A  = 0;								// set PWM to 0% as initial value
			OCR0B  = 0;								// set PWM to 0% as initial value
			break;
		case Timer1:
			DDRB |= (1 << DDB1) | (1 << DDB2);
    		TCCR1A |= (1 << COM1A1)|(1 << COM1B1);	// set none-inverting mode
			TCCR1A |= (1 << WGM11);
			TCCR1B |= (1 << WGM12)|(1 << WGM13);// set Fast PWM mode using ICR1 as TOP
			TCCR1B |= (1 << CS10);
			break;
    // START the timer with no prescaler
			return (0);
			break;
		case Timer2:
			DDRD |= (1 << DDD3) | (1 << DDB3);
			TCCR2A = (1 << COM2A1) | (1 << COM2B1) | (1 << WGM20);
			TCCR2B = (1 << CS21) | (1 << CS20);
			OCR2A  = 0;								// set PWM to 0% as initial value
			OCR2B  = 0;								// set PWM to 0% as initial value
			break;
		default:
			return (0);
	}
	return (0);
}

void PWM_Start(eHW_TIMER eTimer, uint8_t Pin, uint8_t u8Period)
{
	switch (Pin)
	{
		case 'a':
		case 'A':
		case '1':
			switch(eTimer)
			{
				case Timer0:
					OCR0A = u8Period;
					break;
				case Timer1:
					OCR1A = u8Period;
					break;
				case Timer2:
					OCR2A = u8Period;
					break;
			}
			break;
		case 'b':
		case 'B':
		case '2':
			switch(eTimer)
			{
				case Timer0:
					OCR0B = u8Period;
					break;
				case Timer1:
					OCR1B = u8Period;
					break;
				case Timer2:
					OCR2B = u8Period;
					break;
			}
			break;
	}
}

void PWM_Stop(eHW_TIMER eTimer, uint8_t Pin)
{
	PWM_Start(eTimer, Pin, 0);
}

