/* ****************************************************************/
/* File Name    : LedMux.cpp                                      */
/* Description  :                                                 */
/*                                                                */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>

#include <ctype.h>

#include "LedMux.hpp"

#define		segmentA	(4)
#define		segmentB	(8)
#define		segmentC	(16)
#define		segmentD	(32)
#define		segmentE	(64)
#define		segmentF	(128)
#define		segmentG	(256)
#define		segmentDP	(512)
#define		BLANK		(17)
#define		LETTER_R	(segmentE + segmentG)

uint16_t	LedMux::s_digits[4]  = { 0,0,0,0 };

const uint16_t c_number[] = {
	( segmentA	+	segmentB	+	segmentC	+	segmentD	+	segmentE	+	segmentF					),	//	0
	( 				segmentB	+	segmentC																	),	//	1
	( segmentA	+	segmentB	+				+	segmentD	+	segmentE					+	segmentG	),	//	2
	( segmentA	+	segmentB	+	segmentC	+	segmentD									+	segmentG	),	//	3
	( 				segmentB	+	segmentC	+				+				+	segmentF	+	segmentG	),	//	4
	( segmentA					+	segmentC	+	segmentD					+	segmentF	+	segmentG	),	//	5
	( segmentA					+	segmentC	+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	6
	( segmentA	+	segmentB	+	segmentC																	),	//	7
	( segmentA	+	segmentB	+	segmentC	+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	8
	( segmentA	+	segmentB	+	segmentC	+	segmentD					+	segmentF	+	segmentG	),	//	9
	( segmentA	+	segmentB	+	segmentC	+				+	segmentE	+	segmentF	+	segmentG	),	//	A
	( 								segmentC	+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	B
	( segmentA					+				+	segmentD	+	segmentE	+	segmentF					),	//	C
	( 				segmentB	+	segmentC	+	segmentD	+	segmentE					+	segmentG	),	//	D
	( segmentA					+				+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	E
	( segmentA					+				+				+	segmentE	+	segmentF	+	segmentG	),	//	F
	( 0																											)	//	Blank

};


/** ********************************************************************
 *	\brief		Does the workings of the display.

 *				Called by the base class timer at the multiplexing interval
 * 				At each call blanks the current digit and lights the
 * 				next digit using data from the display buffer.
 * \return		Nothing.
 **********************************************************************/
void LedMux::Callback()
{
	static	uint8_t		s_current;
	PORTD = s_digits[s_current] & 255;
	PORTB = (s_digits[s_current] >> 8) & 7;
	PORTC = ~(1 << s_current);
	s_current++;
	s_current &= 3;
}

/** ********************************************************************
 *	\brief		Instantiates the object.

 *				Sets all the port pins to outputs as needed, blanks the
 *				display buffer, and starts the multiplexing timer at
 *				5mS ( could proabably be slowed down if needed ).
 *	\return		Nothing.
 **********************************************************************/
LedMux::LedMux()
{
	DDRB = 7;
	DDRC = 15;
	DDRD = 252;
	PORTD = 252;
	PORTC = 15;
	PORTB = 7;
	Blank();
	StartTimer(5,NULL);
}

/** ********************************************************************
 *	\brief		Clear the display to blank.

 *				Sets the display buffer to ALL OFF
 *	\return		Nothing.
 **********************************************************************/
void LedMux::Blank()
{
	s_digits[0] = 0;
	s_digits[1] = 0;
	s_digits[2] = 0;
	s_digits[3] = 0;
}

/** ********************************************************************
 *	\brief		Test the display segments all work.

 *				Sets the display buffer to ALL ON.
 *	\return		Nothing.
 **********************************************************************/
void LedMux::Test()
{
	s_digits[0] = c_number[8]+segmentDP;
	s_digits[1] = c_number[8]+segmentDP;
	s_digits[2] = c_number[8]+segmentDP;
	s_digits[3] = c_number[8];
}

/** ********************************************************************
 *	\brief		Retrieves the bitmap of the given number in hex.

 *				Performs a lookup in a table to retrieve a suitable
 * 				segment bitmap.
 *	\param		number	--	the number to be displayed.
 *	\return		a segment bitmap for the supplied number.
 **********************************************************************/
uint16_t LedMux::GetHexDigit(uint8_t number)
{
	if ((number >= 0) && (number < 16))
	{
		return (c_number[number]);
	}
	return (0);
}

/** ********************************************************************
 *	\brief		Convert the given character to a seven segment bit
 * 				pattern.

 *				This simply maps some alphabetical letters to segments.\n
 *				Not all latters can be represented on a seven segment
 *				display, so invalid characters return a blank.
 *	\param		ch	--	the letter to get a bitamp for.
 *	\return		a segment bitmap for the supplied letter.
 **********************************************************************/
uint16_t LedMux::GetLetter(char ch)
{
	switch (ch)
	{
		case 'h':
			return (segmentC + segmentE + segmentF + segmentG );			//	h
		case 'H':
			return (segmentB + segmentC + segmentE + segmentF + segmentG );	//	H
		case 'l':
			return LedMux::GetHexDigit(1);									//	l
		case 'L':
			return (segmentD + segmentE + segmentF	);						//	L
		case 'o':
			return (segmentC + segmentD + segmentE + segmentG	);			//	o
		case 'O':
			return LedMux::GetHexDigit(0);									//	O
		case 'r':
			return (segmentE + segmentG);									//	r
		case 'R':
			return (segmentA + segmentE + segmentF	);						//	R
		case 'u':
			return (segmentC + segmentD + segmentE );						//	u
		case 'U':
			return (segmentB + segmentC + segmentD + segmentE + segmentF );	//	U
		default:
			return (0);
	}
	return (0);
}

/** ********************************************************************
 *	\brief		Display a four letter word on the display.

 * 				There is no letter 'k' so not that particular word !!\n
 *				Within the limits of letters available on a
 * 				seven segment display, it writes the first four letters
 * 				in 'word' to the display.
 *	\param		word	--	a null terminated string.
 *	\return		Nothing.
 **********************************************************************/
void LedMux::DisplayWord(const char *word)
{
	uint8_t length = strlen(word);
	uint8_t index = 0;
	uint8_t d_index;
	if (length < 4)
	{
		d_index = 4-length;
	}
	else
	{
		d_index = 0;
	}
	LedMux::Blank();
	while (d_index < 4)
	{
		char letter = toupper(word[index]);
		if (letter <= '9')
		{
			s_digits[d_index] = LedMux::GetHexDigit(letter-'0');
		}
		else if (letter <= 'F')
		{
			s_digits[d_index] = LedMux::GetHexDigit(letter-'A'+10);
		}
		else
		{
			s_digits[d_index] = LedMux::GetLetter(word[index]);
		}
		index++;
		d_index++;
	}
}

/** ********************************************************************
 *	\brief		Displays a number between 0 and 9999.
 *	\param		value			value to display.
 *	\param		DPpos			position of the decimal point
 *	\param		LeadingZero		The number of leading zeros
 *	\return		Nothing.
 **********************************************************************/
void LedMux::DisplayNumber(uint16_t value,int DPpos,int LeadingZero)
{
	if (value > 9999 )
	{
		LedMux::DisplayWord("Err");
	}
	else
	{
		uint8_t d = 3;
		if(LeadingZero > 0)
		{
			while(LeadingZero--)
			{
				s_digits[LeadingZero&3] = c_number[0];
			}
		}
		Blank();
		while (value > 0)
		{
			int digit = value % 10;
			s_digits[d--] = c_number[digit & 15] & 65535;;
			value /= 10;
		}
		if ((DPpos > 0) && (DPpos < 4))
		{
			s_digits[DPpos-1] += segmentDP;
		}
	}
}

/** ********************************************************************
 *	\brief		Display a 3 digit number followed by a 'units' suffix
 * 				as per the 'PostFix' paramater.

 *				Values from 0 to 999 are valid and are displayed in the
 *				leftmost three digits, with the rightmost displaying
 *				the 'units'.
 *	\param		value			A number between 0 and 999.
 *	\param		DPpos			Position of the decimal point.
 *	\param		LeadingZero		How many leading zeros to display.
 *	\param		PostFix			Character to place at the end of the display
 *	\return		Nothing.
 **********************************************************************/
void LedMux::DisplayUnits(uint16_t value,int DPpos,int LeadingZero,char PostFix)
{
	if (value > 999 )
	{
		LedMux::DisplayWord("Err");
	}
	else
	{
		uint8_t d = 2;
		LedMux::Blank();
		if(LeadingZero > 0)
		{
			while(LeadingZero--)
			{
				s_digits[3-(LeadingZero&3)] = LedMux::GetHexDigit(0);
			}
		}
		while (value > 0)
		{
			int digit = value % 10;
			s_digits[d--] = LedMux::GetHexDigit(digit & 15);
			value /= 10;
		}
		if ((DPpos > 0) && (DPpos < 3))
		{
			s_digits[DPpos-1] += segmentDP;
		}
		s_digits[3] = LedMux::GetLetter(PostFix);
	}
}

/** ********************************************************************
 *	\brief		Allow direct control of all segments of the display.

 *				Write 4 bytes into the display buffer.\n
 * 				All bits set to a 1 will cause the corresponding
 *				segment	to be lit. Aall bits set to a 0 will not.
 *	\param		*Message	--	point to a four byte buffer.
 *				NOTE		--	It is NOT a null terminated string.
 *	\return		Nothing.
 **********************************************************************/
void LedMux::DisplayLiteral(uint8_t	*Message)
{
	s_digits[0] = *Message++;
	s_digits[1] = *Message++;
	s_digits[2] = *Message++;
	s_digits[3] = *Message++;
}
