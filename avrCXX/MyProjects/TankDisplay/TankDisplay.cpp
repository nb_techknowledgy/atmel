/* ******************************************************************/
/* File Name    : TankDisplay.cpp                                   */
/* Description  : A program to monitor 8 temp sensor on a hot tank  */
/* Author       : Andrew Carney                                     */
/* Date         : 15 February 2021                                  */
/* Version      : First Version for Atmel devices V1.00             */
/* Copyright(c) 2021  Andrew Carney                                 */
/* License	: GPL v3.0 or later                                     */
/* ******************************************************************/
/* Modification :                                                   */
/* ******************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include "LCD.hpp"
#include "DS18X20.hpp"


LCD lcd(2,3,4,5,6,7,true);
int main()
{
	DS18B00 Device(&PORTB,2, false, 12);
	LCD lcd(2,3,4,5,6,7);
	ROM Probe[5] =	{{0x28, 0x89, 0x4d, 0xe5, 0x05, 0x00, 0x00, 0x9a },
					 {0x28, 0x2b, 0xec, 0x08, 0x06, 0x00, 0x00, 0x66 },
					 {0x28, 0x77, 0x3d, 0x08, 0x06, 0x00, 0x00, 0xa6 },
					 {0x28, 0x6c, 0x98, 0xe5, 0x05, 0x00, 0x00, 0xd4 },
					 {0x28, 0x52, 0x3f, 0x0a, 0x06, 0x00, 0x00, 0x7f }};
	uint8_t	Column[8] = { 0, 0, 0, 0, 11, 11, 11, 11 };
	uint8_t	Row[8] = { 0, 1, 2, 3, 0, 1, 2, 3 };
	uint8_t CharMap[8] = { 7, 5, 7, 0, 0, 0, 0, 0 };
	char buff[30];
	DDRB = 3;
	PORTB = 1;
	lcd.InitDisplay();
	lcd.Display(true);
	lcd.Clear();
	lcd.CustomChar(2,CharMap);
	do
	{
		Device.StartMeasurement(true);	// measurement delay is done here
		for (uint8_t p=0;p<5;p++)
		{
			float Celsius;
			char strC[10];
			Celsius = Device.ReadCelsius(Probe[p],true);
			if ((Celsius >0) && (Celsius < 99))	dtostrf(Celsius,3,2,strC);
			else	strcpy(strC,"--.--");
			lcd.SetCursor(Column[p],Row[p]);
			sprintf(buff,"%i:%4s\2C",p,strC);
			lcd.Write(buff);
		}
	} while (true);
	return 0;
}
