/* ****************************************************************/
/* File Name    : A2D.hpp                                         */
/* Description  : Provides basic access to the  Atmega ADC        */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __A2D_HPP
#define __A2D_HPP

#include "Timer.hpp"

class A2D : public Timer
{
	public:
		A2D(uint8_t Mask = 255);
		~A2D();
		void	SetChannel(uint8_t Channel);
		void	Start(int8_t Channel = -1);
		void	Disable(void);			//	disable ADC
		int16_t	GetValue(uint8_t Channel);
		void	Callback();
	private:
		void 	StartConversion(void *pData);
		volatile uint16_t	m_Value[8];
		volatile uint8_t	m_Channel;
		int8_t 				m_TimerChan;
};


#endif	/* end of #ifdef __A2D_HPP		*/
