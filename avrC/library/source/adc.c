/* ****************************************************************/
/* File Name    : adc.c                                           */
/* Description  : Provides basic access to the  Atmega ADC        */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <string.h>
#include "timer.h"
#ifdef DEBUG_ADC
#include "serial.h"
#endif

volatile static uint16_t	u16ADCvalue[8];
volatile static uint8_t		u8ADCchan;
#define MAX_CHANNELS		(8)
static int8_t i8_TimerChan = -1;

/** ********************************************************************
 *	\brief		Initialise the ADC regsiters
 *	\param		u8MuxMask	not used
 **********************************************************************/
void ADC_Init(uint8_t u8MuxMask)
{
	memset((void*)u16ADCvalue,0, sizeof(uint16_t)*MAX_CHANNELS);
	i8_TimerChan = TIMER_InitTimer();
    // Select Vref=AVcc and set left adjust result
    ADMUX |= (1<<REFS0);	//	|(1<<ADLAR);
    //	set prescaller to 128
    //	enable ADC interupt
    //	and enable ADC
    ADCSRA = ( 1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);	// pre-scaler = 128
    ADCSRB = 0;
}

/** ********************************************************************
 *	\brief		Selects the channel to use for conversion
 *	\param		ADCchannel
 **********************************************************************/
void ADC_SetChannel(uint8_t ADCchannel)
{
    ADMUX = (ADMUX & 0xF0) | (ADCchannel & 0x0F);	//	select ADC channel with safety mask
}

/** ********************************************************************
 *	\brief		Selects the ADC channel to be used, and starts the conversion.
 *	\param		pData
 *				if pData == NULL, selects next ADC channel in the mux
 *				and starts the convertsion
 *				if pData != NULL, it is assumned to be a pointer to 
 *				a uint8_t value for the required channel
 **********************************************************************/
static void StartConversion(void *pData)
{
	if (pData)
	{
		u8ADCchan = *((uint8_t*)(pData));
	}
	else
	{
		u8ADCchan++;
	}
	if (u8ADCchan > 7) u8ADCchan = 0;
    u16ADCvalue[u8ADCchan] = ADCW;	//	ADCL + (ADCH << 8);
	ADC_SetChannel(u8ADCchan);
	ADCSRA |= (1<<ADEN) | (1<<ADSC);
}

/** ********************************************************************
 *	\brief		Start the ADC operating for continuous conversion.
 *				if Channel is specfied always convert specified channel
 *				if not, set m_pData = NULL to cause the ADC channels
 *				to be selected in turn for conversion
 *	\param		pu8Channel -- if NULL read channels in turn
 **********************************************************************/
void ADC_Start(uint8_t *pu8Channel)
{
	TIMER_StartTimer(i8_TimerChan,10,StartConversion,pu8Channel);
}

/** ********************************************************************
 *	\brief		Stops the ADC.
 * 				Disables the ADC in the hardware
 **********************************************************************/
void ADC_Disable(void)			//	disable ADC
{
	ADCSRA &= ~((1<<ADEN));
}

/** ********************************************************************
 *	\brief		Gets the most recent ADC value for the specified channel.
 * 				This DOES NOT causes a a2d conversion !
 *				It simply reads from the stored data the last a2d value
 *				for the specified channel
 *	\return		10 bits of conversion in a 16 bit int
 **********************************************************************/
int16_t ADC_GetValue(uint8_t u8Chan)
{
	return (u16ADCvalue[u8Chan]);
}
