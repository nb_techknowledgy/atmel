/* ****************************************************************/
/* File Name    : Buffer.cpp                                      */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY{

 without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <string.h>
#include "Buffer.hpp"

/** ********************************************************************
 *	\brief		Initialises the object data to a known value
 *	\return		Nothing
 **********************************************************************/
Buffer::Buffer()
{
	Flush();
}

/** ********************************************************************
 * \brief		Places item in the FIFO queue.

 *				Copies item into the buffer and points to next free location
 *	\param		item	-- data to be placed in the FIFO
 *	\return		TRUE on success, FASlE if buffer FULL
 **********************************************************************/
bool 	Buffer::Push(const char item)
{
	if (Full())	return false;	//	 m_Tail = (m_Tail+1) % BUFF_SIZE;
	m_Buffer[m_Head] = item;
	m_Head = (m_Head+1) % BUFF_SIZE;
	return true;
}

/**  *******************************************************************
 *	\brief		Copies multiple bytes into the buffer.

 *				Calls into the basic Push() method to copy each byte
 * 				of 'buffer'  into the  FIFO.
 *	\param		buffer		data to be placed in the fifo
 *	\param		len			length of data to place in the fifo
 *	\return		The number of bytes written, which will be less than
 * 				requested if the buffer becomes full.
 **********************************************************************/
uint8_t	Buffer::Push(const char *buffer,uint8_t len)
{
	uint8_t l = 0;
	do
	{
		if (!Push(*(buffer)))	break;	// exit early as buffer full
		buffer++;
		l++;
	}	while (len--);
	return l;
}

/** ********************************************************************
 *	\brief		Retrieve the next item from the buffer.

 * 				If there is data it retrieves it and returns it in 'item'
 * 				NOTE -- it does not return it in the return value
 * \param[out]	item	--	contains data on success, undefined on fail
 * \return		TRUE if the data is valid
 * 				FALSE if the buffer is empty.
 **********************************************************************/
bool	Buffer::Pull(char &item)
{
	bool result = false;
	if ( !Empty() )
	{
		item = m_Buffer[m_Tail];
		m_Tail = ((m_Tail+1) % BUFF_SIZE);
		result = true;
	}
	return (result);
}

/** ********************************************************************
 *	\brief	Retrieve up to bufflen entries from the buffer.

 *			Uses the basic Pull() method to retrieve each item
 *			for up to 'bufflen' items.
 *			Data is returned in the calling parameter 'buffer'
 *			return value is the number of valid bytes returned
 *	NOTE	--	If the 'buffer' parameter contains data, up to 'bufflen'
 *			bytes will be overwritten, but subsequent bytes will
 *			remain untouched
 *			DO NOT expect the 'buffer'  parameter to have the remaining
 *			data cleared !
 *	\param[out]	buffer 		output parameter
 *	\param[in]	bufflen 	length of the supplied buffer
 *	\return		the length of the data returned.
 **********************************************************************/
uint8_t	Buffer::Pull(char *buffer,uint8_t bufflen)
{
	uint8_t len = 0;
	while ( (!Empty()) && (len < bufflen))
	{
		*(buffer++) = m_Buffer[m_Tail];
		m_Tail = ((m_Tail+1) & (BUFF_SIZE-1));
		len++;
	}
	return len;
}

/** ********************************************************************
 *	\brief		Initialise the object to initial state

	Resets all the indexes, and initialise the data store to all zeros
 *	\return		Nothing
 **********************************************************************/
void	Buffer::Flush()
{
	m_Head = 0;
	m_Tail = 0;
	memset(m_Buffer,0,BUFF_SIZE);
}
