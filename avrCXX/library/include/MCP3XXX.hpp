/* ****************************************************************/
/* File Name    : MCP3XXX.hpp                                     */
/* Description  :                                                 */
/*                                                                */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef	__MCP3XXX_HPP
#define	__MCP3XXX_HPP

#include "SPI.hpp"

template<
	uint8_t Bits,
	uint8_t Channels,
	uint32_t ClockSpeed,
	uint8_t TransferLength = 3
	>

//	As MCP3XXX chips use an SPI inteface, inherit the SPI class
class MCP3XXX : public SPI
{
	public:
		size_t numChannels()	const	{	return NUM_CHANNELS;	}
		size_t numBits()		const	{	return NUM_BITS;		}
		MCP3XXX()	: SPI(250000,MSB_FIRST,SPI::SPI_MODE0)
		{
			// Only here to facilitate calling the base class constructor
		}
		uint16_t analogRead(uint8_t Chan)
		{
			if (Chan < NUM_CHANNELS)
				return Read(Chan,SINGLE_ENDED);
			return (0);
		}
		uint16_t analogReadDifferential(uint8_t Chan) const
		{
			if (Chan < NUM_CHANNELS)
				return Read(Chan, DIFFERENTIAL);
			return (0);
		}
	private:
		enum	{	SINGLE_ENDED = false, DIFFERENTIAL = true	};
		enum	{	NUM_BITS = Bits,
					NUM_CHANNELS = Channels,
					CLOCK_SPEED = ClockSpeed,
					TRANSFER_LENGTH = TransferLength
				};
		uint16_t Read(uint8_t Chan, bool Differential)
		{
			uint8_t DataIn[TRANSFER_LENGTH];		//	Data sent to MOSI
			uint8_t DataOut[TRANSFER_LENGTH];		//	data recd from Slave (MISO)
			if (10 == NUM_BITS)
			{
				// Start/SGL and channel bits are XXXXXXXS DCCCXXXX XXXXXXXX
				//	Where S = start bit ( = 1 )
				//			D	= SGL/Diff ( 1 = Single ended, 0 = Differential )
				//			CCC = Channel selection in D2,D1,D0 format
				DataIn[0] = 1;
				DataIn[1] |= (Chan << 4);
				if (!Differential) DataIn[1] |= 0x80;
				DataIn[2] = 0;
				Enable();
				Transfer(DataIn,DataOut,TRANSFER_LENGTH);
				Disable();
				return  ((DataOut[1] & 3) * 256) + DataOut[2];
			}
			if (12 == NUM_BITS)
			{
				// Start/SGL and channel bits are SDCCCXXX XXXXXXXX XXXXXXXX
				//	Where S = start bit ( = 1 )
				//			D	= SGL/Diff ( 1 = Single ended, 0 = Differential )
				//			CCC = Channel selection in D2,D1,D0 format
				DataIn[0] = 4;		//	Start bit
				if (!Differential) DataIn[0] |= 2;
				DataIn[0] |= (Chan >> 2);
				DataIn[1] = (Chan &  3) << 6;
				Enable();
				Transfer(DataIn,DataOut,TRANSFER_LENGTH);
				Disable();
				return ((DataOut[1]&15)*256) + DataOut[2];
			}
		}
};

typedef MCP3XXX	<10, 2, 1200000, 2>	MCP3002;
typedef MCP3XXX	<10, 4, 1350000>	MCP3004;
typedef MCP3XXX	<10, 8, 1350000>	MCP3008;
typedef MCP3XXX	<12, 2, 900000>		MCP3202;
typedef MCP3XXX	<12, 4, 1000000>	MCP3204;
typedef MCP3XXX	<12, 8, 1000000>	MCP3208;

#endif  //		of #ifndef __MCP3XXX_HPP
