/* ******************************************************************/
/* File Name    : SolarController.c                                 */
/* Description  :                                                   */
/* Author       : Andrew Carney                                     */
/* Date         : 16th February 2021                               	*/
/* Version      : First Version V1.00                               */
/* Copyright (c) Andrew Carney                                      */
/* ******************************************************************/
/* Modification :	                                                */
/* ******************************************************************

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
************************************************************************/
#include <stdio.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

#include "hwtimer.h"
#include "timer.h"
#include "pwm.h"
#include "serial.h"
#include "adc.h"

#define	DEBUG_LEVEL	(1)
#ifdef DEBUG_LEVEL
// #define DEBUG_ADC

#define DEBUG_PRINT(x,y,z)	if (DEBUG_LEVEL >= z) { SERIAL_SendMessageInt(x,y); }
#else
#define DEBUG_PRINT(x,y,z)	while (0) { ; }
#endif

#ifndef FALSE
#define FALSE	(0)
#endif
#ifndef TRUE
#define TRUE	(!FALSE)
#endif

void HeartBeatFN(void *pData)
{
	PORTB ^= (32);					// flashes RED led on Arduino NANO !
#if defined(DEBUG) && defined(DEBUG_ADC)
	int k;
	for (k=0; k<5; k++)
	{
		char pBuff[30];
		uint16_t u16_Value = ADC_GetValue(k);

		memset(pBuff, 0, 30);
		sprintf(pBuff, "ADC Chan %i = %i\r\n",k,u16_Value);
		DEBUG_PRINT(pBuff,0,4);
	}
#endif

/*
 * if 'SERIAL_SendMessageInt()' prints garbage
 * and 'SERIAL_SendByte()' prints OK
 * then objcopy is missing a "-j data" option
 */
//	SERIAL_SendMessageInt("Heartbeat\n",10);
//	SERIAL_SendByte('U');
}

void SerialHandler(uint8_t ReadChar, void *pData)
{
	char* pCmd = (char*)pData;
	if (('\r' == ReadChar) || ('\n' == ReadChar))
	{
		;	//	pCmd->u8ProcessCommand = TRUE;
	}
	else
	{
//		pCmd->CommandString[pCmd->u8InPtr] = ReadChar;
//		pCmd->u8InPtr++;
//		if (pCmd->u8InPtr >= CMD_BUFFLEN-1)
//		{
//			memset(pCmd->CommandString,0,CMD_BUFFLEN);
//			pCmd->u8InPtr = 0;
//		}
		;
	}
	SERIAL_SendByte(ReadChar);
}

int main(int argc, char** argv)
{
	uint8_t HeartBeatTimer = -1;

	TIMER_Init(Timer1);	// MUST BE FIRST !!
	SERIAL_InitSerial(9600,16000000L);
	ADC_Init(0xFF);
	SERIAL_SetReceiveHandler(SerialHandler,&CommandBuffer);

	HeartBeatTimer	=	TIMER_InitTimer();
	if (-1 != HeartBeatTimer)
	{
		TIMER_StartTimer(HeartBeatTimer,125,HeartBeatFN,NULL);
	}

	ADC_Start(NULL);
	for(;;)
	{
		;
	}	// end of 'for(;;)'
	return 0;
}
