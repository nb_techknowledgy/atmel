/* ****************************************************************/
/* File Name    : OneWireTest.cpp                                 */
/* Description  : Test application for the OneWire util/library   */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#include "Timer.hpp"
#include "DS18X20.hpp"
#include "OneWire.hpp"
#include "LCD.hpp"
#include "Serial.hpp"
//======================================================================
//#define DEBUG
#define MARKER_PIN (1<<2)
#define MARKER_PORT PORTD
#define	MARKER_DIRECTION DDRD
#ifdef DEBUG
#define START	MARKER_PORT &= ~MARKER_PIN
#define FINISH	{	MARKER_PORT |= MARKER_PIN;	this->Delay(1500);	}
#else
#define START	while (0) {}
#define FINISH	while (0) {}
#endif
//======================================================================

//	This uses the Timer class to provide a looping test of the four basic
//	protocol commands -- this allows tiuming measurements to be taken
//	using a logic analyser, or oscilloscope.
//	See the "Callback()" method for details
class TimingTest :  public OneWire, public Timer
{
	public:
		TimingTest(volatile uint8_t *pPort, uint8_t pin, bool parasitic) :
			OneWire(pPort,pin,parasitic)
			{
				StartTimer(250,NULL);
			}
	private:
		void	Callback();
		uint8_t myRom[8];
		OneWire MyDevice();
};

void	TimingTest::Callback()
{
#ifdef PULLUP_TESTS
	bool pullup = Pullup();
	Pullup(!pullup);
	if (Pullup())
	{
		DDRB |= 2;
		PORTB |= 2;
	}
	else
	{
		DDRB &= ~2;
		PORTB |= 2;
	}
#endif
	START;
	this->Reset();
	FINISH;
	START;
	this->ReadBit();
	FINISH;
	START;
	this->WriteBit(false);
	FINISH;
	START;
	this->WriteBit(true);
	FINISH
}

void CheckTiming()
{
	MARKER_DIRECTION |= MARKER_PIN;
	bool pullup = false;
	TimingTest timing(&PORTB, 2, pullup);
	while (true);
	do
	{
//		ROM	Rom = {1,2,3,4,5,6,7,8};
		OneWire Device(&PORTB,0, pullup);
		Device.Read();
	} while (true);
}
//	********************************************************************
void FindDevice()
{
	ROM MyRom = { 0,0,0,0,0,0,0,0 };
	OneWire Device(&PORTB,2, false);
	LCD lcd(2,3,4,5,6,7);
	Serial Uart(9600,0,8,1,false);
	lcd.InitDisplay();
	lcd.Display(true);
	uint8_t line = 0;
	do
	{
		do
		{
			if (Device.FindNext(MyRom))
			{
				lcd.SetCursor(0,line);
				line++;
				if (line > 3) line = 0;
				lcd.Write("ROM=");
				for (uint8_t i=0; i<8; i++)
				{
					char buff[30];
					sprintf(buff,"%02x",MyRom[i]);
					Uart.Write(buff,0);
					lcd.Write(buff);
				}
			}
			else
			{
				Uart.Write("NOT FOUND");
				lcd.Write("NOT FOUND");
			}
			lcd.DelayMs(200);
		} while (!Device.LastDevice());
		Device.FindFirst(0,MyRom);
	} while (true);

}
//	********************************************************************
void Check_DS1820()
{
	ROM Probe[6];
	uint8_t k = 0;
	DelayTimer Time;
	DS18B00 Device(&PORTB,2, false, 12);
	Device.PollComplete(false);
	Serial Uart(9600,0,8,1,false);
	Uart.Write("Check_DS1820 -- \r\n",0);
	while (!Device.LastDevice())
	{
		Uart.Write("Searching...\r\n");
		if (Device.FindNext(Probe[k]))	{	k++;	}
	}
	do
	{
		char buff[30];
		for (uint8_t p=0;p<k;p++)
		{
			uint16_t rawvalue = 0;
			float Celsius,Fahrenheit;
			char strC[10];
			char strF[10];
			Uart.Write("Probe -- ",0);
			for (uint8_t i=0; i<8; i++)
			{
				sprintf(buff,"%02x",Probe[p][i]);
				Uart.Write(buff,0);
			}
			Uart.Write(" --> ",0);
			Device.SetAddress(Probe[p]);
			Device.StartMeasurement();
			rawvalue = Device.Read();
			Celsius = Device.ReadCelsius();
			Fahrenheit = Device.ReadFahrenheit(true);
			dtostrf(Celsius,3,1,strC);
			dtostrf(Fahrenheit,3,1,strF);
			sprintf(buff,"\tRaw = %04i  %sC -- %sF\r\n",rawvalue/16,strC, strF);
			Uart.Write(buff,0);
/*
			sprintf(buff,"%02x,%02x,%02x,%02x,%02x,%02x,%02x,%02x,%02x\r\n",
				Device.m_ScratchRAM[0],
				Device.m_ScratchRAM[1],
				Device.m_ScratchRAM[2],
				Device.m_ScratchRAM[3],
				Device.m_ScratchRAM[4],
				Device.m_ScratchRAM[5],
				Device.m_ScratchRAM[6],
				Device.m_ScratchRAM[7],
				Device.m_ScratchRAM[8]);
			Uart.Write(buff,0);
*/		}
//		Uart.Write("=============\r\n");
	} while (true);
}


void DisplayTemp()
{
	uint8_t charmap[8] = { 7, 5, 7, 0, 0, 0, 0, 0 };
	DS18B00 Device(&PORTB,2, false, 12);
	LCD lcd(2,3,4,5,6,7);
	lcd.InitDisplay();
	lcd.CustomChar(2,charmap);
	lcd.Clear();
	lcd.Display(true);
	ROM Probe[5] =	{{0x28, 0x89, 0x4d, 0xe5, 0x05, 0x00, 0x00, 0x9a },
					 {0x28, 0x2b, 0xec, 0x08, 0x06, 0x00, 0x00, 0x66 },
					 {0x28, 0x77, 0x3d, 0x08, 0x06, 0x00, 0x00, 0xa6 },
					 {0x28, 0x6c, 0x98, 0xe5, 0x05, 0x00, 0x00, 0xd4 },
					 {0x28, 0x52, 0x3f, 0x0a, 0x06, 0x00, 0x00, 0x7f }};
	char buff[30];
	do
	{
		Device.StartMeasurement(true);	// measurement delay is done here
		for (uint8_t p=0;p<5;p++)
		{
			float Celsius;
			char strC[10];
			Celsius = Device.ReadCelsius(Probe[p],true);
			dtostrf(Celsius,3,2,strC);
			switch(p)
			{
				case 0:
					lcd.SetCursor(0,0);
					break;
				case 1:
					lcd.SetCursor(0,1);
					break;
				case 2:
					lcd.SetCursor(0,2);
					break;
				case 3:
					lcd.SetCursor(0,3);
					break;
				case 4:
					lcd.SetCursor(10,0);
					break;
			}
			sprintf(buff,"%i:%5s\2C",p,strC);
			lcd.Write(buff);
		}
	} while (true);
}

int main(int argc, char **argv)
{
#ifdef CHECK_TIMING
#warning Doing Timing Checks
	CheckTiming();		//	check pulse timings on the bus
#endif
#ifdef CHECK_FINDDEVICE
#warning Doing "FindDevice()"
	FindDevice();		//	performa device search
#endif
#ifdef CHECK_TEMPERATURE
#warning Doing "Check_DS1820"
	Check_DS1820();		//	test using temperature probe(s)
#endif
#ifdef LCD_CHECK
#warning Doing "Check_DS1820 on LCD"
	DisplayTemp();
#endif
#if !defined(CHECK_TIMING) && !defined(CHECK_FINDDEVICE) && !defined(CHECK_TEMPERATURE) && !defined(LCD_CHECK)
#error "NO TEST DEFINED"
#endif
	return (0);
}
