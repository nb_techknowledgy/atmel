/* ****************************************************************/
/* File Name    : serial.c                                        */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "serial.h"

#define BAUD_PRESCALER(speed,cpu_clk) (((cpu_clk / (speed * 16UL))) - 1)    //The formula that does all the required maths
//#define USER_BAUD
#define	BUFF_SIZE	(255)	// maximum that it can be with a one byte counter !

static pRECEIVE_FN pReceiveFN;
static pTRANSMIT_FN pTransmitFN;
static void   *pReceiveData;

typedef struct tagSerialBuffer
{
	uint8_t u8Sent;
	uint8_t u8SendLen;
	uint8_t pSendBuffer[BUFF_SIZE];
} tSERIAL_BUFFER, *pSERIAL_BUFFER;

static tSERIAL_BUFFER tBuffer;

static int8_t TransmitHandler(uint8_t *Char)
{
	if ((tBuffer.u8SendLen >0) && (tBuffer.u8Sent < tBuffer.u8SendLen))
	{
		*Char = tBuffer.pSendBuffer[tBuffer.u8Sent];
		tBuffer.u8Sent++;
		return (0);
	}
	else
	{
		UCSR0B &= ~(1 << TXCIE0);		// disable transmit interrupts
	}
	return (1);
}

inline static void IntReceiveByte(void *pData)
{
	uint8_t Char = UDR0;
	if (pReceiveFN)
	{
		pReceiveFN(Char,pReceiveData);
	}
	// if no receive handler, char is discarded
}

inline static void IntTransmitByte()
{
	uint8_t TxChar = 0;
	if (pTransmitFN)
	{
		if (0 == pTransmitFN(&TxChar))
		{
			// if we are here, transmit interrupts should already be enabled.
			UDR0 = TxChar;
			return;
		}
		else
		{
			// tun off tranmsit interrupt.
			UCSR0B &= ~(1 << TXCIE0);
		}
		return;
	}
}

inline static void TransmitComplete()
{
//	pTransmitFN = NULL;
	UCSR0B &= ~(1 << TXCIE0);
//	SERIAL_SendByte('.');
}

void SERIAL_InitUart(uint16_t speed, uint32_t cpu_clk)
{
#ifdef USER_BAUD
	UBRR0L = (uint8_t)(BAUD_PRESCALER(speed,cpu_clk));
	UBRR0H = (uint8_t)(BAUD_PRESCALER(speed,cpu_clk)>>8);
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	UCSR0C = ((1<<UCSZ00)|(1<<UCSZ01));
#else
	UBRR0L = 103;
	UBRR0H = (103>>8);
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UCSR0C = ((1<<UCSZ00) | (1<<UCSZ01));
#endif
}

void SERIAL_InitSerial(uint16_t speed, uint32_t cpu_clk)
{
	SERIAL_InitUart(speed,  cpu_clk);
	memset(tBuffer.pSendBuffer,0,BUFF_SIZE);
	tBuffer.u8SendLen = 0;
	tBuffer.u8Sent = 0;
	pReceiveFN = NULL;		// make sure read callback is something sensible at startup !
	pTransmitFN = NULL;		// ditto for transmitting
	pReceiveData = NULL;
	SERIAL_SetTransmitHandler(TransmitHandler);
}

void SERIAL_SetReceiveHandler(pRECEIVE_FN pFN, void *pData)
{
	if (pFN)
	{
		pReceiveFN = pFN;
		pReceiveData = pData;
		UCSR0B |= (1 << RXCIE0);	// enable receive interupt
	}
	else
	{
		pFN = NULL;
		pReceiveData = NULL;
		UCSR0B &= ~(1 << RXCIE0);	// disable receive interupt
	}
}

void SERIAL_SetTransmitHandler(pTRANSMIT_FN pFN)
{
	if (pFN)
	{
		pTransmitFN = pFN;
		UCSR0B |= ((1 << TXCIE0) | (1 << UDRIE0));
	}
	else
	{
		pFN = NULL;
		UCSR0B &= ~(1 << TXCIE0);
	}
}

uint8_t SERIAL_GetByte(void)
{
	while(!(UCSR0A & (1<<RXC0)));
	return UDR0;
}

uint8_t SERIAL_SendByte(char Char)
{
    uint16_t i;
    for(i = 0; !(UCSR0A & (1 << UDRE0)); i++)		//	Wait for empty transmit buffer
    {
        if( i > 2500 )								//	How long one should wait
            return -1;								//	Give feedback to function caller
    }
    UDR0 = Char;									//	Start transmission
    return (0);										//	return value
}

void SERIAL_SendMessagePoll(const char *pu8Message, uint8_t u8Len)
{
	if (0 == u8Len)	u8Len = strlen(pu8Message);
	int8_t u8_k;
	for (u8_k=0;u8_k<u8Len;u8_k++)
	{
		SERIAL_SendByte(pu8Message[u8_k]);
	}
}

int8_t SERIAL_SendMessageInt(const char *pu8Message, uint8_t u8Len)
{
	if (0 == u8Len)	u8Len = strlen(pu8Message);
	memcpy(tBuffer.pSendBuffer,pu8Message,u8Len);
	tBuffer.u8SendLen = u8Len;
	tBuffer.u8Sent = 0;

	UCSR0B |= ((1 << TXCIE0) | (1 << UDRIE0));		// ENABLE TX INTS ....
	return (0);
}

void	SERIAL_Hexdump(void *pBuff, uint16_t u16Len, uint8_t u8Width)
{
	uint8_t k;
	uint16_t u16Address = 0;
	uint8_t *pChar = (uint8_t*)pBuff;
	char pPrintBuff[100];
	memset(pPrintBuff,0,100);
	do
	{
		sprintf(pPrintBuff,"%04x: ",u16Address);
		for (k=0;k<u8Width;k++)
		{
			if (u16Address+k < u16Len)
				sprintf(pPrintBuff,"%s[%02x]", pPrintBuff, pChar[u16Address+k]);
		}
		u16Address += u8Width;
		sprintf(pPrintBuff,"%s\n",pPrintBuff);
		SERIAL_SendMessagePoll(pPrintBuff,strlen(pPrintBuff));
		memset(pPrintBuff,0,100);
	} while (u16Address < u16Len);
}



ISR(USART_RX_vect)		{ IntReceiveByte(pReceiveData); }
ISR(USART_UDRE_vect )	{ IntTransmitByte(); }
ISR(USART_TX_vect )		{ TransmitComplete(); }
