#==================================================
# AVR-GXX Makefile
#==================================================
# File         : rules.inc
# Author       : Andrew Carney
# Date         : 16th February 2021
# Version      : First Version for Atmel devices V1.00
# Copyright(c) 2021  Andrew Carney
# License	: GPL v3.0 or later
# **********************************************************************
# Modification :
# **********************************************************************
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>
# **********************************************************************
#
#=====================================================
OBJECTS	=	$(SOURCES:.c=.o)
DEPENDS	=	$(SOURCES:.c=.d)
#=====================================================
$(TARGET).hex:	$(TARGET).eep
$(TARGET).eep:	$(OBJECTS)

%.out: $(OBJECTS)
	$(CXX) $(LD_FLAGS) -o $@ $^
%.hex:	%.out
	$(OBJCOPY) -j .text -j .data -O ihex $< $@
%.eep:	%.out
	$(OBJCOPY) -j .eeprom        -O ihex $< $@
#=====================================================
