/* ****************************************************************/
/* File Name    : SPItest.cpp                                     */
/* Description  : A program to test a both an LCD display and a   */
/*                analogue to digital converter connected via the */
/*                hardware SPI bus on an atmega at328p as used on */
/*                the Arduino Uno and Nano                        */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License	: GPL v3.0 or later                                   */
/* ****************************************************************/
/* Modification :	                                              */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <stdio.h>			//	needed to use sprintf()
#include "MCP3XXX.hpp"
#include "LCD.hpp"

LCD	lcd(2,3,4,5,6,7);
MCP3208 adc;

int main(int argc, char **argv)
{
	char buffer[20];
	DDRB |= 3;
	PORTB |= 1;

	lcd.InitDisplay();
	lcd.Display(true);
	lcd.SetCursor(20,2);
	lcd.Write("Testing");
	do
	{
		uint16_t Reading;
		lcd.Clear();
		for (int k=0;k<8;k++)
		{
			if (k < 4)	{	lcd.SetCursor(0,k);		}
			else		{	lcd.SetCursor(10,k-4);	}
			Reading = adc.analogRead(k);
			sprintf(buffer,"CH%i = %04i", k, Reading);
			lcd.Write(buffer);
		}
		lcd.DelayMs(1000);
		lcd.Clear();
	} while (true);
	return 0;
}

