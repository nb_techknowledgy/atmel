/* ****************************************************************/
/* File Name    : OneWire.hpp                                     */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Delay() changed to LoopDelay(0 to remove        */
/*                conflict with Delay in DelayTimer.hpp           */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __ONEWIRE_HPP
#define __ONEWIRE_HPP

#include <avr/io.h>
#include <stdint.h>
/*
	====================================================================
	INFORMATION --
	This library is a re-write of the Arduino library of the same name.
	ALL timings have been optimised according to the Dallas One Wire spec.
	This utility/library is intended to be used outside of the Arduino
	environment for those who want to "do it properly".
	To provide SOME flexibilty, a compromise has been made to allow a choice
	of which I/O pins gets used when the constructor is defined/called.
	This has the added side effect of being able to have multiple instances
	running on different I/O pins.
	To revert to a dedicated coded I/O pin define DEBUG, and
	set the port and pin mask accordingly.
	====================================================================
*/
typedef uint8_t ROM[8];

class OneWire
{
	public:
		// -------------------------------------------------------------
		OneWire(volatile uint8_t *pPort, uint8_t Pin, bool NoPullup = true);
		~OneWire() {;}
		// -------------------------------------------------------------
		void		Pullup(bool pullup);
		bool		Pullup()			{	return m_PullupPresent;		}
		void		Reset();
		bool 		FindFirst(uint8_t family, ROM& rom);
		bool		FindNext(ROM& rom);
		void		Select(const ROM rom);
		void		SkipRom()					{	Write(0xCC);			}
		uint8_t		LastDevice()				{	return m_LastDevice;	}
		void		LastDevice(uint8_t last)	{	m_LastDevice = last;	}
		// -------------------------------------------------------------
		uint8_t 	Read();
		uint8_t		Read( char *buffer, uint8_t length);
		// -------------------------------------------------------------
		void		Write(uint8_t byte);
		uint8_t		Write(const char *buffer,	uint8_t length);
		void		Write(const ROM& rom, uint8_t byte)	\
						{	Reset(); Select(rom); Write(byte);	}
		void		Write(const ROM& rom, const char *buffer,	uint8_t length)	\
						{	Reset(); Select(rom); Write(buffer,	length);	}
		// -------------------------------------------------------------
		uint8_t		Crc8(const uint8_t *data, uint8_t length);
		uint16_t	Crc16(const uint8_t* input, uint16_t len, uint16_t crc);
		// -------------------------------------------------------------
	protected:
		// -------------------------------------------------------------
		void	WriteBit(bool bit);
		bool	ReadBit();
		// -------------------------------------------------------------
	private:
		// -------------------------------------------------------------
		ROM		m_Rom;
		inline	void	PinLo()	__attribute__((always_inline));
		inline	void	PinHi()	__attribute__((always_inline));
		void	LoopDelay(uint16_t delay);		// implements approximate uS delays .. based on 16MHz xtal clock
		// -------------------------------------------------------------
		uint8_t	m_LastConflict = 0;			//	bit index into last device conflict
		uint8_t	m_LastFamilyConflict = 0;
		uint8_t	m_LastDevice = 0;
		// -------------------------------------------------------------
		volatile uint8_t	*m_pPort;
		volatile uint8_t	*m_pDirection;
		volatile uint8_t	*m_pPin;
		uint8_t				m_Pin;
		bool				m_PullupPresent;
		// -------------------------------------------------------------
};

#endif		//	end of #ifndef__ONEWIRE_HPP
