/* ****************************************************************/
/* File Name    : HwTimer.cpp                                     */
/* Description  : Provides basic functional control over the      */
/*                Atmega timers                                   */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>

#include "HwTimer.hpp"

//#define HWTIMER_DEBUG
#ifdef HWTIMER_DEBUG
#include "LedDebug.h"
#endif

#define HW_TIMER0_HANDLE	(0x11)
#define HW_TIMER1_HANDLE	(0x22)
#define HW_TIMER3_HANDLE	(0x33)

pHWTIMER_FN HwTimer::s_pHandlerTimer0 = NULL;
pHWTIMER_FN HwTimer::s_pHandlerTimer1 = NULL;
pHWTIMER_FN HwTimer::s_pHandlerTimer2 = NULL;
uint8_t		HwTimer::s_TimerLock[3] = { 0,0,0 };


/* use SYS_CLK/2 as the PIT source clock */
//	====================================================================
//	if running out of memory, this can be placed in code space (PROGMEM)
//	BUT -- the code to access it will need to be updated !
const uint16_t	c_PreScaler[6] = { 0,1,8,64,256,1024 };
//	====================================================================

/** ********************************************************************
 *	\brief		Check if this timer is in use ( i.e. locked ).
 *	\param		eTimer.
 *	\return		How many things are using this timer.
 **********************************************************************/
uint8_t	HwTimer::IsTimerLocked(		eHW_TIMER eTimer)
{
	return s_TimerLock[eTimer];
}

/** ********************************************************************
 *	\brief		Acquires a lock on the specified timer.
 *	\param		eTimer.
 *	\return		Handle to use with this timer.
 **********************************************************************/
eHW_TIMERHANDLE HwTimer::AcquireTimer(eHW_TIMER eTimer)
{
	if (0 == s_TimerLock[eTimer])
	{
		s_TimerLock[eTimer]++;
		return (eHW_TIMERHANDLE)(eTimer << 4);
	}
	return (eHW_TIMERHANDLE)(0);
}

/** ********************************************************************
 *	\brief		Releases the lock on the specified timer
 *	\param		eTimerHandle
 *	\return		Nothing.
 **********************************************************************/
void HwTimer::ReleaseTimer(eHW_TIMERHANDLE eTimerHandle)
{
	s_TimerLock[eTimerHandle>>4] = 0;
}

/** ********************************************************************
 *	\brief		The actual interrupt function which will then call the
 *				registered handler ( if any ).
 *	\return		Nothing.
 **********************************************************************/
void HwTimer::DefaultHandler0(void)
{
	if (s_pHandlerTimer0)
		s_pHandlerTimer0();
	return;
}

/** ********************************************************************
 *	\brief		The actual interrupt function which will then call the
 *				registered handler ( if any ).
 *	\return		Nothing.
 **********************************************************************/
void HwTimer::DefaultHandler1(void)
{
	if (s_pHandlerTimer1)
		s_pHandlerTimer1();
	return;
}

/** ********************************************************************
 *	\brief		The actual interrupt function which will then call the
 *				registered handler ( if any ).
 *	\return		Nothing.
 **********************************************************************/
void HwTimer::DefaultHandler2(void)
{
	if (s_pHandlerTimer2)
		s_pHandlerTimer2();
	return;
}

uint8_t HwTimer::Calculate_TimerRegisters8bit(uint16_t frequency, uint8_t *prescaler)
{
	uint32_t	toc = 0;
	uint8_t		ps = 0;
	do
	{
		ps++;
		toc = ((SYS_CLK/(frequency*2))/c_PreScaler[ps])-1;
	} while (toc > 255);
	*prescaler = ps;
	return toc & 255;
}

/** ********************************************************************
 *	\brief		Calculates the values for the counter and the prescaler
 *				to provide as near as possible the required interval.
 *	\param		u16_uS			The required interval in microseconds.
 *	\param		*pu8prescaler	A pointer to variable to return
 *								prescaler value.
 *	\return		The value to place in timer counter register.
 **********************************************************************/
uint16_t HwTimer::Calculate_TimerRegisters16bit(uint32_t frequency, uint8_t *prescaler)
{
	uint32_t	toc = 0;
	uint8_t ps = 0;
	do
	{
		ps++;
		toc = (SYS_CLK/(frequency*2))/c_PreScaler[ps];
	} while (toc > 255);
	*prescaler = ps;
	return toc-1;
}

/** ********************************************************************
 *	\brief		Actually starts the PIT running.
 *	\param		eTimer	--	The timer to be started.
 *	\return		success or fail -- 1 == success / 0 = fail.
 **********************************************************************/
uint8_t	HwTimer::StartTimer(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			TIMSK0 |= (1 << OCIE0A);
			break;
		case Timer1:
			TIMSK1 |= (1 << OCIE1A);
			break;
		case Timer2:
			TIMSK2 |= (1 << OCIE2A);
			break;
		default:
		    return (0);
	}
	sei();
	return (1);
}

/** ********************************************************************
 *	\brief		Initialises specified timer registers with value
 *				calculated by IntervalUS_CalcPITRegisters().
 *	\param		eTimer		-- timer to be used.
 *	\param		u8PreScale	-- Value to use for prescaler register.
 *	\param		u16Interval -- Value for counter register.
 *	\return		success or fail -- 1 = success / 0 = fail.
***********************************************************************/
uint8_t	HwTimer::InitTimer(eHW_TIMER eTimer, uint8_t prescaler, uint16_t count)
{
#ifdef CHECK_TIMING
		LA_DIRPORT |= 0xFC;
#endif
		cli();									//	disable global interrupts
		switch(eTimer)
		{
			case Timer0:
				TCCR0A = 0; 					//	set entire TCCR0A register to 0
				TCCR0B = 0;						//	same for TCCR0B
				TCCR0A |= (1 << WGM01);			//	Clear timer on Compare (OK)
				TCCR0B &= ~7;					//	Clear prescaler bits before setting
				TCCR0B |= (prescaler & 7);		//	set prescaler and start the timer
				OCR0A = count;					//	set compare match register to desired timer count
				break;
			case Timer1:
				TCCR1A = 0; 					//	set entire TCCR0A register to 0
				TCCR1B = 0;						//	same for TCCR0B
				TCCR1A |= (1 << WGM11);
				TCCR1B &= ~7;					//	Clear prescaler bits before setting
				TCCR1B |= (prescaler & 7);		//	set prescaler and start the timer
				OCR1A = count;					//	set compare match register to desired timer count
				TCCR1B |= (1 << WGM12);
				break;
			case Timer2:
				TCCR2A = 0; 					//	set entire TCCR0A register to 0
				TCCR2B = 0;						//	same for TCCR0B
				TCCR2A |= (1 << WGM21);			//	Clear timer on Compare (OK)
				TCCR2B &= ~7;					//	Clear prescaler bits before setting
				TCCR2B |= (prescaler & 7);		//	set prescaler and start the timer
				OCR2A = count;					//	set compare match register to desired timer count
				break;
			default:
				sei();							//	enable global interrupts:
				return (0);
		}
		sei();									//	enable global interrupts:
		return (1);
//	}
	return (0);
}

/** ********************************************************************
 *	\brief		Initialises specified timer for the interval specified
 *				in u8IntervalUS in microseconds.
 *	\param		eTimer			-- timer to be used.
 *	\param		u16IntervalUS	-- Interval for timer in microseconds.
 *	\return		success or fail -- 1 = success / 0 = fail.
 **********************************************************************/
uint8_t	HwTimer::InitTimerUs(eHW_TIMER eTimer,  uint16_t u16IntervalUS)
{
	/* set up interval for PIT to u8IntervalUS microseconds */
	if (u16IntervalUS)
	{	/* just set up the timer regs, but do NOT start ! */
		uint8_t	u8PreScale = 2;
		uint16_t u16Count = 0xF9;
//		u16Count = IntervalUS_CalcTimerRegisters(u16IntervalUS,&u8PreScale);
		return (InitTimer(eTimer,u8PreScale,u16Count));
	}
	return (0);
}

/** ********************************************************************
 *	\brief		Stop operation of the specified PIT timer.
 *	\param		eTimer	-- timer to be used.
 *	\return		success or fail -- 1 = success / 0 = fail.
 **********************************************************************/
uint8_t	HwTimer::StopTimer(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			break;
		case Timer1:
			break;
		case Timer2:
			break;
		default:
		    return (0);
	}
	return (1);
}

/** ********************************************************************
 *	\brief		Gets the value of the specified PIT timer.
 *	\param		eTimer	-- timer to be used.
 *	\return		The value of the PIT counter register.
 **********************************************************************/
uint16_t	HwTimer::GetTimer(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			return (TCNT0);
		case Timer1:
			return (TCNT1);
		case Timer2:
			return (TCNT2);
		default:
		    return (0);
	}
}

/** ********************************************************************
 *	\brief		Enable interrupts on the specified PIT timer.
 *	\param		eTimer	--	timer to enable interrupts on.
 *	\return		Nothing.
 **********************************************************************/
void	HwTimer::EnableTimerInts(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			TIMSK0 |= (1 << OCIE0A);	// enable timer compare interrupt
			break;
		case Timer1:
			TIMSK1 |= (1 << OCIE1A);	// enable timer compare interrupt
			break;
		case Timer2:
			TIMSK2 |= (1 << OCIE2A);	// enable timer compare interrupt
			break;
		default:
		    return;
	}
}

/** ********************************************************************
 *	\brief		Disable interrupts on the specified PIT timer.
 *	\param		eTimer	--	timer to disable interrupts on.
 *	\return		Nothing.
 **********************************************************************/
void	HwTimer::DisableTimerInts(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			TIMSK0 &= (~(1 << OCIE0A));
			break;
		case Timer1:
			TIMSK1 &= (~(1 << OCIE1A));
			break;
		case Timer2:
			TIMSK2 &= (~(1 << OCIE2A));
			break;
		default:
		    return;
	}
}

/** ********************************************************************
 *	\brief		Set a function to be called when the timer generates an
 *				interrupt.
 *	\param		eTimer	-- timer to disable interrupts on.
 *	\param		pFN		-- The function to be called.
 *	\return		Nothing.
 **********************************************************************/
void	HwTimer::SetTimerHandler(eHW_TIMER eTimer, pHWTIMER_FN pFN)
{
	if (pFN)
	{
		switch(eTimer)
		{
			case Timer0:
				s_pHandlerTimer0 = pFN;
				break;
			case Timer1:
				s_pHandlerTimer1 = pFN;
				break;
			case Timer2:
				s_pHandlerTimer2 = pFN;
				break;
			default:
			    return;
		}
	}
	return;
}

/** ********************************************************************
 *	\brief		Gets the timer counter elapsed since the last timer
 *				event/interrupt.
 *	\param		eTimer	--	timer to get the elapsed time for
 *	\return		Calculated elapsed time interval.
 **********************************************************************/
uint16_t	HwTimer::GetTimerElapsedTime(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			return (TCNT0);
		case Timer1:
			return (0);
		case Timer2:
			return (0);
		default:
		    return (0);
	}
}

ISR(TIMER0_COMPA_vect)  // timer0 overflow interrupt
{
#ifdef HWTIMER_DEBUG
	DLED_TOGGLE(DLED_1);
	BLED_TOGGLE(BLED_1);
#endif
	SET_PIN(0,1);
	HwTimer::DefaultHandler0();//event to be executed here
	SET_PIN(0,0)
}

ISR (TIMER1_COMPA_vect)  // timer1 overflow interrupt
{
#ifdef HWTIMER_DEBUG
	DLED_TOGGLE(DLED_2);
	BLED_TOGGLE(BLED_2);
#endif
	SET_PIN(0,1);
	HwTimer::DefaultHandler1();//event to be executed here
	SET_PIN(0,0)
}

ISR (TIMER2_COMPA_vect)  // timer2 overflow interrupt
{
#ifdef HWTIMER_DEBUG
	DLED_TOGGLE(DLED_3);
	BLED_TOGGLE(BLED_3);
#endif
	SET_PIN(0,1);
	HwTimer::DefaultHandler2();//event to be executed here
	SET_PIN(0,0)
}

