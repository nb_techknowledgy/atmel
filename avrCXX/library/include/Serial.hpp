/* ****************************************************************/
/* File Name    : Serial.hpp                                      */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*		V1.01	                                                  */
/* 			Optimised for speed.                                  */
/* 			Interrupt routines removed from object code           */
/* 			Buffers & data indexes removed from object data space */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __SERIAL_HPP
#define __SERIAL_HPP

#define	BUFF_SIZE	(128)

extern char		ReceiveBuffer[BUFF_SIZE];
extern char		TransmitBuffer[BUFF_SIZE];
extern uint8_t	TxHead,TxTail;
extern uint8_t	RxHead,RxTail;

class Serial
{
	public:
		Serial(	uint16_t baud = 9600,
				uint8_t	bits = 8,
				bool	parity = false,
				uint8_t	stop = 1,
				bool	buffered = true);
				bool	Write(const char ch);
				uint8_t	Write(const char *buffer, uint8_t len = 0);
				bool	Read(char &item, bool waitfordata = false);
				uint8_t	Read(char *buffer, uint8_t bufflen, bool waitfordata = false);
		inline	bool	TxFull()		{	return (((TxHead+1) % BUFF_SIZE) == TxTail);	}
		inline	bool	TxEmpty()		{	return (TxHead == TxTail);						}
		inline	bool	RxFull()		{	return (((RxHead+1) % BUFF_SIZE) == RxTail);	}
		inline	bool	RxEmpty()		{	return (RxHead == RxTail);						}
		inline	void	StartTx()		{	UCSR0B |= ((1 << TXCIE0) | (1 << UDRIE0));		}
		inline	bool	Transmitting()	{	return 	((UCSR0B & (1 << TXCIE0)) > 0);			}

	private:
		bool	m_Buffered;

};

#endif	/* end of #ifdef __SERIAL_HPP	*/
