/* ******************************************************************/
/* File Name    : hwtimer.c                                   	    */
/* Description  : Provides a hardware timer using the Atmel timers  */
/*                Used by the TIMER module to provide multiple      */
/*                timers for use in time driven event handling      */
/* Author       : Andrew Carney                                     */
/* Date         : 15 February 2021                                  */
/* Version      : First Version for Atmel devices V1.00             */
/* Copyright(c) 2021  Andrew Carney                                 */
/* License      : GPL v3.0 or later                                 */
/* ******************************************************************/
/* Modification :                                                   */
/* ******************************************************************

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
**************************************************************************/
#include <avr/interrupt.h>
#include <stdio.h>	//	needed for definition of "NULL"
#include "hwtimer.h"

//#define HWTIMER_DEBUG
#ifdef HWTIMER_DEBUG
#include "LedDebug.h"
#endif

#define HW_TIMER0_HANDLE	(0x11)
#define HW_TIMER1_HANDLE	(0x22)
#define HW_TIMER3_HANDLE	(0x33)


/* use SYS_CLK_KHZ/2 as the PIT source clock */

static  pHWTIMER_FN pHandlerTimer0 = NULL;
static  pHWTIMER_FN pHandlerTimer1 = NULL;
static  pHWTIMER_FN pHandlerTimer2 = NULL;
static	uint8_t		TimerLock[3] = { 0,0,0 };

/** ********************************************************************
 *	\brief		Check if this timer is in use ( i.e. locked ).
 *	\param		eTimer.
 *	\return		How many things are using this timer.
 **********************************************************************/
uint8_t	u8HWTIMER_IsTimerLocked(		eHW_TIMER eTimer)
{
	return TimerLock[eTimer];
}

/** ********************************************************************
 *	\brief		Acquires a lock on the specified timer.
 *	\param		eTimer.
 *	\return		Handle to use with this timer.
 **********************************************************************/
eHW_TIMERHANDLE u8HWTIMER_AcquireTimer(eHW_TIMER eTimer)
{
	if (0 == TimerLock[eTimer])
	{
		TimerLock[eTimer]++;
		return (int8_t)(eTimer << 4);
	}
	return (0);
}

/** ********************************************************************
 *	\brief		Releases the lock on the specified timer
 *	\param		eTimerHandle
 **********************************************************************/
void u8HWTIMER_ReleaseTimer(eHW_TIMERHANDLE eTimerHandle)
{
	TimerLock[eTimerHandle>>4] = 0;
}

/** ********************************************************************
 *	\brief		The actual interrupt function which will then call the
 *				registered handler ( if any ).
 **********************************************************************/
void HWTIMER_DefaultHandler0(void)
{
	if (pHandlerTimer0)
		pHandlerTimer0(0);
	return;
}

/** ********************************************************************
 *	\brief		The actual interrupt function which will then call the
 *				registered handler ( if any ).
 **********************************************************************/
void HWTIMER_DefaultHandler1(void)
{
	if (pHandlerTimer1)
		pHandlerTimer1(0);
	return;
}

/** ********************************************************************
 *	\brief		The actual interrupt function which will then call the
 *				registered handler ( if any ).
 **********************************************************************/
void HWTIMER_DefaultHandler2(void)
{
	if (pHandlerTimer2)
		pHandlerTimer2(0);
	return;
}

/** ********************************************************************
 *	\brief		Calculates the values for the counter and the prescaler
 *				to provide as near as possible the required interval.
 *	\param		u16_uS			The required interval in microseconds.
 *	\param		*pu8prescaler	A pointer to variable to return
 *								prescaler value.
 *	\return		The value to place in timer counter register.
 **********************************************************************/
static uint16_t	IntervalUS_CalcTimerRegisters(uint16_t u16_uS, uint8_t *pu8prescaler) __attribute__((unused));
static uint16_t	IntervalUS_CalcTimerRegisters(uint16_t u16_uS, uint8_t *pu8prescaler)
{
	return (0);
	if (u16_uS >0)
	{
		uint16_t t1;
		uint8_t  t2 = 0;
		t1 = (((SYS_CLK_KHZ)*u16_uS)/2068)/65536; /* == mS */
		while (t1 > (1<<t2)) t2++;
		if (t2 > 15)
		{
			/* value too large */
			return (0);
		}
		*pu8prescaler = t2;
		return ( (((SYS_CLK_KHZ)*u16_uS)/2068)/(1<<t2));
	}
	return(0);
}

/** ********************************************************************
 *	\brief		Actually starts the PIT running.
 *	\param		eTimer	--	The timer to be started.
 *	\return		success or fail -- 1 == success / 0 = fail.
 **********************************************************************/
uint8_t	u8HWTIMER_StartTimer(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			TIMSK0 |= (1 << OCIE0A);
			break;
		case Timer1:
			TIMSK1 |= (1 << OCIE0A);
			break;
		case Timer2:
			TIMSK2 |= (1 << OCIE0A);
			break;
		default:
		    return (0);
	}
	sei();
	return (1);
}

/** ********************************************************************
 *	\brief		Initialises specified timer registers with value
 *				calculated by IntervalUS_CalcPITRegisters().
 *	\param		eTimer		-- timer to be used.
 *	\param		u8PreScale	-- Value to use for prescaler register.
 *	\param		u16Interval -- Value for counter register.
 *	\return		success or fail -- 1 = success / 0 = fail.
***********************************************************************/
uint8_t	u8HWTIMER_InitTimer(eHW_TIMER eTimer, uint8_t u8PreScale, uint16_t u16Interval)
{
//	if ( 0 == u8HWTIMER_AcquireTimer(eTimer))
//	{
//		return 0;
//	}
//	if (u8PreScale < 15 && u16Interval > 0)
//	{
		cli();									// disable global interrupts
		switch(eTimer)
		{
			case Timer0:
				TCCR0A = 0; 					// set entire TCCR0A register to 0
				TCCR0B = 0;						// same for TCCR0B
				TCCR0A |= (1 << WGM01);			// Clear Timer on Compare (OK)
				TCCR0B |= (1 << CS01) | (1 << CS00);	// set prescaler to 64 and start the timer
				OCR0A = 250;					// set compare match register to desired timer count (0xF9 would be 1mS @ 16MHz)
				break;
			case Timer1:
				TCCR1A = 0; 					// set entire TCCR0A register to 0
				TCCR1B = 0;						// same for TCCR0B
//				TCCR1A |= (1 << WGM11);
				TCCR1B |= (1 << CS10) | (1 << CS11) | (1 << WGM12);	// set prescaler to 64 and start the timer
				OCR1A = 250;					// set compare match register to desired timer count (0xF9 would be 1mS @ 16MHz)
				return (0);
				break;
			case Timer2:
				TCCR2A = 0; 					// set entire TCCR0A register to 0
				TCCR2B = 0;						// same for TCCR0B
				TCCR2A |= (1 << WGM21);			// Clear Timer on Compare (OK)
				TCCR2B |= (1 << CS22) ;			// set prescaler to 64 and start the timer
				OCR2A = 250;					// set compare match register to desired timer count (0xF9 would be 1mS @ 16MHz)
				break;
			default:
				sei();							// enable global interrupts:
				return (0);
		}
		sei();									// enable global interrupts:
		return (1);
//	}
	return (0);
}

/** ********************************************************************
 *	\brief		Initialises specified timer for the interval specified
 *				in u8IntervalUS in microseconds.
 *	\param		eTimer			-- timer to be used.
 *	\param		u16IntervalUS	-- Interval for timer in microseconds.
 *	\return		success or fail -- 1 = success / 0 = fail.
 **********************************************************************/
uint8_t	u8HWTIMER_InitTimerUs(eHW_TIMER eTimer,  uint16_t u16IntervalUS)
{
	/* set up interval for PIT to u8IntervalUS microseconds */
	if (u16IntervalUS)
	{	/* just set up the timer regs, but do NOT start ! */
		uint8_t	u8PreScale = 2;
		uint16_t u16Count = 0xF9;
//		u16Count = IntervalUS_CalcTimerRegisters(u16IntervalUS,&u8PreScale);
		return (u8HWTIMER_InitTimer(eTimer,u8PreScale,u16Count));
	}
	return (0);
}

/** ********************************************************************
 *	\brief		Stop operation of the specified PIT timer.
 *	\param		eTimer	-- timer to be used.
 *	\return		success or fail -- 1 = success / 0 = fail.
 **********************************************************************/
uint8_t	u8HWTIMER_StopTimer(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			break;
		case Timer1:
			break;
		case Timer2:
			break;
		default:
		    return (0);
	}
	return (1);
}

/** ********************************************************************
 *	\brief		Gets the value of the specified PIT timer.
 *	\param		eTimer	-- timer to be used.
 *	\return		The value of the PIT counter register.
 **********************************************************************/
uint16_t	u16HWTIMER_GetTimer(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			return (TCNT0);
		case Timer1:
			return (TCNT1);
		case Timer2:
			return (TCNT2);
		default:
		    return (0);
	}
}

/** ********************************************************************
 *	\brief		Enable interrupts on the specified PIT timer.
 *	\param		eTimer	--	timer to enable interrupts on.
 **********************************************************************/
void	HWTIMER_EnableTimerInts(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			TIMSK0 |= (1 << OCIE0A);			// enable timer compare interrupt:
			break;
		case Timer1:
			TIMSK1 |= (1 << OCIE1A);			// enable timer compare interrupt:
			break;
		case Timer2:
			TIMSK2 |= (1 << OCIE2A);			// enable timer compare interrupt:
			break;
		default:
		    return;
	}
}

/** ********************************************************************
 *	\brief		Disable interrupts on the specified PIT timer.
 *	\param		eTimer	--	timer to disable interrupts on.
 **********************************************************************/
void	HWTIMER_DisableTimerInts(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			TIMSK0 &= (~(1 << OCIE0A));
			break;
		case Timer1:
			TIMSK1 &= (~(1 << OCIE1A));
			break;
		case Timer2:
			TIMSK2 &= (~(1 << OCIE2A));
			break;
		default:
		    return;
	}
}

/** ********************************************************************
 *	\brief		Set a function to be called when the timer generates an
 *				interrupt.
 *	\param		eTimer	-- timer to disable interrupts on.
 *	\param		pFN		-- The function to be called.
 **********************************************************************/
void	HWTIMER_SetTimerHandler(eHW_TIMER eTimer, pHWTIMER_FN pFN)
{
	if (pFN)
	{
		switch(eTimer)
		{
			case Timer0:
				pHandlerTimer0 = pFN;
				break;
			case Timer1:
				pHandlerTimer1 = pFN;
				break;
			case Timer2:
				pHandlerTimer2 = pFN;
				break;
			default:
			    return;
		}
	}
	return;
}

/** ********************************************************************
 *	\brief		Gets the timer counter elapsed since the last timer
 *				event/interrupt.
 *	\param		eTimer	--	timer to get the elapsed time for
 *	\return		Calculated elapsed time interval.
 **********************************************************************/
uint16_t	u16HWTIMER_GetTimerElapsedTime(eHW_TIMER eTimer)
{
	switch(eTimer)
	{
		case Timer0:
			return (TCNT0);
		case Timer1:
			return (0);
		case Timer2:
			return (0);
		default:
		    return (0);
	}
}

ISR(TIMER0_COMPA_vect)  // timer0 overflow interrupt
{
#ifdef HWTIMER_DEBUG
	DLED_TOGGLE(DLED_1);
	BLED_TOGGLE(BLED_1);
#endif
	HWTIMER_DefaultHandler0();//event to be executed here
}

ISR (TIMER1_COMPA_vect)  // timer0 overflow interrupt
{
#ifdef HWTIMER_DEBUG
	DLED_TOGGLE(DLED_2);
	BLED_TOGGLE(BLED_2);
#endif
	HWTIMER_DefaultHandler1();//event to be executed here
}

ISR (TIMER2_COMPA_vect)  // timer0 overflow interrupt
{
#ifdef HWTIMER_DEBUG
	DLED_TOGGLE(DLED_3);
	BLED_TOGGLE(BLED_3);
#endif
	HWTIMER_DefaultHandler2();//event to be executed here
}

