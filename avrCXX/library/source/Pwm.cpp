/* ****************************************************************/
/* File Name    : Pwm.cpp                                         */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 17th March 2021                                 */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

*********************************************************************/
#include <avr/io.h>

#include "Pwm.hpp"

/** ********************************************************************
 *	\brief		Initialise all the Timer and pwm regsiters.
 *	\param		period		--	selects the divisor to be used.
 *	\param		inverted	--	inverts the output waveform when true.
 *	\param		fast		--	selects pwm fast mode when true.
 **********************************************************************/
void Pwm::Init(	ePeriod period, bool inverted, bool fast)
{
	switch (m_Timer)
	{
		case PwmTimer0:
			SetMode(fast);
			SetPolarity(inverted);
			SetPrescaler(period);
			TCCR0A |= (1 << WGM00);
			TCCR0B &= ~7;							//	reset prescaler bits
			TCCR0B = period;
			if (m_Pin == PinA)
			{
				TCCR0A |= (1 << COM0A1);
				OCR0A  = 0;								// set PWM to 0% as initial value
				DDRD |= (1 << DDD6);
			}
			if (m_Pin == PinB)
			{
				TCCR0A |= (1 << COM0B1);
				OCR0B  = 0;								// set PWM to 0% as initial value
				DDRD |= (1 << DDD5);
			}
		case PwmTimer1:
			SetMode(fast);
			SetPolarity(inverted);
			SetPrescaler(period);
			if (m_Pin == PinA)
			{
				TCCR1A |= (1 << COM1A1);
				OCR1A  = 0;								// set PWM to 0% as initial value
				DDRB |= (1 << DDB1);
			}
			if (m_Pin == PinB)
			{
				TCCR1A |= (1 << COM1B1);
				OCR1B  = 0;								// set PWM to 0% as initial value
				DDRB |= (1 << DDB2);
			}
			break;
		case PwmTimer2:
			SetMode(fast);
			SetPolarity(inverted);
			SetPrescaler(period);
			if (m_Pin == PinA)
			{
				TCCR2A |= (1 << COM2A1);
				OCR2A  = 0;								// set PWM to 0% as initial value
				DDRB |= (1 << DDB3);
			}
			if (m_Pin == PinB)
			{
				TCCR2A |= (1 << COM2B1);
				OCR2B  = 0;								// set PWM to 0% as initial value
				DDRD |= (1 << DDD3);
			}
			break;
		default:
			break;
	}
	Stop();
	return;
}

/** ********************************************************************
 *	\brief		Actually starts the PWM output
 *	\param		width	--	Sets the pulse width.\n
 *							Valid values are between 0 and 255.
 **********************************************************************/
void Pwm::Start(uint8_t width)
{
	switch (m_Pin)
	{
		case PinA:				//	Pin1:
			switch(m_Timer)
			{
				case PwmTimer0:
					OCR0A = width;
					break;
				case PwmTimer1:
					OCR1A = width;
					break;
				case PwmTimer2:
					OCR2A = width;
					break;
			}
			break;
		case PinB:				//	Pin2:
			switch(m_Timer)
			{
				case PwmTimer0:
					OCR0B = width;
					break;
				case PwmTimer1:
					OCR1B = width;
					break;
				case PwmTimer2:
					OCR2B = width;
					break;
			}
			break;
	}
}

/** ********************************************************************
 *	\brief		Stops the PWM output waveform.

	NOTE	--	This DOES NOT actually stop the PWM from the timer, it
				simply sets it to minimum, which is effectively off.
				Setting to maximum has the same effect but the pin
				willl be set to the inverse state.\n
				In 'normal' mode the pin is low at minimum and high at 
				maximum.\n
				In 'inverted' mode, the pin is high at minimum and low
				at maximum.
 **********************************************************************/
void Pwm::Stop()
{
	Start(0);
}

/** ********************************************************************
 *	\brief		Switch BOTH pins on this timer to fast pwm mode
 *	\param		fastmode	Defaults to false.\n
 *							Set to true to enable fast pwm mode.
 **********************************************************************/
void Pwm::SetMode(bool fastmode)
{
	switch (m_Timer)
	{
		case PwmTimer0:
			if (fastmode)		TCCR0A |=  (1 << WGM00) | (1 << WGM01);
			else				TCCR0A &= ~(1 << WGM01);
			break;
		case PwmTimer1:
			if (fastmode)		TCCR1A |=  (1 << WGM10) | (1 << WGM11);
			else				TCCR1A &= ~(1 << WGM11);
			break;
		case PwmTimer2:
			if (fastmode)		TCCR2A |=  (1 << WGM20) | (1 << WGM21);
			else				TCCR2A &= ~(1 << WGM21);
			break;
		default:
			break;
	}
}

/** ********************************************************************
 *	\brief		Invert the polarity of the output waveform
 *	\param		inverted	Defaults to false, i.e. Normal polarity\n
 *							Set to true to invert
 **********************************************************************/
void Pwm::SetPolarity(bool inverted)
{
	switch (m_Timer)
	{
		case PwmTimer0:
			if (m_Pin == PinA)
			{
				if (inverted)		TCCR0A |= (1 << COM0A0);
				else				TCCR0A &= ~(1 << COM0A0);
			}
			if (m_Pin == PinB)
			{
				if (inverted)		TCCR0A |= (1 << COM0B0);
				else				TCCR0A &= ~(1 << COM0B0);
			}
		case PwmTimer1:
			if (m_Pin == PinA)
			{
				if (inverted)		TCCR1A |= (1 << COM1A0);
				else				TCCR1A &= ~(1 << COM1A0);
			}
			if (m_Pin == PinB)
			{
				if (inverted)		TCCR1A |= (1 << COM1B0);
				else				TCCR1A &= ~(1 << COM1B0);
			}
			break;
		case PwmTimer2:
			if (m_Pin == PinA)
			{
				if (inverted)		TCCR2A |= (1 << COM2A0);
				else				TCCR2A &= ~(1 << COM2A0);
			}
			if (m_Pin == PinB)
			{
				if (inverted)		TCCR2A |= (1 << COM2B0);
				else				TCCR2A &= ~(1 << COM2B0);
			}
			break;
		default:
			break;
	}
}

/** ********************************************************************
 *	\brief		Sets the prescaler bits in TCCRnB.

				The prescaler can be set to one of :-
				0	--	No clock, i.e. OFF
				1		divide clock source by 1
				2		divide clock source by 8
				3		divide clock source by 64
				4		divide clock source by 256
				5		divide clock source by 1024
				Check the definition of ePeriod
 *	\param		ePeriod -- prescaler
 **********************************************************************/
void Pwm::SetPrescaler(ePeriod prescaler)
{
	switch (m_Timer)
	{
		case PwmTimer0:
			TCCR0B &= ~7;
			TCCR0B |= prescaler;
			break;
		case PwmTimer1:
			TCCR1B &= ~7;
			TCCR1B |= prescaler;
			break;
		case PwmTimer2:
			TCCR2B &= ~7;
			TCCR2B |= prescaler;
			break;
		default:
			break;
	}
}
