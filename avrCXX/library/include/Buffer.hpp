/* ****************************************************************/
/* File Name    : Buffer.hpp                                      */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __BUFFER_HPP
#define __BUFFER_HPP

#include <stdint.h>

#ifndef BUFF_SIZE
#define BUFF_SIZE	128
#endif
/**
	\class	Buffer
	\fn		inline	uint8_t	Size();
	\brief	Gets the size of the buffer
	\return	The size of the buffer
*/
/**
	\class	Buffer
	\fn		inline	bool	Full();
	\brief	Gets the size of the buffer
	\return	The size of the buffer
*/
/**
	\class	Buffer
	\fn		inline	bool	Empty();
	\brief	Gets the size of the buffer
	\return	The size of the buffer
*/

class Buffer
{
	public:
		Buffer();
		bool 	Push(const char ch);
		uint8_t	Push(const char *buffer,uint8_t len);
		bool	Pull(char &item);
		uint8_t	Pull(char *buffer,uint8_t bufflen);
		void	Flush();
		inline	uint8_t	Size()			{	return BUFF_SIZE;	}
		inline	bool	Full()			{	return (((m_Head+1) % BUFF_SIZE) == m_Tail);	}
		inline	bool	Empty()			{	return (m_Head == m_Tail);			}
	private:
		uint8_t	m_Head;					//!<	Location to Push() next entry
		uint8_t	m_Tail;					//!<	Location for next Pull()
		char	m_Buffer[BUFF_SIZE];	//!<	The actual buffer data
};

#endif	//	end of "#ifndef __BUFFER_HPP"
