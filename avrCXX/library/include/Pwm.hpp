/* ****************************************************************/
/* File Name    : Pwm.hpp                                         */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 17th March 2021                                 */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

*********************************************************************/
#ifndef __PWM_HPP
#define __PWM_HPP

/*
 *	NOTE -- The frequencies defined in ePeriod are approximate
 *			and based on the CPU running at 16Mhz
 */
enum	ePeriod		{	P31KHz    = 1, P4KHz,  P500Hz, P120Hz, P30Hz,		// Phase correct frequencies
						F62KHz    = 1, F8KHz,  F1KHz,  F240Hz, F60Hz,		//	Fast pwm correct frequencies
						VERY_FAST = 1, FAST,   MEDIUM, SLOW,   VERY_SLOW};	//	Generic identifiers
enum	ePwmPin		{	PinA = 1, PinB, Pin1 = 1, Pin2 };
enum	ePwmTimer	{	PwmTimer0, PwmTimer1, PwmTimer2 };

class Pwm
{
	public:
		Pwm(ePwmTimer	eTimer		= PwmTimer0, 
			ePwmPin		pin			= PinA)
			{	m_Timer = eTimer; 	m_Pin = pin;	}
		void	Init(	ePeriod period,
						bool	inverted	= false,
						bool	fast 		= false);
		void	Start(	uint8_t width);
		void	Stop();
		void	SetPrescaler(	ePeriod prescaler = MEDIUM);
		void	SetPolarity(	bool inverted = false);
		void	SetMode(		bool fastmode = false);
		void	SetNormalPwm()	{	SetMode(false);	}
		void	SetFastPwm()	{	SetMode(true);	}
	private:
		ePwmPin		m_Pin;
		ePwmTimer	m_Timer;
};

#endif
