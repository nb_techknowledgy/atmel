/* ****************************************************************/
/* File Name    : OneWire.cpp                                     */
/* Description  : Implements the Dallas single wire protocol      */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License	: GPL v3.0 or later                                   */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Delay() changed to LoopDelay(0 to remove        */
/*                conflict with Delay in DelayTimer.hpp           */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

***********************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/crc16.h>

#include "OneWire.hpp"
/*
	====================================================================
	INFORMATION --
	This library is a re-write of the Arduino library of the same name.
	ALL timings have been optimised according to the Dallas One Wire spec.
	This utility/library is intended to be used outside of the Arduino
	environment for those who want to "do it properly".
	To provide SOME flexibilty, a compromise has been made to allow a choice
	of which I/O pins gets used when the constructor is defined/called.
	This has the added side effect of being able to have multiple instances
	running on different I/O pins.
	To revert to a dedicated coded I/O pin define DEBUG, and
	set the port and pin mask accordingly.
	The extra code and variable around the ports and pins
	could then be removed.
	====================================================================
	NOTES	--
		ALL methods that use method LoopDelay() SHOULD disable interrupts
			BEFORE calling LoopDelay() and restore the SREG state on exit
		WHY ?
			the timing is dependant on a tight loop, and getting an
			interrupt during LoopDelay() will BADLY affect the timing
	====================================================================
*/
/*	====================================================================
 * if TIMING is defined, an addional LOW pulse is added to allow
 * timings to be measured with a scope or logic analyser
 * 	-- this SHOULD NOT be defined by default
 *                 ===
 */
//#define TIMING
#ifdef TIMING
#define TIMING_CHECK	{ 	PinLo(); asm("nop"); PinHi();	}
#else
#define TIMING_CHECK	while(0) {;}
#endif
// -------------------------------------------------------------
/** ********************************************************************
 *	\brief		Set IO pin up as the onewire interace.
 *	\param		*pPort		uC port to use for the onewire bus.
 *	\param		Bit			Bit of the port ( between 0 and 7 ).
 *	\param		NoPullup	Set true to use internal pullup.
 *	\return		Nothing.
 **********************************************************************/
OneWire::OneWire(volatile uint8_t *pPort, uint8_t Bit, bool NoPullup)
{
	m_PullupPresent = NoPullup;
	m_Pin = (1 << Bit);
	m_pPort = pPort;
	// This is an ugly solution .. but it only executes the once!
	if (pPort == &PORTB) { m_pDirection = &DDRB; m_pPin = &PINB; }
	if (pPort == &PORTC) { m_pDirection = &DDRC; m_pPin = &PINC; }
	if (pPort == &PORTD) { m_pDirection = &DDRD; m_pPin = &PIND; }
	//	Calling PinHi() sets the direction and data registes for us.
	PinHi();		//	Inline function ALWAYS !
}

//	The optimize setting here is to improve timings
//	It could probably be removed, but timings should be checked
//	and the resultant assembler code checked for efficiency
#pragma GCC push_options
#pragma GCC optimize ("O3")
/** ********************************************************************
 *	\brief		Changes whether there is an external pullup is in use or not.
 *	\param		pullup		-- Set true if there is a pullup present.
 *	\return		Nothing.
 **********************************************************************/
void	OneWire::Pullup(bool pullup)
{
	m_PullupPresent = pullup;
	if (pullup)
	{
		*m_pDirection &= ~m_Pin;	//	Set pin as input -- do not drive high
	}
	else
	{
		*(m_pDirection) |=  m_Pin;	//	Set pin as output
		*m_pPort |=  m_Pin;			//	AND drive it HIGH
	}
}

/** ********************************************************************
 *	\brief		Performs a onewire bus reset.

 *				Set the pin low for 480uS and waits a further 480uS.
 *	\return		Nothing.
 ************************************************************************/
void	OneWire::Reset()
{
	uint8_t sREG = SREG;
	cli();
	PinLo();		//	Inline function ALWAYS !
	LoopDelay(480);
	PinHi();		//	Inline function ALWAYS !
	LoopDelay(480);
	SREG = sREG;
}
#pragma GCC pop_options

/** ********************************************************************
 *	\brief		Find the FIRST device on the onewire interface.

 *				Resets all the search conditions and calls FindNext()
 *	 \return	See FindNext()
 **********************************************************************/
bool OneWire::FindFirst(uint8_t family, ROM& rom)
{
	m_LastConflict = 0;
	m_LastFamilyConflict = 0;
	m_LastDevice = 0;
	uint8_t i = 7;
	do	{	m_Rom[i--] = 0;		} while (i);
	m_Rom[0] = family;
	return FindNext(rom);
}

/** ********************************************************************
 *	\brief		Find a device on the onewire interface

 *				Too complicated .. read the Maxim datasheet !!
 * NOTE		--	When all devices have been found m_LastDevice is set true
 *	\param[out]	rom		--	the device ROM is returned here
 *	\return		true	--	device found.\n
 * 				false	--	no device found.
 **********************************************************************/
bool OneWire::FindNext(ROM& rom)
{
	bool IdBit,CompBit;							//	bits read from wire
	bool NextWrite;								//	set search direction
	uint8_t BitNumber = 1;						//	start at bit 1 ?
	uint8_t LastZero = 0;
	uint8_t	RomBit = 1,RomByte = 0;				//	index into indivual bits in 'm_Rom[8]'
	Reset();
	Write(0xF0);
	do
	{
		IdBit = ReadBit();						//	read a bit from the wire
		CompBit = ReadBit();					//	read the complement bit
		if ( IdBit & CompBit )					//	Sanity check
		{
			return false;						//	both bits HIGH so no devices present !
		}
		else
		{
			if ( IdBit != CompBit)
			{
				NextWrite = IdBit;
			}
			else
			{
				if (BitNumber < m_LastConflict)
				{
					NextWrite = (m_Rom[RomByte] & RomBit);	//	 > 0;
				}
				else
				{
					NextWrite = (BitNumber == m_LastConflict);
				}
				if (!NextWrite)
				{
					LastZero = BitNumber;
					if (LastZero < 9)
					{
						m_LastFamilyConflict = LastZero;
					}
				}
			}
			//	====================================
			if (NextWrite)
			{
				m_Rom[RomByte] |= RomBit;
			}
			else
			{
				m_Rom[RomByte] &= ~RomBit;
			}
			WriteBit(NextWrite);
			BitNumber++;
			RomBit = RomBit << 1;
			if (0 == RomBit)
			{
				RomByte++;
				RomBit = 1;
			}
		}
	}	while(RomByte < 8);		// loop until through all ROM bytes 0-7
	if (BitNumber < 65)			//	double check we have processed 64 bits
	{
		return false;
	}
	m_LastConflict = LastZero;
	if (0 == m_LastConflict)
	{
		m_LastDevice = true;
	}
	for (uint8_t i=0; i<8; i++)
	{
		rom[i] = m_Rom[i];
	}
	return true;
}

/** ********************************************************************
 *	\brief		Choose which device to communicate with.

 *				Writes a command to the bus followed by the device rom.
 *	\return		Nothing.
 **********************************************************************/
void OneWire::Select(const ROM rom)
{
	Write(0x55);           // Choose ROM
	for (uint8_t i = 0; i < 8; i++)
	{
		Write(rom[i]);
	}
}

/** ********************************************************************
 *	\brief		Reads a byte of data from the onewire interface.

 *				Calls the ReadBit() method for each bit of a byte.
 *	\return		the byte read
 **********************************************************************/
uint8_t	OneWire::Read()
{
	uint8_t Byte = 0;
	for (uint8_t bit=1; bit; bit<<= 1)
	{
		if (ReadBit())
		{
			 Byte |= bit;
		}
    }
    return Byte;
}

/** ********************************************************************
 *	\brief		Reads 'length' bytes of data from the onewire interface.

 *				Calls the basic Read() method for each byte.
 *	\param[out]	buffer	--	filled in with the received data.
 *	\param[in]	length	--	length of the supplied buffer.
 *	\return		The length of the returned data.
 * 				The data is returned in the 'buffer' parameter.
 **********************************************************************/
uint8_t	OneWire::Read(char *buffer, uint8_t length)
{
	for (uint8_t l=0;l<length;l++)
	{
		*(buffer++) = Read();
	}
	return length;
}

/** ********************************************************************
 *	\brief		Write 8 bits onto the onewire interface.

 *				Calls the WriteBit() method for each bit in 'Byte'.
 *	\param		Byte	data to send on the bus.
 *	\return		Nothing.
 **********************************************************************/
void	OneWire::Write(uint8_t Byte)
{
    for (uint8_t bit=1; bit; bit<<=1)
    {
		WriteBit(Byte & bit  ? true : false);
	}
}

/** ********************************************************************
 *	\brief		Writes the supplied buffer to the onewire pin.

 *				Calls the basic Write() method for each character.
 *	\param		buffer	--	the data to be sent.
 *	\param		length	--	the length of the data in the buffer
 *	\return		Number of bytes sent.
 **********************************************************************/
uint8_t	OneWire::Write(const char *buffer, uint8_t length)
{
	for (uint8_t l=0;l<length;l++)
	{
		Write(*(buffer++));
	}
	return length;
}

//	The optimize setting here is to improve timings
//	This one should NOT be removed unless the timings are be checked
//	The resultant assembler code should also be checked for efficiency
#pragma GCC push_options
#pragma GCC optimize ("O3")
/** ********************************************************************
 *	\brief		Produce best possible delay times using a loop.

 *				Some values are preset and selected using the switch
 * 				statement. the loop values are optimised for a 16MHz
 * 				atmega328p\n.
 * 				values outside of the switch are applied as a loop count.
 *	\param		delay	--	the length of the delay in approximate uS.
 *	\return		Nothing.
 **********************************************************************/
void	OneWire::LoopDelay(uint16_t delay)
{
	if (1 == delay)
		return;
	else
	{
		volatile uint16_t d;
		switch(delay)
		{
			case 15 :	m_PullupPresent ? d=12 : d = 9 ;
								break;	//	Low pulse to Write a '1' to bus
			case 30 :	d=31;	break;	//	Readbit delay before sampling the pin
			case 60 :	d=61;	break;	//	less than 60 .. to allow for reading the pin
			case 75 :	d=80;	break;	//	WriteBit = 1 high time after 15uS LOW pulse
			case 90 :	d=98;	break;	//	WriteBit = 0 low time for line to write a '0'
			case 480:	d=550;	break;	//	Reset	Time for LOW pulse and for Rx presence bit
			default:	d=delay;		//	Cater for non standard delays -- NOT CALIBRATED
		}
		while(d--);
	}
}
//	====================================================================
/** ********************************************************************
 *	\brief		Sets the onewire pin low.

 *				Sets the pin to an output, and sets the output to 0.
 * \nNOTE		--	This should ALWAYS be inlined by the compiler as the
 * 				delays incurred by calling a function adversely impact
 * 				the required timing
 *	\return		Nothing.
 **********************************************************************/
inline	void	OneWire::PinLo()
{
	*m_pPort &= ~m_Pin;				//	PINLO
	*m_pDirection |=  m_Pin;		//	PINLO
}

/** ********************************************************************
 *	\brief		Sets the onewire pin high.

 *				Sets the pin to an input, to be pulled hi by other means.
 * \nNOTE	--	This should ALWAYS be inlined by the compiler as the
 * 				delays incurred by calling a function adversely impact
 * 				the required timing.
 *	\return		Nothing.
 **********************************************************************/
inline	void	OneWire::PinHi()
{
	volatile uint8_t k = 1;
	if (m_PullupPresent)
	{
		(k--);
		*m_pDirection &= ~m_Pin;	//	PINHI;
	}
	else
	{
//		asm("nop");
		*m_pDirection &= ~m_Pin;	//	PINHI;
		*m_pPort |= m_Pin;			//	RE-ENABLE PULLUP;
	}
}
//	====================================================================
/** ********************************************************************
 *	\brief		Write a 1 or a 0 to the onewire pin.
 *	\param		bit		--		true for a 1.\n	--	false for a 0.
 *	\return		Nothing.
 **********************************************************************/
void	OneWire::WriteBit(bool bit)
{
	uint8_t sREG = SREG;
	cli();
	if (bit)
	{
		PinLo();		//	Inline function ALWAYS !
		LoopDelay(15);
		PinHi();		//	Inline function ALWAYS !
		LoopDelay(75);
		TIMING_CHECK
	}
	else
	{
		PinLo();		//	Inline function ALWAYS !
		LoopDelay(90);
		PinHi();		//	Inline function ALWAYS !
		TIMING_CHECK
	}
	SREG = sREG;
}

/** ********************************************************************
 *	\brief		Reads a bit from the onewire interface.

 *				Wiggles the pin and waits for the device to respond.
 *	\return		true if the bit is 1.\n
 *				false if the bit is 0.
 **********************************************************************/
bool OneWire::ReadBit()
{
	uint8_t sREG = SREG;
	bool value;
	cli();
	PinLo();		//	Inline function ALWAYS !
	PinHi();		//	Inline function ALWAYS !
	LoopDelay(10);
	TIMING_CHECK
	value = *m_pPin & m_Pin;
	LoopDelay(60);
	TIMING_CHECK
	SREG = sREG;
	return value ? true : false;
}
#pragma GCC pop_options
//	See note above
//
//	C++ code below here is based on the avr/utils/crc16.h file "C equivalent" code
//	All rights of the copyright holder ackknowdged
/** ********************************************************************
 *	\brief		Calculate the crc8 of the supplied buffer.
 *	\param		data	data buffer to be checked.
 *	\param		length	length of data to performa crc on.
 *	\return		crc8 of the supplied data.
 **********************************************************************/
uint8_t OneWire::Crc8(const uint8_t *data, uint8_t length)
{
	uint8_t crc = 0;
	while (data--)
	{
#if (defined  __AVR__ ) && (defined _UTIL_CRC16_H_)
		crc = _crc_ibutton_update(crc, *(data++));
#else
		uint8_t i;
		crc = crc ^ *(data++);
		for (i = 0; i < 8; i++)
		{
			if (crc & 0x01)
				crc = (crc >> 1) ^ 0x8C;
			else
				crc >>= 1;
		}
#endif
	}
	return crc;
}

/** ********************************************************************
 *	\brief		Calculate the crc8 of the supplied buffer.
 *	\param		data	data buffer to be checked.
 *	\param		length	length of data to performa crc on.
 *	\param		crc
 *	\return		crc16 of the supplied data.
 **********************************************************************/
uint16_t OneWire::Crc16(const uint8_t* input, uint16_t len, uint16_t crc)
{
    for (uint16_t i = 0 ; i < len ; i++)
    {
#if defined(__AVR__) && (defined _UTIL_CRC16_H_)
        crc = _crc16_update(crc, input[i]);
#else
		int i;
		crc ^= a;
		for (i = 0; i < 8; ++i)
		{
			if (crc & 1)
				crc = (crc >> 1) ^ 0xA001;
			else
				crc = (crc >> 1);
		}
#endif
    }
	return crc;
}
