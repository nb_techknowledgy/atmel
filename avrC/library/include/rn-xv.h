/* ****************************************************************/
/* File Name    : rn-xv.h                                         */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __RN_XV_H
#define __RN_XV_H

// RN-XV Command strings

#if defined (HANDSET) || defined (CONTROLLER)
// common commands here
#define RNXV_COMMAND_MODE		("$$$")
#define RNXV_SET_WLAN_SSID(x)	("set wlan ssid %s\r",x)
#define RNXV_SET_WLAN_PHRASE(x)	("set wlan phrase "x"\r")
#define RNXV_SET_WLAN_AUTH		("set wlan auth 0\r")
#define RNXV_SAVE_CONFIG		("save\r")
#define RNXV_EXIT_CMD_MODE		("exit\r")
#define RNXV_REBOOT_RADIO		("reboot\r")
#define RNXV_SET_IP_ADDR(x)		("set ip address "x"\r")
#define RNXV_SET_IP_NETMASK		("set ip netmask "x"\r")
#define RNXV_DISABLE_DHCP		("set ip dhcp 0\r")
#define RNXV_RADIO_HEARTBEAT	("set sys printlvl 0x10\r")


#endif

#if defined (HANDSET) && !defined(CONTROLLER)
// just handset specific commands here
#define RNXV_SET_WLAN_JOIN		("set wlan join 1\r")
#define RNXV_SET_WLAN_CHANNEL	("set wlan channel 0\r")
#define RNXV_CONNECT			("open\r")
#define RNXV_SET_HOST_IP(x)		("set ip host "x"\r")
#define RNXV_SET_HOST_PORT(x)	("set ip remote_port "x"\r")
#define RNXV_SET_AUTO_CONNECT	("set sys autoconn 1\r")
#define RNXV_SET_AUTO_IDLE(x)	("set com idle "x"\r")
#define RNXV_SET_AUTO_SLEEP(x)	("set sys sleep "x"\r")
#endif

#if defined (CONTROLLER) && !defined(HANDSET)
// just controller specific commands here
#define SET_WLAN_CHANNEL(x)	("set wlan channel "x"\r")
#define SET_WLAN_JOIN		("set wlan join 4\r")
#endif


int RNXV_SetupBoard();



#endif	/* end of #ifdef __RN_XV_H		*/
