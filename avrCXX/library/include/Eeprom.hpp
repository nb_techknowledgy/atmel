/* ****************************************************************/
/* File Name    : Eeprom.hpp                                      */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 12th March 2021                                 */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __EEPROM_HPP
#define __EEPROM_HPP

class Eeprom
{
	public:
		Eeprom()	{	m_MaxAddr = GetSizeFromDevice();	}
		void		Write(	uint16_t address, uint8_t byte);
		void		Write(	uint16_t address, uint16_t byte);
		uint16_t	Write(	uint16_t address, const uint8_t *buffer, uint16_t length);
		uint8_t		Read(	uint16_t address);
		uint16_t	Read(	uint16_t address, uint8_t *buffer, uint16_t bufflen);
		uint16_t	ReadWord(	uint16_t address);
		uint16_t	Size() 		{	return m_MaxAddr;	}
		uint32_t	DeviceId()	{	return m_DeviceId;	}
	private:
		uint16_t	GetSizeFromDevice();
		uint16_t	m_MaxAddr;		// ATmega328 has 1024 bytes of eeprom
		uint32_t	m_DeviceId;
};

#endif	//	end of "#ifndef __EEPROM_HPP"
