/* ****************************************************************/
/* File Name    : adc.h                                           */
/* Description  : Provides basic access to the  Atmega ADC        */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __ADC_H
#define __ADC_H


#ifdef __cplusplus
extern "C"
{
#endif	// of #ifdef __cplusplus

void	ADC_Init();
void	ADC_SetChannel(uint8_t ADCchannel);
void	ADC_Start(uint8_t *pu8Channel);
void	ADC_Disable(void);			//	disable ADC
int16_t	ADC_GetValue(uint8_t u8Chan);


#ifdef __cplusplus
}
#endif	/* end of #ifdef __cplusplus	*/
#endif	/* end of #ifdef __ADC_H		*/
