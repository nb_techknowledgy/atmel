/* ****************************************************************/
/* File Name    : A2D.cpp                                         */
/* Description  : Provides basic access to the  Atmega ADC        */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

***********************************************************************/
#include <avr/io.h>
#include "A2D.hpp"

/** ********************************************************************
 *	\brief		Initialise ADC registers
 *	\param		Mask -- not used -- yet
 *	\return		Nothing returned
 **********************************************************************/
A2D::A2D(uint8_t Mask)
{
	memset((void*)m_Value,0, sizeof(uint16_t)*8);
    // Select Vref=AVcc and set left adjust result
    ADMUX |= (1<<REFS0);	//	|(1<<ADLAR);
    //	set prescaler to 128
    //	enable ADC interupt
    //	and enable ADC
    ADCSRA = ( 1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);	// pre-scaler = 128
    ADCSRB = 0;
}

/** ********************************************************************
 *	\brief		Sets the ADC mux to the specified channel
 *	\param		Channel -- the channel to be selected
 *	\return		Nothing
 **********************************************************************/
void A2D::SetChannel(uint8_t Channel)
{
    ADMUX = (ADMUX & 0xF0) | (Channel & 0x0F);	//	select ADC channel with safety mask
}

/** ********************************************************************
 *	\brief		This method is not called directly. It is called from
 *				the ADC conversion complete or Timer interrupt.
 * 				It calls the StartConversion() method to start the next 
 *				conversion.
 *	\return		Nothing
 **********************************************************************/
void A2D::Callback()
{
	StartConversion(m_pData);
}

/** ********************************************************************
 *	\brief		Selects the ADC channel to be used, and starts the conversion.
 *	\param		pData
 *				if pData == NULL, selects next ADC channel in the mux
 *				and starts the convertsion
 *				if pData != NULL, it is assumned to be a pointer to 
 *				a uint8_t value for the required channel
 *	\return		Nothing
 **********************************************************************/
void A2D::StartConversion(void *pData)
{
	if (pData)
	{
		m_Channel = *((uint8_t*)(pData));
	}
	else
	{
		m_Channel++;
	}
	if (m_Channel > 7) m_Channel = 0;
	if      ( 0 == m_Channel)	m_Value[4] = ADCL + (ADCH << 8);
	else if ( 4 == m_Channel)	m_Value[0] = ADCL + (ADCH << 8);
    else                m_Value[m_Channel] = ADCL + (ADCH << 8);
	SetChannel(m_Channel);
	ADCSRA |= (1<<ADEN) | (1<<ADSC);
}

/** ********************************************************************
 *	\brief		Start the ADC operating for continuous conversion.
 *				if Channel is specfied always convert specified channel
 *				if not, set m_pData = NULL to cause the ADC channels
 *				to be selected in turn for conversion
 *	\param		Channel -- if NULL read channels in turn
 *	\return		Nothing
 **********************************************************************/
void A2D::Start(int8_t Channel)
{
	if (Channel != -1)
	{
		m_Channel = 0;
		m_pData = (void*)&m_Channel;
	}
	else
	{
		m_Channel = Channel;
	}
	StartTimer(10);
}

/** ********************************************************************
 *	\brief		Stops the ADC.
 * 				Disables the ADc in the hardware
 *	\return		Nothing
 **********************************************************************/
void A2D::Disable(void)
{
	ADCSRA &= ~((1<<ADEN));
}

/** ********************************************************************
 *	\brief		Gets the most recent ADC value for the specified channel.
 * 				This DOES NOT causes a a2d conversion !
 *				It simply reads from the stored data the last a2d value
 *				for the specified channel
 *	\return		10 bits of conversion in a 16 bit int
 **********************************************************************/
int16_t A2D::GetValue(uint8_t Channel)
{
	return (m_Value[Channel]);
}
