/* ****************************************************************/
/* File Name    : lDEBUG.h                                        */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __LED_CONTROL_H
#define __LED_CONTROL_H

#define BLED_1			(1)
#define BLED_2			(2)
#define BLED_3			(4)
#define BLED_4			(8)
#define BLED_5			(16)

#define DLED_1			(4)
#define DLED_2			(8)
#define DLED_3			(16)
#define DLED_4			(32)
#define DLED_5			(64)
#define DLED_6			(128)

#define BLED_ALL			(BLED_1 | BLED_2 | BLED_3 |BLED_4 |BLED_5)
#define DLED_ALL			(DLED_1 | DLED_2 | DLED_3 |DLED_4 |DLED_5 | DLED_6)

#define BLED_OFF(x)		{ bLedArray &= ~x; PORTB = bLedArray; }
#define BLED_ON(x)		{ bLedArray |= x;  PORTB = bLedArray; }
#define BLED_TOGGLE(x)	{ bLedArray ^= x;  PORTB = bLedArray; }

#define DLED_OFF(x)		{ dLedArray &= ~x; PORTD = dLedArray; }
#define DLED_ON(x)		{ dLedArray |= x;  PORTD = dLedArray; }
#define DLED_TOGGLE(x)	{ dLedArray ^= x;  PORTD = dLedArray; }

extern uint8_t		bLedArray;
extern uint8_t		dLedArray;

#endif
