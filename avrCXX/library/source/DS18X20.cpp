/* ****************************************************************/
/* File Name    : DS18X20.cpp                                     */
/* Description  : Provides class to use Dallas 1-wire temperature */
/*                sensors -- needs the OneWire base class and the */
/*                Timer class to implement timed wait for         */
/*                measurement complete                            */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c)   2021  Andrew Carney                             */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Migrate from Timer inheritance to DelayTimer to */
/*                implment Delay() function                       */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

********************************************************************/

#include "DS18X20.hpp"

/** ********************************************************************
 *	\brief		Initialise the object and the one wire interface pin.

 *				Initialises all the memeber variables to a known state
 *	\return		Nothing
 **********************************************************************/
DS18B00::DS18B00(volatile	uint8_t *pPort,
							uint8_t Pin,
							bool NoPullup,
							uint8_t Family,
							uint8_t Resolution)
					 : OneWire(pPort,Pin,NoPullup)
{
	m_Family			=	Family;
	m_Resolution		=	Resolution;
	m_PollForComplete	=	false;
	m_Read				=	false;
	for (uint8_t i=0;i<9;i++) {	m_ScratchRAM[i] = 0;	}
	for (uint8_t i=0;i<8;i++) {	m_ThisDevice[i] = 0;	}
}

/** ********************************************************************
 *	\brief		Reads the "scratchram' of the chosen device.

 *				if 'immediate' is true then don't wait for the previous
 *				operation.\n
 *				if 'immediate' is false, wait either for the device to
 *				indicate the conversion is complete, or for the time
 *				specified in the data sheet for the resolution requested.
 *	\param		immediate
 *	\return		16 bit value of the scratchram contents corresponding to
 * 				the last measurement
 **********************************************************************/
uint16_t	DS18B00::Read(bool immediate)
{
	if (!immediate)
	{
		if (m_PollForComplete)
		{
			// wait approx 1mS .. so we don't hit the bus too hard
			while (!ReadBit())	{	Delay(1000); }
		}
		else
		{
			switch (m_Resolution)
			{
				case 9	:	{	Delay(95);	break;	}
				case 10	:	{	Delay(190);	break;	}
				case 11	:	{	Delay(380);	break;	}
				case 12 :
				default :	{	Delay(750);	break;	}
			}
		}
	}
	// Conversion SHOULD be complete by now
	//	Write the 'Read Scratch memory" command
	Write(m_ThisDevice,0xBE);
	OneWire::Read(m_ScratchRAM,9);		//	And read 9 bytes
	m_Read = true;
	return ((m_ScratchRAM[1] & 255) << 8) | (m_ScratchRAM[0] & 255);
}

/** ********************************************************************
 *	\brief		Retrieves the latest temperature reading in Celsius
 *				Uses the raw Read() method to get the value	and
 *				converts to Celsius
 *	\return		16 bit int containing the temperature in Celsius
 **********************************************************************/
float	DS18B00::ReadCelsius(bool immediate)
{
	uint16_t raw;
	if (m_Read)	{	raw = ((m_ScratchRAM[1] & 255) << 8) | (m_ScratchRAM[0] & 255); }
	else		{	raw = Read(immediate);	}
	// convert raw to Celsius here
	return raw/16.0;
}

/** ********************************************************************
 *	\brief		Retrieves the latest temperature reading in Fahrenheit
 *				Uses the raw Read() method to get the value	and
 *				converts to Fahrenheit
 *	\return		16 bit int containing the temperature in Fahrenheit
 **********************************************************************/
float	DS18B00::ReadFahrenheit(bool immediate)
{
	uint16_t raw;
	if (m_Read)	{	raw = ((m_ScratchRAM[1] & 255) << 8) | (m_ScratchRAM[0] & 255); }
	else		{	raw = Read(immediate);	}
	// convert raw value to Fahrenheit here
	return ((raw/16)*1.8)+32;
}

/** ********************************************************************
 *	\brief		Tell a device to initiate a temperature conversion.

 *				Sends the '0x44' command to this device
 *				Sends to all devices connected to the bus if
 *				broadcast is true
 *	\param		broadcast	--	If true - Send command to all devices.\n
 * 				-- If false - Send to this device only.
 *	\return		Nothing
 **********************************************************************/
void	DS18B00::StartMeasurement(bool broadcast)
{
	m_Read = false;
	if (broadcast)
	{
		Reset();
		Write("\xcc\x44",2);
		switch (m_Resolution)
		{
			case 9	:	{	Delay(95);	break;	}
			case 10	:	{	Delay(190);	break;	}
			case 11	:	{	Delay(380);	break;	}
			case 12 :
			default :	{	Delay(750);	break;	}
		}
	}
	Write(m_ThisDevice,0x44);
}

/**********************************************************************
 *	/brief		NOT YET IMPLEMENTED.
 *	/return		Nothing
 **********************************************************************/
void	DS18B00::SetResolution(uint8_t bits)
{
	Read(true);	//	no need to wait as not measuring temperature
	if (m_Read)
	{
		char buffer[4] = { 0x4E, m_ScratchRAM[2], m_ScratchRAM[3], 0 };
		buffer[3] = (((bits-9)&3)<<5) | 0x1F;
		Write(m_ThisDevice,buffer,4);
	}
}
