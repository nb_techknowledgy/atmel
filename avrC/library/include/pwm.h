/* ****************************************************************/
/* File Name    : pwm.h                                           */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __PWM_H
#define __PEM_H

#include "hwtimer.h"

uint8_t PWM_Init(	uint32_t u32Period, eHW_TIMER eTimer);
void	PWM_Start(	eHW_TIMER eTimer, uint8_t Pin, uint8_t u8Period);
void	PWM_Stop(	eHW_TIMER eTimer, uint8_t Pin);



#endif
