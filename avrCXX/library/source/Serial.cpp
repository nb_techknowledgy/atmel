/* ****************************************************************/
/* File Name    : Serial.cpp                                      */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*		V1.01	                                                  */
/* 			Optimised for speed.                                  */
/* 			Interrupt routines removed from object code           */
/* 			Buffers & data indexes removed from object data space */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include "Serial.hpp"

#define BAUD_PRESCALER(speed,cpu_clk) (((cpu_clk / (speed * 16UL))) - 1)    //The formula that does all the required maths
//#define USER_BAUD

/* ---------------------------------------------------------------------
This may seem very 'C' like , but to optimise the Serial class
we just cannot call a function from within the interrupt handler.
AND you can't have a C++ method in the handler ...
SO
	--	the code to transmit/receive in interrupts is C based.
		which means the buffers and index pointers need to be too.

	This technique is about efficiency and speed, as opposed to being
	a "correct" C++ implementation.
 -------------------------------------------------------------------- */
char	ReceiveBuffer[BUFF_SIZE];
char	TransmitBuffer[BUFF_SIZE];
uint8_t	TxHead,TxTail;
uint8_t	RxHead,RxTail;
/* ------------------------------------------------------------------ */

/** ********************************************************************
 *	\brief	Sets up the USART registers
 *	\param	baud		sets the speed if USER_BAUD is defined else 9600.
 *	\param	bits		sets the number of bits --	defaults to 8.
 *	\param	parity		even odd or no parity	--	defaults to no parity.
 *	\param	stop		number of stop bits		--	defaults to none.
 *	\param	buffered	switches between polled mode without buffering to
 * 						interrupt mode with input and output buffers.\n
 * 						false	=	polled mode.\n
 *						true	=	interrupt mode.
 *	\return		Nothing.
 **********************************************************************/
Serial::Serial(uint16_t baud, uint8_t bits, bool parity, uint8_t stop, bool buffered)
{
#ifdef USER_BAUD
	UBRR0L = (uint8_t)(BAUD_PRESCALER(speed,cpu_clk));
	UBRR0H = (uint8_t)(BAUD_PRESCALER(speed,cpu_clk)>>8);
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	UCSR0C = ((1<<UCSZ00)|(1<<UCSZ01));
#else
	UBRR0L = 103;
	UBRR0H = (103>>8);
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UCSR0C = ((1<<UCSZ00) | (1<<UCSZ01));
#endif
	memset(ReceiveBuffer,0,BUFF_SIZE);
	memset(TransmitBuffer,0,BUFF_SIZE);
	TxHead = 0;
	TxTail = 0;
	RxHead = 0;
	RxTail = 0;
	m_Buffered = buffered;
	if (buffered)
	{
		UCSR0B |= (1 << RXCIE0);	// enable receive interupt
		sei();
	}
}

/** ********************************************************************
 *	\brief		Writes a byte to the serial port
 *	\param		ch	-- the byte to send
 *	\return		true	=	byte sent/queued.\n
 *				false	=	byte not sent/queued.
 **********************************************************************/
bool	Serial::Write(const char ch)
{
	bool result = false;
	if (m_Buffered)
	{
		//	just put it straight in the transmit buffer
		//	and let the interrupt handler take care of sending it.
		if (!TxFull())
		{
			TransmitBuffer[TxHead] = ch;
			TxHead = (TxHead+1) % BUFF_SIZE;
			result = true;
		}
		if (!Transmitting() && !TxEmpty()) StartTx();
	}
	else
	{
		uint16_t wait = 2500;
		do
		{
			if (UCSR0A & (1 << UDRE0))
			{
				UDR0 = ch;
				break;
			}
		} while (--wait);
		result = (wait >0);
	}
	return result;
}

/** ********************************************************************
 *	\brief		Writes a sequnce of bytes to the serial port.
 *	\param		buffer	--	data to be sent/queued
 *	\param		length	--	length of data to send/queue
 *	\return		number of bytes sent/queued
 **********************************************************************/
uint8_t	Serial::Write(const char *buffer, uint8_t length)
{
	uint8_t txlen = 0;
	if (0 == length)	length = strlen(buffer);
	do
	{
		if (Write(*buffer))
		{
			txlen++;
			buffer++;
		}
		else
		{
			break;
		}
	}	while (txlen < length);
	return (txlen);
}

/** ********************************************************************
 *	\brief		Reads data from the serial port/queue
 *	\param[out]	item	read data is returned here
 *	\param[in]	waitfordata	--	in polled mode, wait for a character
 *	\return		true	=	valid data in 'item'.\n
 *				false	=	read failed.
 **********************************************************************/
bool	Serial::Read(char &item, bool waitfordata)
{
	bool result = false;
	if (m_Buffered && !RxEmpty())
	{
		item = ReceiveBuffer[RxTail];
		RxTail = ((RxTail+1) % BUFF_SIZE);
		result = true;
	}
	else
	{
		uint8_t pause = 128;
		while(!(UCSR0A & (1<<RXC0)))
		{
			if (waitfordata) continue;
			if (0 == --pause) break;
		}
		if (pause)
		{
			item = UDR0;
			result = true;
		}
	}
	return result;
}

/** ********************************************************************
 *	\brief		Reads available data from the serial port/queue
 *	\param[out]		buffer		--	buffer to place the read data in.
 *	\param[in]		bufflen		--	the length of the supplied buffer.
 *	\param[in]		waitfordata	--	in polled mode, wait for a character.
 *	\return			number of bytes returned in buffer.
 **********************************************************************/
uint8_t	Serial::Read(char *buffer, uint8_t bufflen, bool waitfordata)
{
	uint8_t rxlen = 0;
	do
	{
		char item;
		if(Read(item,waitfordata))
		{
			*(buffer++) = item;
			rxlen++;
		}
		else
		{
			break;
		}
	}
	while (rxlen < bufflen);
	return rxlen;
}

/** ********************************************************************
 *	\fn			ReceiveInterrupt()
 *	\brief		Receive interrupt function
 *	\param		None
 *	\return		Nothing
 **********************************************************************/
ISR(USART_RX_vect)	//	See comments near the top  of the file
{
	if (((RxHead+1) % BUFF_SIZE) == RxTail)
	{
		return;
	}
	else
	{
		ReceiveBuffer[RxHead] = UDR0;
		RxHead = (RxHead+1) % BUFF_SIZE;
	}
}
/** ********************************************************************
 *	\fn			TransmitInterrupt()
 *	\brief		Transmit register empty interrupt function
 *	\param		None
 *	\return		Nothing
 **********************************************************************/
ISR(USART_UDRE_vect)	//	See comments near the top  of the file
{
	if (TxHead == TxTail)			//	Empty ?
	{
		UCSR0B &= ~(1 << UDRIE0);	// turn tranmsit interrupt OFF.
	}
	else							// NOT Empty
	{
		UDR0 = TransmitBuffer[TxTail];
		TxTail = ((TxTail+1) % BUFF_SIZE);
	}
}
/** ********************************************************************
 *	\fn			TransmitComplete()
 *	\brief		Transmit complete interrupt function
 *	\param		None
 *	\return		Nothing
 **********************************************************************/
ISR(USART_TX_vect)
{
	UCSR0B &= ~(1 << TXCIE0);
}
