/* ****************************************************************/
/* File Name    : serial.h                                        */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __SERIAL_H
#define __SERIAL_H

#ifdef __cplusplus
extern "C"
{
#endif	// of #ifdef __cplusplus

typedef void	(*pRECEIVE_FN)(uint8_t,void *pData);
typedef int8_t	(*pTRANSMIT_FN)(uint8_t*);

void	SERIAL_InitUart(			uint16_t speed, uint32_t cpu_clk);	// Polled serial mode ONLY !!
void	SERIAL_InitSerial(			uint16_t speed, uint32_t cpu_clk);	// Interrupt code or polled
void	SERIAL_SetReceiveHandler(	pRECEIVE_FN pFN, void *pData);		// interrupt mode
void	SERIAL_SetTransmitHandler(	pTRANSMIT_FN pFN);					// interrupt mode
uint8_t	SERIAL_GetByte();												// polled mode
uint8_t	SERIAL_SendByte(char Char);										// polled mode
void	SERIAL_SendMessagePoll(	const char *pu8Message, uint8_t u8Len);		// polled mode
int8_t	SERIAL_SendMessageInt(	const char *pu8Message, uint8_t u8Len);		// interrupt mode
void	SERIAL_Hexdump(void *pBuff, uint16_t u16Len, uint8_t u8Width);

#ifdef __cplusplus
}
#endif	/* end of #ifdef __cplusplus	*/
#endif
