/* *******************************************************************/
/* File Name    : timer.c                                            */
/* Description  : A generic ms based timer that provides the ability */
/*                to callback to a given function on timer expiry.   */
/*                This gives a ms accuracy timer that will only tie  */
/*                up one CPU timer.                                  */
/* Author       : Andrew Carney                                      */
/* Date         : 16th February 2021                                 */
/* Copyright(c) 2021  Andrew Carney                                  */
/* License      : GPL v3.0 or later                                  */
/* *******************************************************************/
/* Modification :	                                                 */
/* *******************************************************************

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
**************************************************************************/
#include <stdio.h>
#include "hwtimer.h"
#include "timer.h"

#define FALSE	0
#define TRUE	!FALSE
#define TIMER_CHAN			(eTimerChan)
#define TIMER_RESOLUTION	(49)	/* 1mS resolution -- PIT fires every milli-second */
#define TIMER_PRESCALER		(4)
#define MAX_TIMER_CHANS		(16)	/* sixteen timers ... from one hardware timer */

#define TIMER_ELAPSED (-1)

static tTIMER_CHANNEL TimerChan[MAX_TIMER_CHANS];
static uint8_t mtimer_initialised = FALSE;
static volatile uint32_t TimerElapsedTime = 0;
static int8_t	MaxToTry = 0;
static uint8_t TimerRunning = FALSE;
static eHW_TIMER eTimerChan = Timer0;	// set a default

/** ********************************************************************
 *	\brief		This is the main functionality of the timer routines

 *				This calls the registered function when the time to do
 *				so has been reached.
 **********************************************************************/
static void TIMER_Handler(void *data)
{
	int8_t chan;
	uint8_t channel_running;

	++TimerElapsedTime;

	channel_running = FALSE;
	for (chan=0;chan<MaxToTry;chan++)
	{
		/* If this channel is active and has expired */
		if(TimerChan[chan].TimerFN && (TimerElapsedTime > TimerChan[chan].TimeOut))
		{
			TimerChan[chan].TimeOut += TimerChan[chan].Interval;
			if (TimerChan[chan].TimerFN)
			{
				if (TimerChan[chan].TimerData)
					(*TimerChan[chan].TimerFN)(TimerChan[chan].TimerData);
				else
					(*TimerChan[chan].TimerFN)(0);
			}
		}
		if (TimerChan[chan].Interval)	 /* this	channel active ? */
		{
			if(TimerChan[chan].Stopped)	 /* has it been stopped ? */
				TimerChan[chan].TimeOut++;	/* YES -- so defer it by 1ms */
			else
				channel_running	= TRUE;
		}
	}
	if (channel_running == FALSE)
	{
		HWTIMER_DisableTimerInts(TIMER_CHAN);
		TimerRunning = FALSE;
	}
}

/** ********************************************************************
 *	\brief		Initialise the Timer

 *	When using the Timer routines, the resolution is as close to 1mS
 *	as is possible.\n
 *	The callback "pTIMER_FN TimerFN" will be called at the selected 
 *	multiple of 1mS.\n
 *	However -- it should be noted that there is a 20uS overhead
 *	in entering and exiting the function due to interrupt overhead 
 *	and processing.\n
 *	Even the simplest function will take a minimum of 25uS
 *	to complete.
 *	\param		eTimer	--	a hardware timer as defined in HwTimer.hpp
 **********************************************************************/
void TIMER_Init(enum tagHW_TIMER eTimer)
{
	int8_t chan;

	if (!mtimer_initialised)
	{
		TIMER_CHAN = eTimer;
		for (chan=0; chan<MAX_TIMER_CHANS; chan++)
		{
			TimerChan[chan].TimerFN = (void*)NULL;
			TimerChan[chan].TimerData = NULL;
			TimerChan[chan].Interval = 0;
			TimerChan[chan].TimeOut = 0;
			TimerChan[chan].Stopped = TRUE;
			TimerChan[chan].InUse	 = FALSE;
		}

		u8HWTIMER_InitTimer(TIMER_CHAN, TIMER_PRESCALER, TIMER_RESOLUTION);
		HWTIMER_SetTimerHandler(TIMER_CHAN, TIMER_Handler);
		u8HWTIMER_StartTimer(TIMER_CHAN);
		mtimer_initialised = TRUE;
	}
}

/** ********************************************************************
 *	\brief		Initialises the data for the next free timer
 *	\return		Handle to the next free timer or -1 on failure
 *********************************************************************/
int8_t TIMER_InitTimer()
{
	int8_t chan = 0;

	while ((TimerChan[chan].InUse) && (chan < MAX_TIMER_CHANS)) chan++;

	if (chan < MAX_TIMER_CHANS)
	{
		if (chan >= MaxToTry)
			MaxToTry = chan + 1;
		TimerChan[chan].InUse = TRUE;
		return chan;
	} else
	return -1;
}

/** ********************************************************************
 *	\brief		Starts the timer
 * 	\param		chan	--	The handle returned from TIMER_InitTimer()
 *	\param		timeout	--	time in milliseconds (mS) before the
 *							callback gets called.
 *	\param		func	--	The function to be called after 'timeout' mS
 *	\param		*data	--	Data that is needed by the function 'func'
 **********************************************************************/
void TIMER_StartTimer(int8_t chan, uint32_t timeout, pTIMER_FN func, void *data)
{
	TimerChan[chan].TimerFN = func;
	TimerChan[chan].TimerData = data;
	TimerChan[chan].Interval = timeout;
	TimerChan[chan].TimeOut = timeout+TimerElapsedTime;
	TimerChan[chan].Stopped = FALSE;
	if (TimerRunning == FALSE)
	{
		HWTIMER_EnableTimerInts(TIMER_CHAN);
		TimerRunning = TRUE;
	}
}

/** ********************************************************************
 *	\brief		Un-pause a timer

 *				This will allow a timer that was previously stopped to
 * 				carry on from where it was stopped.\n
 *				The timer will run for the amount of time left when it
 *				was stopped.
 * 	\param		chan	--	The handle returned from TIMER_InitTimer()
 **********************************************************************/
void TIMER_RestartTimer(int8_t chan)
{
	TimerChan[chan].Stopped = FALSE;
}

/** ********************************************************************
 *	\brief		Resets the timer, thus restarting the timer interval.
 * 	\param		chan	--	The handle returned from TIMER_InitTimer()
 **********************************************************************/
void TIMER_ResetTimer(int8_t chan)
{
	TimerChan[chan].Stopped = FALSE;
	TimerChan[chan].TimeOut = TimerChan[chan].Interval+TimerElapsedTime;
}

/** ********************************************************************
 *	\brief		Stops this timer object.

				this pauses the timer and prevents the handler from
				being called.
 * 	\param		chan	--	The handle returned from TIMER_InitTimer()
 **********************************************************************/
void TIMER_StopTimer(int8_t chan)
{
	TimerChan[chan].Stopped = TRUE;
}

/** ********************************************************************
 *	\brief		Clear out/free up the timer.
 * 	\param		chan	--	The handle returned from TIMER_InitTimer()
 **********************************************************************/
void TIMER_KillTimer(int8_t chan)
{
	/* Have we just killed the biggest timer? */
	if (chan == (MaxToTry-1))
		MaxToTry--;
	TimerChan[chan].TimerFN = (void*) NULL;
	TimerChan[chan].TimerData = NULL;
	TimerChan[chan].Interval = 0;
	TimerChan[chan].TimeOut = 0;
	TimerChan[chan].Stopped = TRUE;
	TimerChan[chan].InUse	 = FALSE;
}

/** ********************************************************************
 *	\brief		Gets Elapsed time for this specific object.
 * 	\param		chan	--	The handle returned from TIMER_InitTimer()
 *	\return		Elapsed time for this timer object.
 **********************************************************************/
uint32_t TIMER_GetElapsedTime(int8_t chan)
{

	if (chan == TIMER_ELAPSED)
		return (TimerElapsedTime);

	if (TimerChan[chan ].Stopped != TRUE)
		return (TimerChan[chan].TimeOut - TimerElapsedTime);
	return (0);
}


