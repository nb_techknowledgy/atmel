/* ****************************************************************/
/* File Name    : LCD.cpp                                         */
/* Description  : C++ class to drives a basic HD4470              */
/*                liquid crystal display                          */
/*              : Unlike the original Arduino library, this class */
/*                1. does NOT support 8 bit mode                  */
/*                2. defaults to using a single write to write to */
/*                   the data bits (D7..D4)                       */
/*                3. defaults to a 20 char by 4 line display      */
/*                4. is optimised for speed, not size             */
/*                5. does not need the Arduino.h header file      */
/*                Derived form Arduino Libary LiquidCrystal.h     */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Migrate from Timer inheritance to DelayTimer to */
/*                implment Delay() function                       */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <stdint.h>

#include "LCD.hpp"

volatile uint8_t *l_pDataPort[] = { (uint8_t*)&PORTB, (uint8_t*)&PORTC, (uint8_t*)&PORTD };
volatile uint8_t *l_pDataPortDirection[] = { (uint8_t*)&DDRB, (uint8_t*)&DDRC, (uint8_t*)&DDRD };

/** ********************************************************************
 *	\brief		Initialises the LCD object.
 *
 *	The code here is quite lengthy. This is to provide a level of
 *	compatability to the Arduino library "LiquidCrystal".
 *	It allows instantiation with similar parameters, and uses the
 *	Arduino pin numbers to set up the ports and pins.
 *	For best effect the data bits should be set to sequential pins on the
 *	same port. This allows the data bits to be written in one, otherwise
 *	each pin of the data port has to be written individually, thus
 *	increasing the software overhead, and slowing the whole process down.
 *	Writing individual bits, which is how the Arduino "LiquidCrystal"
 *	library does it can be forced by setting 'bitwise' true.
 *
 * NOTE	--	This class DOES NOT allow driving the display on all eight
 *			dataport lines.\n
 *			This is a microcontroller -- what is to be gained ?\n
 *			There isn't a single port with all 8 bits available so it
 *			will always need a minimum of 2 writes.
 *			So writing 4 bits twice is just a quick.
 *	\param		rs		pin connected to RS on the liquid crystal display.
 *	\param		enable	pin connected to E  on the liquid crystal display.
 *	\param		d0		pin connected to D0 on the liquid crystal display.
 *	\param		d1		pin connected to D1 on the liquid crystal display.
 *	\param		d2		pin connected to D2 on the liquid crystal display.
 *	\param		d4		pin connected to D3 on the liquid crystal display.
 *	\param		bitwise	Force writing individual bits for D0..D3.
 *	\return		Nothing.
 **********************************************************************/
LCD::LCD(	uint8_t rs,
			uint8_t enable,
			uint8_t d0,
			uint8_t d1,
			uint8_t d2,
			uint8_t d3,
			bool	bitwise)
{
	volatile	uint8_t	*pPORT,*pDDR;
	uint8_t multiport = 0;
	uint8_t	pin;
	uint8_t portnum;
	uint8_t pinarray[6] = { rs,enable,d0,d1,d2,d3 };
	m_pRS_Port				= nullptr;
	m_RS_PortPin			= nullptr;
	m_pRS_PortDirection		= nullptr;

	m_pEnablePort			= nullptr;
	m_pEnablePortDirection	= nullptr;

	m_pDataPort				= nullptr;
	m_pDataPortDirection	= nullptr;

	m_DisplayState			=	LCD_DISPLAYCONTROL;
	m_DisplayMode			=	LCD_ENTRYMODESET | LCD_ENTRYLEFT;
	m_RS_PortBit			=	0;
	m_EnablePortBit			=	0;
	m_DataPortMask			=	0;
	m_DataShift				=	0;
	m_PortNum				=	0;
	m_BitWrite				=	false;	//	if true -- data bits are spread across 2 or more ports !
	m_DataPortBits[0] = 0;		//	PORTB
	m_DataPortBits[1] = 0;		//	PORTC
	m_DataPortBits[2] = 0;		//	PORTD
	for (int k=0;k<6;k++)
	{
		switch(pinarray[k])
		{
			case 2 ... 7 :
				pPORT	=	&PORTD;
				pDDR	=	&DDRD;
				pin = pinarray[k];
				break;
			case 8 ... 13:
				pPORT	=	&PORTB;
				pDDR	=	&DDRB;
				pin = pinarray[k] -8;
				break;
			default:
				break;
		}
		switch(k)
		{
			case 0:
				m_pRS_Port = pPORT;
				m_pRS_PortDirection = pDDR;
				m_RS_PortBit |= (1 << pin);
				m_RS_PortPin =  pPORT - 0x20;
				break;
			case 1:
				m_pEnablePort = pPORT;
				m_pEnablePortDirection = pDDR;
				m_EnablePortBit |= (1 << pin);
				break;
			case 2 ... 5:
				m_DataBit[k-2].Port = pPORT;
				m_DataBit[k-2].Pin  |= (1 << pin);
				if (pPORT == &PORTB)
				{
					m_DataPortBits[0] |= (1 << pin);
				}
				if (pPORT == &PORTC)
				{
					m_DataPortBits[1] |= (1 << pin);
				}
				if (pPORT == &PORTD)
				{
					m_DataPortBits[2] |= (1 << pin);
				}
				break;
		}
	}
	//	========================================================================
	//	ANY bits set in each element adds one to the count but only once per port
	//	So if multiport == 1 .. there is only one data port being used
	//	BUT greater than 1 then more than one port is being used for the data
	//	AND we will then HAVE to write the bits to the ports individually, which
	//	is less than optimal
	for (int k=0; k<3; k++)
	{
		if (m_DataPortBits[k])
		{
			portnum = k;
			multiport++;
		}
	}
	//	========================================================================
	if (multiport > 1)
	{	//	TODO	--	set up ports -- and WriteNibble code !
		m_BitWrite = true;
	}
	else
	{	// need to check for contiguous bits ( no gaps !! )
		m_DataPortMask = 0;
		for (int k=0; k<5; k++)
		{
			if (m_DataPortBits[portnum] & (1 << k))
			{
				if ( (m_DataPortBits[portnum] & ( 1 << (k+1)))
				&&	 (m_DataPortBits[portnum] & ( 1 << (k+2)))
				&&	 (m_DataPortBits[portnum] & ( 1 << (k+3))) )
				{
					m_DataPortMask |= (0xFF & ( 0x0F << k));
					m_pDataPort = l_pDataPort[portnum];
					m_DataShift = k;
					*(l_pDataPortDirection[portnum]) |= m_DataPortBits[portnum];
					m_BitWrite = false;
					break;
				}
				else
				{	//	bits are NOT contiguous, so will need to write then individually
					m_BitWrite = true;
				}
			}
		}
	}
	*m_pRS_PortDirection	|=	m_RS_PortBit;
	*m_pEnablePortDirection	|=	m_EnablePortBit;
	if (bitwise)
	{
		m_BitWrite = true;
	}
	DelayUs(100);
}

/** ********************************************************************
 *	\brief		Initialise the liquid crystal display for first use.
 *	\param		cols	-- the number of columns on the display.
 *	\param		rows	--	the number of rows on the display.
 *	\return		Nothing.
 **********************************************************************/
void	LCD::InitDisplay(uint8_t cols, uint8_t rows)
{
	Delay(50);					//	Spec says wait 15mS after power up
	*m_pRS_Port &= ~m_RS_PortBit;	//	RS = 0 -- about to send init commands
	//-------------------				As per spec --
	for (uint8_t k=0;k<2;k++)
	{
		Delay(1);						//	A bit of settling time
		WriteNibble(3);					//	Send 3
		Delay(4);						//	wait > 4mS
		WriteNibble(3);					//	Send 3
		Delay(4);						//	wait > 4mS
		WriteNibble(3);					//	Send 3
		DelayUs(350);					//	wait > 150uS
		WriteNibble(2);					//	Send 2
		DelayUs(800);
		if (k) Write(0x28);				//	Send 0x28	( b3 = 1 for 2/4 line, b3 = 0 for 1 line )
		else   Write(0x20);
		DelayUs(100);					//				( b2 = 0 for 5x8 char, b2 = 1 for 5x10 chr )
		Write(0x0c);					//	Send 0x08
		DelayUs(100);
		Write(0x01);					//	Send 0x01
		Delay(2);
		Write(0x06);
		DelayUs(100);
	}
	*m_pRS_Port |= m_RS_PortBit;	//	RS = 1 -- done with initialisation
	m_Rows = rows;
	m_Columns = cols;
}

/** ********************************************************************
 *	\brief		Send a command to the display.
 *	\param		cmd	--	command to send.
 *	\return		Nothing.
 **********************************************************************/
void	LCD::Command(uint8_t cmd)
{
	volatile uint8_t k = 20;
	*m_pRS_Port &= ~m_RS_PortBit;
	while(k--);
	k = 20;
	Write(cmd);
	while(k--);
	*m_pRS_Port |= m_RS_PortBit;
}

/** ********************************************************************
 *	\brief		Positions the cursor on the display.
 *	\param		column	--	Which column.
 *	\param		row		--	Which row.
 *	\return		Nothing.
 **********************************************************************/
void	LCD::SetCursor(uint8_t column, uint8_t row)
{
	uint8_t mem_addr = column;			// set initial address value
	if (row > m_Rows)
		row -= m_Rows;
	if (row & 2)
	{
		mem_addr += m_Columns;			//	line 2 .. add display width to address
	}
	if (row & 1)
	{
		mem_addr += 0x40;				// lines 1 and 3 .. add 0x40 to address
	}
	Command(LCD_SETDDRAMADDR | mem_addr);
}

/** ********************************************************************
 *	\brief		Stores a custom character in the character RAM.
 *	\param		loc		--	Which RAM location.
 *	\param		map		--	the character bitmap data.
 *	\return		Nothing.
 **********************************************************************/
void	LCD::CustomChar(uint8_t loc, uint8_t map[])
{
	Command(LCD_SETCGRAMADDR | ((loc & 7)  << 3));
	for (int i=0; i<8; i++)
	{
		Write(map[i]);
	}
}

/** ********************************************************************
 *	\brief		Perform a write sequence to the liquid crystal display

 *				Calls WriteNibble twice to transfer a whole byte of
 *				data to the display, for each byte in the buffer.\n
 *				Terminates when the data to be written is zero.
 *				i.e. the end of a null terminated string.
 *	\param		pData	--	A null terminated string.
 *	\return		The length of data written to the display.
 **********************************************************************/
uint8_t	LCD::Write(const char *pData)
{
	uint8_t len = 0;
	do
	{
		WriteNibble((pData[len] >> 4) & 15);
		WriteNibble(pData[len] &15);
		len++;
	}	while (pData[len]);
	return len;
}

/** ********************************************************************
 *	\brief		Writes a single byte of data to the liquid crystal display

 *				Calls WriteNibble twice to transfer a whole byte of
 *				data to the display.
 *	\param		data	--	The byte to be written.
 *	\return		Nothing.
 **********************************************************************/
void	LCD::Write(uint8_t data)
{
	WriteNibble((data >> 4));
	WriteNibble( data );
}

/** ********************************************************************
 *	\brief		This is uised when the data bits are not contiguous on
 *				on a single port, or spread across more than one port.
 *	\param		data	byte of data to be written.
 *	\return		Nothing.
 **********************************************************************/
void	LCD::WritePins(uint8_t data)
{
	WriteNibblePins((data >> 4) & 15);
	WriteNibblePins( data & 15);
}

#pragma GCC push_options
#pragma GCC optimize ("O3")
/** ********************************************************************
 *	\brief		Toggles the 'E' pin of the display to clock data into
 *				the display
 *	\return		Nothing.
 **********************************************************************/
void	LCD::DoClock()
{
	volatile uint8_t k=5;
	*m_pEnablePort |= m_EnablePortBit;
	while(k--);
	*m_pEnablePort &= ~m_EnablePortBit;
	k = 55;
	while (k--);
}
#pragma GCC pop_options

/** ********************************************************************
 *	\brief		Writes four bits of data to the liquid crystal display

 *				Calls DoClock() to write the data on thye bus to the
 * 				liquid crystal display.
 *	\param		nibble	--	a byte containing the data in the least
 *							significant four bits
 *	\return		Nothing.
 **********************************************************************/
void	LCD::WriteNibble(uint8_t nibble)
{
	if (m_BitWrite)
	{
		for (int k=0;k<4;k++)
		{
			if (nibble & (1 << k))
				*(m_DataBit[k].Port) |= m_DataBit[k].Pin;
			else
				*(m_DataBit[k].Port) &= ~m_DataBit[k].Pin;
		}
	}
	else
	{
		*m_pDataPort = (*m_pDataPort & ~m_DataPortMask) | (( nibble << m_DataShift));
	}
	DoClock();
}

/** ********************************************************************
 *	\brief		Writes four bits of data to the liquid crystal display

 *				Calls DoClock() to write the data on thye bus to the
 *				liquid crystal display.
 *				this version works across multiple ports, and sets
 *				D0..D3 using individual port writes.\n
 * 	NOTE --		SLOWER than a direct write.
 *	\param		nibble	--	a byte containing the data in the least
 *							significant four bits
 *	\return		Nothing.
 **********************************************************************/
void	LCD::WriteNibblePins(uint8_t nibble)
{
	if (nibble & 1)	PORTB |= 1; else PORTB &= ~1;
	if (nibble & 2)	PORTB |= 2; else PORTB &= ~2;
	if (nibble & 4)	PORTB |= 4; else PORTB &= ~4;
	if (nibble & 8)	PORTB |= 8; else PORTB &= ~8;
	DoClock();
}
