/********************************************************************/
/* File Name    : LevelsDisplay.c                              	    */
/* Description  : Uses an Arduino Nano and a 4 digit multiplexed    */
/*                display to display the tank levels on my boat     */
/*                                                                  */
/* Author       : Andrew Carney                                     */
/* Date         : 16th February 2021                               	*/
/* Version      : First Version V1.00                               */
/* Copyright (c) Andrew Carney                                      */
/* ******************************************************************/
/* Modification :	                                                */
/* ******************************************************************

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
************************************************************************/
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <avr/io.h>

#include "hwtimer.h"
#include "timer.h"
#include "serial.h"
#include "adc.h"
#include "LedMux.h"

#define	DEBUG_LEVEL	(1)
#ifdef DEBUG_LEVEL
// #define DEBUG_ADC

#define DEBUG_PRINT(x,y,z)	if (DEBUG_LEVEL >= z) { SERIAL_SendMessageInt(x,y); }
#else
#define DEBUG_PRINT(x,y,z)	while (0) { ; }
#endif

static int index = 0;
void Display_FN(void *pData)
{
	int ADCvalue = -1;
	switch (index)
	{
		case 0:	{
			LEDMUX_DisplayWord("FUEL");
			}	break;
		case 1:
			ADCvalue = ADC_GetValue(5);
			break;
		case 3:	{
			LEDMUX_DisplayWord("H20");
			}	break;
		case 4:
			ADCvalue = ADC_GetValue(6);
			break;
		case 6:	{
			LEDMUX_DisplayWord("Loo");
			}	break;
		case 7:
			ADCvalue = ADC_GetValue(7);
			break;
		default:
			LEDMUX_Blank();
			break;
	}
	if (-1 != ADCvalue)
	{
		if (ADCvalue < 1001)
//			LEDMUX_DisplayUnits(ADCvalue/5,3,1,'L');
			LEDMUX_DisplayNumber(ADCvalue,3,1);
		else
			LEDMUX_DisplayWord("FULL");
	}
#ifdef DEBUG_ADC
	char toprint[40];
	memset(toprint,0,40);
	int length = sprintf(toprint,"ADC(%i) = %i\n\r",index,ADCvalue);
	SERIAL_SendMessagePoll(toprint,length);
#endif
	index++;
	if (index > 9) index = 0;
}

int main()
{
	int8_t	DisplayTimerHandle;

	TIMER_Init(Timer1);	// MUST BE FIRST !!
//	SERIAL_InitSerial(9600,16000000L);	//	assumes 16MHz CPU clock
	ADC_Init();
	LEDMUX_Init();
	ADC_Start(NULL);

	DisplayTimerHandle = TIMER_InitTimer();
	TIMER_StartTimer(DisplayTimerHandle,1000,Display_FN,NULL);
	do
	{
		;	// put any 'housekeeping' code in here ...
	} while (1);
	return 0;
}
