/* ****************************************************************/
/* File Name    : DS18X20.hpp                                     */
/* Description  : Provides class to use Dallas 1-wire temperature */
/*                sensors -- needs the OneWire base class and the */
/*                Timer class to implement timed wait for         */
/*                measurement complete                            */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Migrate from Timer inheritance to DelayTimer to */
/*                implment Delay() function                       */
/*      V1.02     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __DS18B00_HPP
#define __DS18B00_HPP
#include <avr/io.h>
#include <stdint.h>

#include "DelayTimer.hpp"
#include "OneWire.hpp"


class DS18B00	:	public OneWire, public DelayTimer
{
	public:
							DS18B00(volatile	uint8_t *pPort,
												uint8_t Pin,
												bool NoPullup = true,
												uint8_t Family = 0,
												uint8_t Resolution = 12);
				uint16_t	Read(				bool immediate = false);
				float		ReadCelsius(		bool immediate = false);
				float		ReadFahrenheit(		bool immediate = false);
				void		StartMeasurement(	bool broadcast = false);
		inline	uint16_t	Read(ROM device,	bool immediate = false)			\
							{	SetAddress(device);	return Read(immediate);		}
		inline	float		ReadCelsius(ROM device,	bool immediate = false)			\
							{	SetAddress(device);	return ReadCelsius(immediate);	}
		inline	float		ReadFahrenheit(ROM device,	bool immediate = false)			\
							{	SetAddress(device);	return ReadFahrenheit(immediate);	}
		inline	void		StartMeasurement(	ROM device)							\
							{	SetAddress(device);		StartMeasurement(false);	}
				void		SetResolution(uint8_t bits);	//	bits = 9,10,11 or 12 ..
		inline	void		SetAddress(ROM address)					\
							{										\
								for (uint8_t i=0; i<8; i++)			\
									m_ThisDevice[i] = address[i];	\
								m_Read = false;						\
							}
		inline	bool		GetAddress(ROM &address)	\
							{	for (uint8_t i=0; i<8; i++)			\
								{									\
									address[i] = m_ThisDevice[i];	\
								}									\
								return true;						\
							}
				//			-------------------------------------------------------------------
				//			The following affect the wait for measurement complete behaviour
				bool		PollComplete()				{	return m_PollForComplete;	}
				void		PollComplete(bool poll)		{	m_PollForComplete = poll;	}
				bool		WaitComplete()				{	return !m_PollForComplete;	}
				void		WaitComplete(bool wait)		{	m_PollForComplete = !wait;	}
				//			-------------------------------------------------------------------
	private:
		char			m_ScratchRAM[9];
		bool			m_Read;
		bool			m_PollForComplete;
		ROM				m_ThisDevice;
		uint8_t			m_Family;
		uint8_t			m_Resolution;	//	relevant bits are b5 and b6
										//	0 0	==	9	(conv time = 95mS )
										//	0 1	==	10	(conv time = 190mS )
										//	1 0	==	11	(conv time = 380mS )
										//	1 1	==	12	(conv time = 750mS )
};
#endif	//	end if "#ifndef __DS18B00_HPP"
