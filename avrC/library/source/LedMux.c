/* ****************************************************************/
/* File Name    : LedMux.c                                        */
/* Description  :                                                 */
/*                                                                */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
**************************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "LedMux.h"
#include "hwtimer.h"
#include "timer.h"

#define	DEBUG_LEVEL	(1)
#ifdef DEBUG_LEVEL

#include "serial.h"

#define DEBUG_PRINT(x,y,z)	if (DEBUG_LEVEL >= z) { SERIAL_SendMessageInt(x,y); }
#else
#define DEBUG_PRINT(x,y,z)	while (0) { ; }
#endif

#define		segmentA	(4)
#define		segmentB	(8)
#define		segmentC	(16)
#define		segmentD	(32)
#define		segmentE	(64)
#define		segmentF	(128)
#define		segmentG	(256)
#define		segmentDP	(512)

uint16_t const segments[] = {
	( segmentA	+	segmentB	+	segmentC	+	segmentD	+	segmentE	+	segmentF					),	//	0
	( 				segmentB	+	segmentC																	),	//	1
	( segmentA	+	segmentB	+				+	segmentD	+	segmentE					+	segmentG	),	//	2
	( segmentA	+	segmentB	+	segmentC	+	segmentD									+	segmentG	),	//	3
	( 				segmentB	+	segmentC	+				+				+	segmentF	+	segmentG	),	//	4
	( segmentA					+	segmentC	+	segmentD					+	segmentF	+	segmentG	),	//	5
	( segmentA					+	segmentC	+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	6
	( segmentA	+	segmentB	+	segmentC																	),	//	7
	( segmentA	+	segmentB	+	segmentC	+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	8
	( segmentA	+	segmentB	+	segmentC	+	segmentD					+	segmentF	+	segmentG	),	//	9
	( segmentA	+	segmentB	+	segmentC	+				+	segmentE	+	segmentF	+	segmentG	),	//	A
	( 								segmentC	+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	B
	( segmentA					+				+	segmentD	+	segmentE	+	segmentF					),	//	C
	( 				segmentB	+	segmentC	+	segmentD	+	segmentE					+	segmentG	),	//	D
	( segmentA					+				+	segmentD	+	segmentE	+	segmentF	+	segmentG	),	//	E
	( segmentA					+				+				+	segmentE	+	segmentF	+	segmentG	),	//	F

};


static int8_t	LEDMUX_Timer;
static uint16_t	digits[4]  = { 0,0,0,0 };
static uint8_t	current = 0;

/** ********************************************************************
 *	\brief		Retrieves the bitmap of the given number in hex.

 *				Performs a lookup in a table to retrieve a suitable
 * 				segment bitmap.
 *	\param		number	--	the number to be displayed.
 *	\return		a segment bitmap for the supplied number.
 **********************************************************************/
uint16_t LEDMUX_GetHexDigit(uint8_t number)
{
	if ((number >= 0) && (number < 16))
	{
		return (segments[number]);
	}
	return (0);
}

/** ********************************************************************
 *	\brief		Convert the given character to a seven segment bit
 * 				pattern.

 *				This simply maps some alphabetical letters to segments.\n
 *				Not all latters can be represented on a seven segment
 *				display, so invalid characters return a blank.
 *	\param		ch	--	the letter to get a bitamp for.
 *	\return		a segment bitmap for the supplied letter.
 **********************************************************************/
uint16_t LEDMUX_GetLetter(char ch)
{
	switch (ch)
	{
		case 'h':
			return (segmentC + segmentE + segmentF + segmentG );			//	h
		case 'H':
			return (segmentB + segmentC + segmentE + segmentF + segmentG );	//	H
		case 'l':
			return LEDMUX_GetHexDigit(1);									//	l
		case 'L':
			return (segmentD + segmentE + segmentF	);						//	L
		case 'o':
			return (segmentC + segmentD + segmentE + segmentG	);			//	o
		case 'O':
			return LEDMUX_GetHexDigit(0);									//	O
		case 'r':
			return (segmentE + segmentG);									//	r
		case 'R':
			return (segmentA + segmentE + segmentF	);						//	R
		case 'u':
			return (segmentC + segmentD + segmentE );						//	u
		case 'U':
			return (segmentB + segmentC + segmentD + segmentE + segmentF );	//	U
		default:
			return (0);
	}
	return (0);
}

/** ********************************************************************
 *	\brief		Display a four letter word on the display.

 * 				There is no letter 'k' so not that particular word !!\n
 *				Within the limits of letters available on a
 * 				seven segment display, it writes the first four letters
 * 				in 'word' to the display.
 *	\param		word	--	a null terminated string.
 **********************************************************************/
void LEDMUX_DisplayWord(char *word)
{
	uint8_t length = strlen(word);
	uint8_t index = 0;
	uint8_t d_index;
	if (length < 4)
	{
		d_index = 4-length;
	}
	else
	{
		d_index = 0;
	}
	LEDMUX_Blank();
	while (d_index < 4)
	{
		char letter = toupper(word[index]);
		if (letter <= '9')
		{
			digits[d_index] = LEDMUX_GetHexDigit(letter-'0');
		}
		else if (letter <= 'F')
		{
			digits[d_index] = LEDMUX_GetHexDigit(letter-'A'+10);
		}
		else
		{
			digits[d_index] = LEDMUX_GetLetter(word[index]);
		}
		index++;
		d_index++;
	}
}

/** ********************************************************************
 *	\brief		Does the workings of the display.

 *				Called by the base class timer at the multiplexing interval
 * 				At each call blanks the current digit and lights the
 * 				next digit using data from the display buffer.
 **********************************************************************/
static void Muxer_FN(void *pData)
{
	uint8_t this = current;
	PORTD = digits[current] & 255;
	PORTB = (digits[current] >> 8) & 7;
	PORTC = ~(1 << current);
	this++;
	current = 3 & this;
}

/** ********************************************************************
 *	\brief		Initialises all the port pinsnand starts the timer

 *				Sets all the port pins to outputs as needed, blanks the
 *				display buffer, and starts the multiplexing timer at
 *				5mS ( could proabably be slowed down if needed ).
 *	\return		Nothing.
 **********************************************************************/
int LEDMUX_Init()
{
	DDRB = 7;
	DDRC = 15;
	DDRD = 252;
	PORTD = 252;
	PORTC = 15;
	PORTB = 7;
	LEDMUX_Timer = TIMER_InitTimer();
	if (-1 != LEDMUX_Timer)	TIMER_StartTimer(LEDMUX_Timer,5,Muxer_FN,NULL);
	return LEDMUX_Timer;
}

/** ********************************************************************
 *	\brief		Clear the display to blank.

 *				Sets the display buffer to ALL OFF
 **********************************************************************/
void LEDMUX_Blank()
{
	digits[0] = 0;
	digits[1] = 0;
	digits[2] = 0;
	digits[3] = 0;
}

/** ********************************************************************
 *	\brief		Test the display segments all work.

 *				Sets the display buffer to ALL ON.
 **********************************************************************/
void LEDMUX_Test()
{
	LEDMUX_DisplayWord("8888");
	digits[0] += segmentDP;
	digits[1] += segmentDP;
	digits[2] += segmentDP;
}

/** ********************************************************************
 *	\brief		Displays a number between 0 and 9999.
 *	\param		value			value to display.
 *	\param		DPpos			position of the decimal point
 *	\param		LeadingZero		The number of leading zeros
 **********************************************************************/
void LEDMUX_DisplayNumber(uint16_t value,int DPpos,int LeadingZero)
{
	if (value > 9999 )
	{
		LEDMUX_DisplayWord("Err");
	}
	else
	{
		uint8_t d = 3;
		LEDMUX_Blank();
		if(LeadingZero > 0)
		{
			while(LeadingZero--)
			{
				digits[3-(LeadingZero&3)] = LEDMUX_GetHexDigit(0);
			}
		}
		while (value > 0)
		{
			int digit = value % 10;
			digits[d--] = LEDMUX_GetHexDigit(digit & 15);
			value /= 10;
		}
		if ((DPpos > 0) && (DPpos < 4))
		{
			digits[DPpos-1] += segmentDP;
		}
	}
}

/** ********************************************************************
 *	\brief		Display a 3 digit number followed by a 'units' suffix
 * 				as per the 'PostFix' paramater.

 *				Values from 0 to 999 are valid and are displayed in the
 *				leftmost three digits, with the rightmost displaying
 *				the 'units'.
 *	\param		value			A number between 0 and 999.
 *	\param		DPpos			Position of the decimal point.
 *	\param		LeadingZero		How many leading zeros to display.
 *	\param		PostFix			Character to place at the end of the display
 **********************************************************************/
void LEDMUX_DisplayUnits(uint16_t value,int DPpos,int LeadingZero,char PostFix)
{
	if (value > 999 )
	{
		LEDMUX_DisplayWord("Err");
	}
	else
	{
		uint8_t d = 2;
		LEDMUX_Blank();
		if(LeadingZero > 0)
		{
			while(LeadingZero--)
			{
				digits[3-(LeadingZero&3)] = LEDMUX_GetHexDigit(0);
			}
		}
		while (value > 0)
		{
			int digit = value % 10;
			digits[d--] = LEDMUX_GetHexDigit(digit & 15);
			value /= 10;
		}
		if ((DPpos > 0) && (DPpos < 3))
		{
			digits[DPpos-1] += segmentDP;
		}
		digits[3] = LEDMUX_GetLetter(PostFix);
	}
}

/** ********************************************************************
 *	\brief		Allow direct control of all segments of the display.

 *				Write 4 bytes into the display buffer.\n
 * 				All bits set to a 1 will cause the corresponding
 *				segment	to be lit. Aall bits set to a 0 will not.
 *	\param		*Message	--	point to a four byte buffer.
 *				NOTE		--	It is NOT a null terminated string.
 **********************************************************************/
void LEDMUX_DisplayLiteral(uint16_t	*Message)
{
	digits[0] = *Message++;
	digits[1] = *Message++;
	digits[2] = *Message++;
	digits[3] = *Message++;
}

