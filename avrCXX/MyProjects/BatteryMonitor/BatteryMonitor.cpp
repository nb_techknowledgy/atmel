/* ****************************************************************/
/* File Name    : BatteryMonitor.cpp                              */
/* Description  : A program to monitor 3 12V batteries            */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License	: GPL v3.0 or later                                   */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include "A2D.hpp"
#include "MCP3XXX.hpp"
#include "LCD.hpp"

//#define EXTERNAL

LCD lcd(2,3,4,5,6,7,true);
#define SHIFT	(6)
int main()
{
	uint16_t	Filtered[8] = {0,0,0,0,0,0,0,0};
	A2D	adc;
	MCP3008 Eadc;
	DDRB = 3;
	PORTB = 1;
	lcd.InitDisplay();
	adc.Start();
	lcd.Display(true);
#ifdef EXTERNAL
	do
	{
		lcd.Clear();
		for (int k=4; k<8; k++)
		{
			char	buffer[20];
			char	temp[20];
			uint16_t	u16Value;
			uint32_t	u32ExtValue;
			u16Value = adc.GetValue(k);
			u32ExtValue = Eadc.analogRead(k);
			switch (k)
			{
				case 0 ... 3:
					lcd.SetCursor(0,k);
					break;
				case 4 ... 7:
					lcd.SetCursor(0,k-4);
					break;
				default:
					break;
			}
			sprintf(buffer,"CH%i = %04x Ex = %04lx",k,u16Value,u32ExtValue);
			lcd.Write(buffer);
		}
		lcd.Delay(250);
	} while (1);
			
#else
	do
	{
		lcd.Clear();
		for (int k=0; k<8; k++)
		{
			char	buffer[20];
			char	temp[20];
			uint16_t	u16Value;
			uint16_t	u16refAmp;
			uint16_t	u16refVolt;
			//	(Value * 10) + reading) - (Value *10)
			for (int a=0;a<500;a++)
			{
				u16Value = (Filtered[k] - (Filtered[k]>>SHIFT)) + adc.GetValue(k) ;
				Filtered[k] = u16Value;
				u16refAmp  = 512 - ((Filtered[4])>>SHIFT);
				u16refVolt = 876 - ((Filtered[0])>>SHIFT);
				u16Value = Filtered[k] >> SHIFT;
			}
#if 0
			switch (k)
			{
				case 0 ... 3:
					lcd.SetCursor(0,k);
					break;
				case 4 ... 7:
					lcd.SetCursor(10,k-4);
					break;
				default:
					break;
			}
			sprintf(buffer,"CH%i = %04i",k,u16Value);
			lcd.Write(buffer);
#else
			switch (k)
			{
//#if 1
				case 0 ... 3:
					lcd.SetCursor(0,k);
					dtostrf((10.3 + (((u16Value+u16refVolt))/(1024/(18.45-10.3)))),2,1,temp);
					sprintf(buffer,"CH%i %sV",k,temp);
					lcd.Write(buffer);
					break;
//#else
				case 4 ... 7:
					lcd.SetCursor(10,(k-4));
					dtostrf((((u16Value+u16refAmp)/2.56)-200),3,1,temp);
					sprintf(buffer,"CH%i %sA",k,temp);
//					strcat(buffer,temp);
					lcd.Write(buffer);
					break;
//#endif
				default:
					break;
			}
#endif
		}
		lcd.Delay(200);
	}
	while(1);
#endif
	return 0;
}
