/* ****************************************************************/
/* File Name    : LedMux.h                                        */
/* Description  : Provides basic functional control over the      */
/*                Atmega timers                                   */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __LEDMUX_H
#define __LEDMUX_H

#ifdef __cplusplus
extern "C"
{
#endif	// of #ifdef __cplusplus

extern const uint16_t number[];

int  LEDMUX_Init();
void LEDMUX_Blank();
void LEDMUX_Test();
uint16_t LEDMUX_GetHexDigit(uint8_t number);
uint16_t LEDMUX_GetLetter(char ch);
void LEDMUX_DisplayWord(char *word);
void LEDMUX_DisplayNumber(uint16_t value,int DPpos,int LeadingZero);
void LEDMUX_DisplayUnits(uint16_t value,int DPpos,int LeadingZero,char PostFix);
void LEDMUX_DisplayLiteral(uint16_t	*Message);


#ifdef __cplusplus
}
#endif	/* end of #ifdef __cplusplus	*/
#endif	/* end of #ifdef __LEDMUX_H		*/
