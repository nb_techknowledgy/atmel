/* ****************************************************************/
/* File Name    : rn-xv.c	                                      */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include "hwtimer.h"
#include "timer.h"
#include "serial.h"

// --------------------------------------------------------------------
// RN-XV Command strings
// --------------------------------------------------------------------
prog_char command_0[]	PROGMEM = "set time port 123\r ";
prog_char command_1[]	PROGMEM = "save\r ";
prog_char command_2[]	PROGMEM = "exit\r ";
prog_char command_3[]	PROGMEM = "reboot\r ";
prog_char command_4[]	PROGMEM = "set sys printlvl 0x10\r ";
// --------------------------------------------------------------------
prog_char command_5[]	PROGMEM = "set ip dhcp 0\r ";
prog_char command_6[]	PROGMEM = "set wlan ssid BoatControl\r ";
prog_char command_7[]	PROGMEM = "set wlan phrase Controller\r ";
prog_char command_8[]	PROGMEM = "set ip netmask 255.255.0.0\r ";
prog_char command_9[]	PROGMEM = "set ip gw 0.0.0.0\r ";
prog_char command_10[]	PROGMEM = "set wlan auth 0\r ";
// --------------------------------------------------------------------
prog_char command_11[]	PROGMEM = "set wlan channel 0\r";			//	handset ONLY
prog_char command_12[]	PROGMEM = "set wlan join 1\r ";				//	handset only
prog_char command_13[]	PROGMEM = "set ip address 169.254.1.2\r ";	//	handset only
prog_char command_14[]	PROGMEM = "set ip host 169.254.1.1\r ";		//	handset only
prog_char command_15[]	PROGMEM = "set ip remote 2000\r ";			//	handset only
prog_char command_16[]	PROGMEM = "set sys autoconn 1\r ";			//	handset only
// --------------------------------------------------------------------
prog_char command_17[]	PROGMEM = "set wlan channel 4\r";			//	controller ONLY
prog_char command_18[]	PROGMEM = "set wlan join 4\r ";				//	controller ONLY
prog_char command_19[]	PROGMEM = "set ip address 169.254.1.1\r ";	//	controller ONLY
// --------------------------------------------------------------------


#if defined(HANDSET) && !defined(CONTROLLER)
const char *command_table[] =
{
	command_5,		//	set ip dhcp 0
	command_13,		//	set ip address 169.254.1.2
	command_8,		//	set ip netmask 255.255.0.0
	command_9,		//	set ip gw 0.0.0.0
	command_14,		//	set ip host 169.254.1.1
	command_15,		//	set ip remote 2000
	command_6,		//	set wlan ssid BoatControl
	command_7,		//	set wlan phrase Controller
	command_10,		//	set wlan auth 0
	command_11,		//	set wlan channel 0
	command_12,		//	set wlan join 1
	command_16,		//	set sys autoconn 1
	command_1,		//	save
	command_3,		//	reboot
	NULL
};
#define COMMAND_LIST_LENGTH 	(sizeof(command_table)/sizeof(char*))
#endif
#if defined(CONTROLLER) && !defined(HANDSET)
const char *command_table[] =
{
	command_5,		//	set ip dhcp 0
	command_19,		//	set ip address 169.254.1.1
	command_8,		//	set ip netmask 255.255.0.0
	command_9,		//	set ip gw 0.0.0.0
	command_6,		//	set wlan ssid BoatControl
	command_7,		//	set wlan phrase Controller
	command_10,		//	set wlan auth 0
	command_17,		//	set wlan channel 4
	command_18,		//	set wlan join 4
	command_1,		//	save
	command_3,		//	reboot
	NULL
};
#define COMMAND_LIST_LENGTH 	(sizeof(command_table)/sizeof(char*))
#endif

#define RESPONSE_BUFF_LEN	(100)
#define MAX_REQUEST_LEN		(50)

#define STATUS_ONLINE		(0)
#define STATUS_OFFLINE		(1)
#define STATUS_COMPLETE		(2)
#define STATUS_FAILED		(3)


typedef struct tagCommsState
{
	volatile uint8_t	u8CommsStatus;
	volatile uint8_t	u8Retries;
	volatile uint8_t	u8RequestNum;
	volatile uint8_t	u8ResponseLen;
	volatile uint8_t	u8CommsState;
	char				pRequest[MAX_REQUEST_LEN];
	char				pResponse[RESPONSE_BUFF_LEN];
} tCOMMS_STATE, *pCOMMS_STATE;


void SendComment(char *pu8Message, uint8_t u8Len, void *pData);

void CommsTimeOUt(void *pData)
{
	volatile pCOMMS_STATE pState = (pCOMMS_STATE) pData;
	if (STATUS_ONLINE == pState->u8CommsStatus)
	{
		SERIAL_SendMessageInt("$$$",3);
	}
	else
	{
		SERIAL_SendByte('\r');
	}
	pState->u8Retries++;
	if (10 < pState->u8Retries)
	{
		SERIAL_SetReceiveHandler(NULL,NULL);
		pState->u8CommsStatus = STATUS_FAILED;
	}
}

void ResponseHandler(uint8_t u8ReadChar, void *pData)
{	// this is the serial comms handler when setting up the RN-XV
	pCOMMS_STATE pState = (pCOMMS_STATE) pData;

	if (pState->u8ResponseLen < RESPONSE_BUFF_LEN)
	{
		pState->pResponse[pState->u8ResponseLen] = u8ReadChar;
		pState->u8ResponseLen++;
	}
	if ('\r' == u8ReadChar )
	{
		if (NULL != strstr(pState->pResponse, "AOK"))
		{
			pState->u8RequestNum++;		// Next command
		}
		if (NULL != strstr(pState->pResponse, "ERR"))
		{
			;	// nothing to do .....
		}
	}
	if (NULL != strstr(pState->pResponse,"<2.30>"))
	{	// must have a prompt .. have we got a result code ?
		uint8_t u8Len = 0;
		if (NULL == command_table[pState->u8RequestNum])
		{
			SERIAL_SetReceiveHandler(NULL, NULL);
			pState->u8RequestNum = 99;
		}
		else
		{
			memset(pState->pResponse, 0, RESPONSE_BUFF_LEN);
			pState->u8ResponseLen = 0;
			u8Len = strlen_P(command_table[pState->u8RequestNum]);
			memset(pState->pRequest, 0, MAX_REQUEST_LEN);
			strncpy_P(pState->pRequest, command_table[pState->u8RequestNum], MAX_REQUEST_LEN);
			SERIAL_SendMessageInt(pState->pRequest,u8Len);
		}
	}
	if ((NULL != strstr(pState->pResponse,"$$$"))	// if we are seeing characters back .. must be offline
	||  (NULL != strstr(pState->pResponse,"CMD")))	// likewise CMD means successful offline request
	{	// so send a CR to get a prompt
		memset(pState->pResponse, 0, RESPONSE_BUFF_LEN);
		pState->u8ResponseLen = 0;
		pState->u8CommsStatus = STATUS_OFFLINE;
		SERIAL_SendByte('\r');
	}
	if (NULL != strstr(pState->pResponse, "READY"))
	{
		SERIAL_SetReceiveHandler(NULL,NULL);
		pState->u8CommsStatus = STATUS_COMPLETE;	// nothing to do .....
		SERIAL_SendMessageInt("Back ONLINE\n",12);
	}
}

void SendComment(char *pu8Message, uint8_t u8Len, void *pData)
{
	SERIAL_SetReceiveHandler(NULL, NULL);
	SERIAL_SendMessagePoll(pu8Message,u8Len);
	SERIAL_SetReceiveHandler(ResponseHandler, pData);
}

int RNXV_SetupBoard()
{
	static int8_t	i8CommsTimer;
	tCOMMS_STATE	tCommsState;

	memset(&tCommsState, 0, sizeof(tCOMMS_STATE));
	tCommsState.u8RequestNum = 0;
	SERIAL_SetReceiveHandler(ResponseHandler, &tCommsState);
	i8CommsTimer = -1;
	i8CommsTimer = TIMER_InitTimer();
	if (-1 == i8CommsTimer)	return (-1);
	TIMER_Init(Timer0);	// can still be called even after it has been initialised
	TIMER_StartTimer(i8CommsTimer, 1000, CommsTimeOUt, &tCommsState);	// start a timeout timer in case we get no response
	SERIAL_SendMessagePoll("$$$",3);									// Send command mode request


	while (tCommsState.u8CommsStatus < STATUS_COMPLETE)
	{
		if (STATUS_COMPLETE == tCommsState.u8CommsStatus)
		{
			SERIAL_SendMessageInt("Success\n",8);
			return (0);
		}
		if (STATUS_FAILED == tCommsState.u8CommsStatus)
		{
			SERIAL_SendMessageInt("Failure\n",8);
			return (-1);
		}
	}
	return (0);
}

