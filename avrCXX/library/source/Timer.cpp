/* ****************************************************************/
/* File Name    : Timer.cpp                                       */
/* Description  : A generic ms based timer class that provides    */
/*                the ability to callback to a given method on    */
/*                timer expiry. This gives a ms accuracy timer    */
/*                that willonly tie up one CPU timer.             */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <stdint.h>

#include "HwTimer.hpp"
#include "Timer.hpp"

#define TIMER_ELAPSED (-1)

eHW_TIMER	Timer::s_eHWtimer;	// set a default
uint8_t		Timer::s_initialised = false;
uint32_t 	Timer::s_ElapsedTime = 0;
int8_t		Timer::s_MaxToTry = 0;
uint8_t		Timer::s_Running = false;
Timer		*Timer::s_Timer[MAX_TIMER_CHANS];
HwTimer		Timer::s_hwtimer;

extern "C" void __cxa_pure_virtual()	 { while (1); }
/** ********************************************************************
 *	\brief		Instantiates the Timer class.

 *	When using the Timer class, the resolution is as close to 1mS
 *	as is possible.\n
 *	The Callback will be called at the selected multiple of 1mS.\n
 *	However -- it should be noted that there is a 20uS overhead
 *	in entering and exiting the Callback function due to interrupt
 *	overhead and processing.\n
 *	Even the simplest Callback() function will take a minimum of 25uS
 *	to complete.
 *	\param		eTimer	--	a hardware timer as defined in HwTimer.hpp
 *	\return		Nothing
 **********************************************************************/
Timer::Timer(eHW_TIMER eTimer)
{
	int8_t chan = 0;
	if (!s_initialised)
	{
		s_eHWtimer = eTimer;
		for (chan=0; chan<MAX_TIMER_CHANS; chan++)
		{
			s_Timer[chan]->m_Interval = 0;
			s_Timer[chan]->m_TimeOut = 0;
			s_Timer[chan]->m_Stopped = true;
			s_Timer[chan]->m_InUse	 = false;
		}
		s_hwtimer.InitTimer(s_eHWtimer, TIMER_PRESCALER, TIMER_RESOLUTION);
		s_hwtimer.SetTimerHandler(s_eHWtimer, Handler);
		s_hwtimer.StartTimer(s_eHWtimer);
		s_initialised = true;
	}
	chan = 0;
	while (s_Timer[chan] && (s_Timer[chan]->m_InUse) && (chan < MAX_TIMER_CHANS)) chan++;
	if (chan < MAX_TIMER_CHANS)
	{
		if (chan >= s_MaxToTry)
			s_MaxToTry = chan + 1;
		s_Timer[chan] = this;		// this is VITAL to associate elements to objects !
		m_Channel = chan;
		m_InUse = true;
		m_Stopped = true;
	}
	else
	{
		m_Channel = -1;
	}
}

/** ********************************************************************
 *	\brief		Destroys the Timer object and releases the hwtimer if
 *				this is the last timer object instance.
 *	\return		Nothing.
 **********************************************************************/
Timer::~Timer()
{
	KillTimer();
	s_Timer[m_Channel] = 0;		// destroy association before memory gets deleted
	for (int k=0;k<MAX_TIMER_CHANS;k++)
	{
		if (s_Timer[k])
		{
			return;		// still got active timers - so exit NOW !
		}
	}
	// if here, no active timers to stop interrupts and clear down
	s_hwtimer.DisableTimerInts(s_eHWtimer);
	s_eHWtimer = (eHW_TIMER)0;
}

/** ********************************************************************
 *	\brief		This is the main worker method of the timer class.

 *				This is common across all instances of the timer class
 *				and calls the appropriate obejct's callback function
 *				when the time to do so has been reached.
 *	\return		Nothing.
 **********************************************************************/
void Timer::Handler()
{
	int8_t chan;
	uint8_t channel_running;

	channel_running = true;	//	false;
	SET_PIN(1,1);
	++s_ElapsedTime;
	for (chan=0;chan<s_MaxToTry;chan++)
	{
		// If this channel is active and has expired
		if (s_Timer[chan])	// check if pointing to a valid object ??
		{
			if(s_Timer[chan]->m_Stopped)	 /* has it been stopped ? */
			{
				s_Timer[chan]->m_TimeOut++;	/* YES -- so defer it by 1ms */
			}
			else
			{
				if (s_ElapsedTime > s_Timer[chan]->m_TimeOut)
				{
					s_Timer[chan]->m_TimeOut += s_Timer[chan]->m_Interval;
					SET_PIN(2,1);
					s_Timer[chan]->Callback();
					SET_PIN(2,0);
				}
				channel_running	= true;
			}
		}
	}
	if (channel_running == false)
	{
		s_hwtimer.DisableTimerInts(s_eHWtimer);
		s_Running = false;
	}
	SET_PIN(1,0);
}

/** ********************************************************************
 *	\brief		Starts the timer
 *	\param		timeout	--	time in milliseconds (mS) before the
 *							callback gets called.
 *	\param		pData		Data that is needed by the object's callback
 *	\return		Nothing.
 **********************************************************************/
void Timer::StartTimer(uint32_t timeout, void *pData)
{
	m_pData = pData;
	m_Interval = timeout;
	m_TimeOut = timeout+s_ElapsedTime;
	m_Stopped = false;
	if (s_Running == false)
	{
		s_hwtimer.EnableTimerInts(s_eHWtimer);
		s_Running = true;
	}
}

/** ********************************************************************
 *	\brief		Un-pause a timer

 *				This will allow a timer that was previously stopped to
 * 				carry on from where it was stopped.\n
 *				The timer will run for the amount of time left when it
 *				was stopped.
 *	\return		Nothing.
 **********************************************************************/
void Timer::RestartTimer()
{
	m_Stopped = false;
}

/** ********************************************************************
 *	\brief		Resets the timer, thus restarting the timer interval.
 *	\return		Nothing.
 **********************************************************************/
void Timer::ResetTimer()
{
	m_Stopped = false;
	m_TimeOut = m_Interval+s_ElapsedTime;
}

/** ********************************************************************
 *	\brief		Stops this timer object.

				this pauses the timer and prevents the handler from
				being called.
 *	\return		Nothing.
 **********************************************************************/
void Timer::StopTimer()
{
	m_Stopped = true;
}

/** ********************************************************************
 *	\brief		Clear out/free up the timer.
 *	\return		Nothing.
 **********************************************************************/
void Timer::KillTimer()
{
	/* Have we just killed the biggest timer? */
	if (m_Channel == (s_MaxToTry-1))
		s_MaxToTry--;
	m_Interval = 0;
	m_TimeOut = 0;
	m_Stopped = true;
	m_InUse	 = false;
}

/** ********************************************************************
 *	\brief		Gets Elapsed time for this specific object.
 *	\return		Elapsed time for this timer object.
 **********************************************************************/
uint32_t Timer::GetElapsedTime()
{
	if (m_Channel == TIMER_ELAPSED)
		return (s_ElapsedTime);

	if (m_Stopped != true)
		return (m_TimeOut - s_ElapsedTime);
	return (0);
}
