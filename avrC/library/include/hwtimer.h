/* ****************************************************************/
/* File Name    : hwtimer.h                                       */
/* Description  : Provides basic functional control over the      */
/*                Atmega timers                                   */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __HWTIMER_H
#define __HWTIMER_H

//#include <string.h>		// needed for memset

#define	SYS_CLK_KHZ	(16000000L)

#ifdef __cplusplus
extern "C"
{
#endif	// of #ifdef __cplusplus

typedef enum tagHW_TIMER
{
	Timer0,
	Timer1,
	Timer2
} eHW_TIMER;

typedef enum tagHW_TIMERHANDLE
{
	TimerHandle0,
	TimerHandle1,
	TimerHandle2
} eHW_TIMERHANDLE;

/* definition of timer handler function pointers */
typedef void	(*pHWTIMER_FN)(void*);

/* FUNCTION PROTOTYPES -- see .c file for details */
void u8HWTIMER_ReleaseTimer(					eHW_TIMERHANDLE eTimerHandle);
eHW_TIMERHANDLE u8HWTIMER_AcquireTimer(			eHW_TIMER eTimer);
uint8_t	u8HWTIMER_IsTimerLocked(		eHW_TIMER eTimer);


uint8_t			u8HWTIMER_InitTimer(			eHW_TIMER eTimer,	uint8_t u8Prescale, uint16_t u16Interval);
uint8_t			u8HWTIMER_InitTimerUs(			eHW_TIMER eTimer,	uint16_t u8IntervalUS);
uint8_t			u8HWTIMER_StartTimer(			eHW_TIMER eTimer);
uint8_t			u8HWTIMER_StopTimer(			eHW_TIMER eTimer);
uint16_t		u16HWTIMER_GetTimer(			eHW_TIMER eTimer);
void			HWTIMER_EnableTimerInts(		eHW_TIMER eTimer);
void			HWTIMER_DisableTimerInts(		eHW_TIMER eTimer);
void			HWTIMER_SetTimerHandler(		eHW_TIMER eTimer,	pHWTIMER_FN pFN);
uint16_t		u16HWTIMER_GetTimerElapsedTime(	eHW_TIMER eTimer);


#ifdef __cplusplus
}
#endif	/* end of #ifdef __cplusplus	*/
#endif	/* end of #ifdef __HWTIMER_H		*/
