/* ****************************************************************/
/* File Name    : LedMux.hpp                                      */
/* Description  :                                                 */
/*                                                                */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __LEDMUX_HPP
#define __LEDMUX_HPP

#include "Timer.hpp"

class LedMux : public Timer
{
	public:
		LedMux();
		~LedMux();
		void		Blank();
		void		Test();
		void		Callback();
		uint16_t	GetHexDigit(uint8_t number);
		uint16_t	GetLetter(char ch);
		void		DisplayWord(const char *word);
		void		DisplayNumber(uint16_t value,int DPpos,int LeadingZero);
		void		DisplayUnits(uint16_t value,int DPpos,int LeadingZero,char PostFix);
		void		DisplayLiteral(uint8_t	*Message);
	private:
		static	uint16_t	s_digits[4];
};

#endif	/* end of #iifdef __LEDMUX_HPP		*/
