/* ********************************************************************/
/* File Name    : SPI.cpp                                             */
/* Description  : C++ class to drive the Atmel SPI port               */
/*                Unlike the correpsonding Arduino library this       */
/*                class does NOT provide a 'bit-bang' style interface */
/*                It will ONLY operate on the dedicated SPI lines     */
/*                the devce select line can be chosen which allows    */
/*                more than one device to be attached and driven      */
/*                with each device being driven by a separate class   */
/*                instance.                                           */
/*                This DOES NOT operate in interrupt mode, but may be */
/*                a future enhancement if the speed gain is deemed    */
/*                worth the effort                                    */
/* Author       : Andrew Carney                                       */
/* Date         : 16th February 2021                                  */
/* Version      : First Version for Atmel devices V1.00               */
/* Copyright(c) 2021  Andrew Carney                                   */
/* License      : GPL v3.0 or later                                   */
/* ********************************************************************/
/* Modification :                                                     */
/*      V1.01     Doxygen documentation hooks added                   */
/* ********************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

***********************************************************************/
#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

#include "SPI.hpp"
#include "LCD.hpp"

const uint32_t	F_CPU PROGMEM = 16000000L;

typedef union tagWORD
{
	uint16_t Word;
	struct
	{
		uint8_t LSB;
		uint8_t MSB;
	};
}	tWORD, *pWORD;

typedef union tagLONG
{
	uint32_t Long;
	struct
	{
		tWORD HIGH;
		tWORD LOW;
	};
}	tLONG, *pLONG;

/*		Setting the clock --
		SPR1 SPR0 SPI2X Freq
		0    0     1   fosc/2
		0    0     0   fosc/4
		0    1     1   fosc/8
		0    1     0   fosc/16
		1    0     1   fosc/32
		1    0     0   fosc/64
		1    1     1   fosc/64
		1    1     0   fosc/128
*/
/** ********************************************************************
 *	\brief		Sets up the hardware SPI port.

 *				This class DOES NOT do software (bitbang) SPI.\n
 *				It is strictly a hardware SPI class.
 *	\param		Clock	--	the clock speed for the SPI bus.
 *	\param		BitOrder	MSB or LSB first ??
 *	\param		Mode		One of four possible combinations of clock
 *							and data.
 *	\return		Nothing.
 **********************************************************************/
SPI::SPI(uint32_t Clock, uint8_t BitOrder, SPImode Mode)
{
	uint8_t __SPCR = 0;
	uint8_t ClockDivider = 0;
	while ((ClockDivider < 6) && (Clock < (uint32_t)(F_CPU >> ClockDivider))) { ClockDivider++; }
	if (ClockDivider == 6)	ClockDivider = 7;	// Allow for the second fosc/64
/*	from the avr-gcc headers --
		SPCR 		--	the register
		============================
		The bits --
		SPR0			0
		SPR1			1
		CPHA			2
		CPOL			3
		MSTR			4
		DORD			5
		SPE				6
		SPIE			7
*/
	//	Set port direction -- no decision here as we are NOT doing bit bang SPI
	//	This is a HARDWARE ONLY SPI interface
	DDRB |= (1 << PORTB5) | (1 << PORTB3) | ( 1 << PORTB2) | ( 1 << PORTB1);
	__SPCR	&=	~(1 << SPE);		//	DO NOT enable SPI yet -- that will be done later
    if (LSB_FIRST == BitOrder)
    {
		__SPCR |= ( 1<< DORD);		//	Set bit order
	}
	__SPCR |=	(1 << MSTR)	;		//	Set MASTER
	__SPCR |=	 Mode;				//	Set Clock polarity and phase
	__SPCR |=	((ClockDivider >> 1) & ((1 << SPR0) | (1 << SPR1)));	//	Set SPI speed
	  SPSR |=	( ~ClockDivider & 1);									//	Set speed multiplier
	SPCR = __SPCR;					// finally write to register
	m_pCS_Port = &PORTB;
	m_CS_PinMask = (1 << PORTB2);		//	SS pin

}

/** ********************************************************************
 *	\brief		Selects which pin to use as the CS ( bar) pin.
 *	\param		Pointer to the port to use.
 *	\param		Which pin to use on the port.
 *	\return		Nothing.
 **********************************************************************/
void	SPI::SetCSpin(uint8_t *pPort, uint8_t Mask)
{
	m_pCS_Port = pPort;
	m_CS_PinMask = Mask;
}

/** ********************************************************************
 *	\brief		Send and receive a single 8 bit byte.
 *	\param		Data	--	byte to send.
 *	\return		Byte received.
 **********************************************************************/
uint8_t		SPI::Transfer(uint8_t Data)
{
	uint8_t NOP = 2;
	*m_pCS_Port &= ~m_CS_PinMask;
	NOP--;		// used instead of  asm("nop")
	SPDR = Data;
	NOP--;		// used instead of  asm("nop")
	while (!(SPSR & _BV(SPIF))) ; // wait
	*m_pCS_Port |= m_CS_PinMask;
	return SPDR;
}

/** ********************************************************************
 *	\brief		Send and receive a 16 bit word.
 *	\param		Data	--	word to send.
 *	\return		Word received.
 **********************************************************************/
uint16_t	SPI::Transfer(uint16_t SendData)
{
	volatile uint8_t NOP = 4;
	tWORD Data;
	Data.Word = SendData;
	*m_pCS_Port &= ~m_CS_PinMask;
	if (!(SPCR & _BV(DORD)))
	{
		SPDR = Data.MSB;
		NOP--;
		while (!(SPSR & _BV(SPIF))) ;
		Data.MSB = SPDR;
		SPDR = Data.LSB;
		NOP--;
		while (!(SPSR & _BV(SPIF))) ;
		Data.LSB = SPDR;
	}
	else
	{
		SPDR = Data.LSB;
		NOP--;
		while (!(SPSR & _BV(SPIF))) ;
		Data.LSB = SPDR;
		SPDR = Data.MSB;
		NOP--;
		while (!(SPSR & _BV(SPIF))) ;
		Data.MSB = SPDR;
	}
	*m_pCS_Port |= m_CS_PinMask;
	return Data.Word;
}

/** ********************************************************************
 *	\brief		Send and receive a 32 bit Long.
 *	\param		SendData	--	Long to send.
 *	\return		Long received.
 **********************************************************************/
uint32_t	SPI::Transfer(uint32_t SendData)
{
	tLONG  Data;
	uint8_t Send[4],Recv[4];
	Data.Long = SendData;
	if (!(SPCR & (1 << DORD)))
	{
		Send[0] = Data.HIGH.MSB;
		Send[1] = Data.HIGH.LSB;
		Send[2] = Data.LOW.MSB;
		Send[3] = Data.LOW.LSB;
	}
	else
	{
		Send[3] = Data.HIGH.MSB;
		Send[2] = Data.HIGH.LSB;
		Send[1] = Data.LOW.MSB;
		Send[0] = Data.LOW.LSB;
	}
	Transfer(Send,Recv,4);
	if (!(SPCR & (1 << DORD)))
	{
		Data.HIGH.MSB	= Recv[0];
		Data.HIGH.LSB	= Recv[1];
		Data.LOW.MSB	= Recv[2];
		Data.LOW.LSB	= Recv[3];
	}
	else
	{
		Data.HIGH.MSB	= Recv[3];
		Data.HIGH.LSB	= Recv[2];
		Data.LOW.MSB	= Recv[1];
		Data.LOW.LSB	= Recv[0];
	}
	return Data.Long;
}

/** ********************************************************************
 *	\brief		Send and Receive a stream of data.
 *	\param[in]	SendData	--	data to be sent over SPI.
 *	\param[out]	RecvData		data received over SPI.
 *	\param[in]	DataLen			length of buffer/transfer.
 *	\return		Number of bytes transferred.
 **********************************************************************/
uint8_t		SPI::Transfer(uint8_t SendData[], uint8_t RecvData[], uint8_t DataLen)
{
	uint8_t idx = 0;
	if (0 == DataLen)
		return (0);
	else
	{
		*m_pCS_Port &= ~m_CS_PinMask;
		do
		{
			SPDR = SendData[idx];
			while (!(SPSR & _BV(SPIF))) ;
			RecvData[idx] = SPDR;
			idx++;
			SPDR = SendData[idx];
		} while (--DataLen > 0);
		*m_pCS_Port |= m_CS_PinMask;
	}
	return (idx);
}
