/* ****************************************************************/
/* File Name    : HwTimer.hpp                                     */
/* Description  : Provides basic functional control over the      */
/*                Atmega timers                                   */
/* Author       : Andrew Carney                                   */
/* Date         : 16th February 2021                              */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/*      V1.01     Doxygen documentation hooks added               */
/* *****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#ifndef __HWTIMER_HPP
#define __HWTIMER_HPP

#include <string.h>		// needed for memset

//#define	CHECK_TIMER_TIMINGS
#ifdef		CHECK_TIMER_TIMINGS
#include <avr/io.h>
#define		SET_PIN(pin,state)	state ? PORTD |= (1 << (pin+2)) : PORTD &= ~(1 << (pin+2));
#define		TOGGLE(pin)			PORTD ^= (1 << (pin+2))
#define		LA_DIRPORT	DDRD
#else
#define		SET_PIN(pin,state) while(0) {;}
#define		TOGGLE(pin)			while(0)	{;}
#endif

#define	SYS_CLK	(16000000L)

#ifdef __cplusplus
extern "C"
{
#endif	// of #ifdef __cplusplus

/* definition of timer handler function pointers */
typedef void	(*pHWTIMER_FN)();

#ifdef __cplusplus
}
#endif	/* end of #ifdef  __cplusplus	*/

typedef enum tagHW_TIMER
{
	Timer0,
	Timer1,
	Timer2
} eHW_TIMER;

typedef enum tagHW_TIMERHANDLE
{
	TimerHandle0,
	TimerHandle1,
	TimerHandle2
} eHW_TIMERHANDLE;

class HwTimer
{
	public:
		void 			ReleaseTimer(		eHW_TIMERHANDLE eTimerHandle);
		eHW_TIMERHANDLE AcquireTimer(		eHW_TIMER eTimer);
		uint8_t			IsTimerLocked(		eHW_TIMER eTimer);
		uint8_t			InitTimer(			eHW_TIMER eTimer,	uint8_t u8Prescale = 3, uint16_t u16Interval = 250);
		uint8_t			InitTimerUs(		eHW_TIMER eTimer,	uint16_t u16IntervalUS);
		uint8_t			StartTimer(			eHW_TIMER eTimer);
		uint8_t			StopTimer(			eHW_TIMER eTimer);
		uint16_t		GetTimer(			eHW_TIMER eTimer);
		void			EnableTimerInts(	eHW_TIMER eTimer);
		void			DisableTimerInts(	eHW_TIMER eTimer);
		void			SetTimerHandler(	eHW_TIMER eTimer,	pHWTIMER_FN pFN);
		uint16_t		GetTimerElapsedTime(eHW_TIMER eTimer);
		uint8_t			Calculate_TimerRegisters8bit(uint16_t frequency, uint8_t *prescaler);
		uint16_t		Calculate_TimerRegisters16bit(uint32_t frequency, uint8_t *prescaler);
		static	void	DefaultHandler0();
		static	void	DefaultHandler1();
		static	void	DefaultHandler2();
	private:
		static	pHWTIMER_FN s_pHandlerTimer0;
		static	pHWTIMER_FN s_pHandlerTimer1;
		static	pHWTIMER_FN s_pHandlerTimer2;
		static	uint8_t		s_TimerLock[3];
};
#endif	// end of #ifdef __HWTIMER_H
