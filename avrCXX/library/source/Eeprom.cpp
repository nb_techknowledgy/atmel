/* ****************************************************************/
/* File Name    : Eeprom.cpp                                      */
/* Description  : A simple class to write to and read from the    */
/*                eeprom in an AVR micorcontroler                 */
/* Author       : Andrew Carney                                   */
/* Date         : 12th March 2021                                 */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**********************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <stdint.h>
#define SIGRD 5
#include <avr/boot.h>

#include "Eeprom.hpp"

/** ********************************************************************
 *	\brief		Reads a byte from the onboard EEPROM.

				the address is not checked for validity so accesses
				beyond the end of the memory are not valid.
 *	\param		address		--	Location in the eeprom.
 *	\return		The byte stored at 'address'.
 **********************************************************************/
uint8_t Eeprom::Read(uint16_t address)
{
#if 0
	return eeprom_read_byte((uint8_t*)address);
#else
	cli();
	while(EECR & (1<<EEPE))	;
	EEAR = address;
	EECR |= (1<<EERE);
	sei();
	return (EEDR);
#endif
}

/** ********************************************************************
 *	\brief		Reads two bytes from the onboard EEPROM as a uint16.

				The address is not checked for validity so accesses
				beyond the end of the memory are not valid.
 *	\param		address		--	Location in the eeprom.
 *	\return		The bytes stored at 'address'and 'address+1'
 **********************************************************************/
uint16_t Eeprom::ReadWord(uint16_t address)
{
	uint8_t MSB;
	uint8_t LSB;
	MSB = Read(address);
	LSB = Read(address+1);
	return (MSB << 8) + LSB;
}

/** ********************************************************************
 *	\brief		Reads multiple bytes from the onboard EEPROM.

				The address is not checked for validity so accesses
				beyond the end of the memory are not valid.
 *	\param[in]	address		--	Location in the eeprom.
 *	\param[out]	buffer		--	Bytes from the eeprom are returned in
								the buffer provided.
 *	\param[in]	bufflen		--	The length of the buffer.
 *	\return		The bytes stored at 'address' to 'address+bufflen'
 **********************************************************************/
uint16_t Eeprom::Read(uint16_t address, uint8_t *buffer, uint16_t bufflen)
{
	uint16_t readlen = 0;
	do
	{
		*(buffer++) = Read(address++);
		readlen++;
	} while ((readlen < bufflen) && ( address <= m_MaxAddr));
	return readlen;
}

/** ********************************************************************
 *	\brief		Writes a byte into the EEPROM.

		NOTE	--	This uses an avr-gcc library routine as the code
					that is part of the '#else' doesn't work, despite
					it coming direct from the datasheet !
 *	\param			address		Location to write to.
 *	\param			byte		Data to write to location.
 *	\return			Nothing.
 **********************************************************************/
void Eeprom::Write(uint16_t address, uint8_t byte)
{
#if 1
	//	Using the library routine -- because IT WORKS !!
	eeprom_write_byte ((uint8_t*)address, byte);
#else
	//	This is the 'recommended' code to write the eeprom
	//	BUT -- it simply doesn't work !
	cli();
	while(EECR & (1<<EEPE))	;
	EEAR = address;
	EEDR = byte;
	EECR |= (1<<EEMPE);
	EECR |= (1<<EEPE);
	sei();
#endif
}

/** ********************************************************************
 *	\brief		Writes a word into the EEPROM.

		NOTE	--	This uses an avr-gcc library routine as the code
					that is part of the '#else' doesn't work, despite
					it coming direct from the datasheet !
 *	\param			address		Location to write to.
 *	\param			word		Data to write to location.
 *	\return			Nothing.
 **********************************************************************/
void Eeprom::Write(uint16_t address, uint16_t word)
{
	Write(address,   (uint8_t)((word>>8) & 0xFF));
	Write(address+1, (uint8_t)((word   ) & 0xFF));
}

/** ********************************************************************
 *	\brief		Writes multiple bytes to the onboard EEPROM.

				The address is not checked for validity so accesses
				beyond the end of the memory are not valid.
 *	\param[in]	address		--	Location in the eeprom.
 *	\param[in]	buffer			Bytes in buffer are written to eeprom.
 *	\param[in]	length		--	The length of the buffer.
 *	\return		The number of bytes written.
 **********************************************************************/
uint16_t Eeprom::Write(uint16_t address, const uint8_t *buffer, uint16_t length)
{
	uint16_t written = 0;
	do
	{
		Write(address++,*(buffer++));
		written++;
	} while ((written < length) && ( address <= m_MaxAddr));
	return written;
}

/** ********************************************************************
 *	\brief		Reads device signature to lookup the size of the EEPROM.

	NOTE	--	It also populates the device signature, which can then
				be accessed by a call to GetDeviceId().
				This method is called from the constructor.
 *	\return		The size of the eeprom on the device.
 **********************************************************************/
uint16_t	Eeprom::GetSizeFromDevice()
{
	m_DeviceId = boot_signature_byte_get(0x0000);
	m_DeviceId = m_DeviceId << 8;
	m_DeviceId |= boot_signature_byte_get(0x0002);
	m_DeviceId = m_DeviceId << 8;
	m_DeviceId |= boot_signature_byte_get(0x0004);
	switch (m_DeviceId)
	{
		case 0x1e9205:		//	ATmega48A	0x1E 0x92 0x05
		case 0x1e920a:		//	ATmega48PA	0x1E 0x92 0x0A
			return 256;		//	256 bytes of eeprom
		case 0x1e930a:		//	ATmega88A	0x1E 0x93 0x0A
		case 0x1e930f:		//	ATmega88PA	0x1E 0x93 0x0F
		case 0x1e9406:		//	ATmega168A	0x1E 0x94 0x06
		case 0x1e940b:		//	ATmega168PA	0x1E 0x94 0x0B
			return 512;		//	512 bytes of eeprom
		case 0x1e9514:		//	ATmega328	0x1E 0x95 0x14
		case 0x1e950f:		//	ATmega328P	0x1E 0x95 0x0F
			return 1024;	//	1Kb of eeprom
		default:
			break;
	}
	return 0;
}
