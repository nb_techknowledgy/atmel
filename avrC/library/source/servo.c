/* ****************************************************************/
/* File Name    : MotorControl.c                                  */
/* Description  :                                                 */
/* Author       : Andrew Carney                                   */
/* Date         : 15 February 2021                                */
/* Version      : First Version for Atmel devices V1.00           */
/* Copyright(c) 2021  Andrew Carney                               */
/* License      : GPL v3.0 or later                               */
/* ****************************************************************/
/* Modification :                                                 */
/* ****************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

**************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <avr/io.h>
#include "eprom.h"
#include "hwtimer.h"
#include "timer.h"
#include "pwm.h"
#include "serial.h"
#include "adc.h"

//	#define	DEBUG_SERVO

typedef struct tagControl
{
	uint8_t		ControlTimer;
	eHW_TIMER	eServoTimer;
	char		chPWMpin;
	int16_t		i16MinPosition;
	int16_t		i16MaxPosition;
	volatile	uint8_t		u8ServoADC;
	volatile	int16_t 	i16Position;
	volatile	uint8_t 	u8ClutchState;
} tCONTROL, *pCONTROL;

#define FALSE	(0)
#define TRUE	(1)

#define ENGAGE_CLUTCH	(PORTB |=  (1 << PB5))
#define RELEASE_CLUTCH	(PORTB &= ~(1 << PB5))
#define DIRECTION_IN	(PORTB |=  (1 << PB4))
#define DIRECTION_OUT	(PORTB &= ~(1 << PB4))

// As the servo may NOT travel over it's full range,
//		these extents need to be detected , and stored somewhere
static	tCONTROL	tControl;


static void ActuatorControlFN(void *pData)
{
	pCONTROL pControl = (pCONTROL) pData;

	if (pControl->u8ClutchState)
	{
		if (0 == (PORTB & (1 << PB5)))	// is clutch released ?
		{
			SERIAL_SendByte('C');
			ENGAGE_CLUTCH;
		}
		else							// or is clutch engaged ?
		{
			int iADCvalue = ADC_GetValue(pControl->u8ServoADC);
			int iDiff = abs(pControl->i16Position - iADCvalue);
			int iPWMvalue = 255;
			if (iDiff <100)		iPWMvalue = (2*iDiff);
			if (iPWMvalue < 65) iPWMvalue = 65;
			if (iADCvalue < (pControl->i16Position-8))
			{
				PWM_Start(pControl->eServoTimer,pControl->chPWMpin,iPWMvalue);// drive in ...
				DIRECTION_IN;
			}
			else if (iADCvalue > (pControl->i16Position+8))
			{
				PWM_Start(pControl->eServoTimer,pControl->chPWMpin,iPWMvalue);				// drive out ...
				DIRECTION_OUT;
			}
			else
			{
				PWM_Stop(pControl->eServoTimer,pControl->chPWMpin);
			}
		}
	}
	else
	{
		if ((PORTB & (1 << PB5)))	// is clutch engaged ?
		{
			PWM_Stop(pControl->eServoTimer,pControl->chPWMpin);
			SERIAL_SendByte('c');
			RELEASE_CLUTCH;
		}
	}
}

static int16_t calibrate_min()
{
	int16_t i16Position = 0;
	EPROM_Set_UINT16(SERVO_MIN_POSITION,i16Position);
	return(i16Position);
}

static int16_t calibrate_max()
{
	int16_t i16Position = 0;
	EPROM_Set_UINT16(SERVO_MAX_POSITION,i16Position);
	return(i16Position);
}

void SERVO_Init(eHW_TIMER ePWMtimer, int u8ServoADC)	// need params for setting which pins and timers !!)
{
	memset(&tControl, 0, sizeof(tCONTROL));
	tControl.ControlTimer = -1;
	tControl.u8ServoADC = u8ServoADC;
	tControl.eServoTimer = ePWMtimer;
	tControl.chPWMpin = 'A';
	tControl.i16Position = 0;
	tControl.i16MinPosition = 50;
	tControl.i16MaxPosition = 950;
	tControl.u8ClutchState = FALSE;

//	tControl.i16MinPosition = EPROM_Get_UINT16(SERVO_MIN_POSITION);
//	tControl.i16MaxPosition = EPROM_Get_UINT16(SERVO_MAX_POSITION);

	PWM_Init(1000, ePWMtimer);
	tControl.ControlTimer			=	TIMER_InitTimer();
	if (-1 != tControl.ControlTimer)	TIMER_StartTimer(tControl.ControlTimer,50,ActuatorControlFN,&tControl);
	ADC_Start();
/*
	if (-1 == tControl.i16MinPosition)
	{
		// value not stored yet -- need to run calibrate_min
	}
	if (-1 == tControl.i16MaxPosition)
	{
		// value not stored yet -- need to run calibrate_max
	}
*/
}

int16_t SERVO_SetPosition(uint16_t u16Position)
{
	int16_t i16SafePosition = u16Position;
	if (0 < tControl.i16MaxPosition)
	{
		if (u16Position > tControl.i16MaxPosition)
			i16SafePosition = tControl.i16MaxPosition;
	}
	if (0 < tControl.i16MinPosition)
	{
		if (u16Position < tControl.i16MinPosition)
			i16SafePosition = tControl.i16MinPosition;
	}
	tControl.i16Position = i16SafePosition;
#ifdef DEBUG_SERVO
	if(1)
	{
		char pBuff[50];
		sprintf(pBuff,"SERVO SetPosition %i\n",u16Position);
		SERIAL_SendMessageInt(pBuff,0);
	}
#endif
	tControl.u8ClutchState = TRUE;
	return (0);	// possibly change to return actual position
}

int16_t SERVO_Enable()
{
	SERIAL_SendMessageInt("SERVO Engage clutch\n",0);
	tControl.i16Position = ADC_GetValue(tControl.u8ServoADC);
	tControl.u8ClutchState = TRUE;
	return (tControl.i16Position);
}

int16_t SERVO_Disable()
{
	SERIAL_SendMessageInt("SERVO Release clutch\n",0);
	tControl.u8ClutchState = FALSE;
	return (tControl.i16Position);
}


